# Folder Structure

* The `src` folder contains all source code for the webinterface.
* The `libs` folder contains all necessary third-party libraries as zip files.
* The `test` folder contains the unittests for the webinterface.
* The `ext` folder contains extension for the webinterface. The make file will
  copy all javascript files from `ext/js/` into the public folder and links the
  javascript in the `public/xsl/main.xsl`.
* The `misc` folder contains a simple http server which is used for running the
  unit tests.

# Setup

* Run `make install` to compile/copy the webinterface to a newly created
  `public` folder.

# Test

* Run `make test` to compile/copy the webinterface and the tests to a newly
  created `public` folder.
* Run `make run-test-server` to start a python http server.
* The test suite can be started with `firefox
  http://localhost:8000/webinterface/`

# Clean

* Run `make clean` to clean up everything.
