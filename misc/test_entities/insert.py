import caosdb as db


try:
    db.execute_query("FIND *-TEST").delete()
except:
    pass

c = db.Container()

singleTextProp = db.Property("SingeTextProp-TEST", datatype="TEXT")
singleDoubleUnitProp = db.Property("SingleDoubleUnitProp-TEST",
                                   datatype="DOUBLE", unit="m")

simpleRT = db.RecordType("SimpleRT-TEST").add_property(
    singleTextProp, importance=db.OBLIGATORY)
simpleRec = db.Record("SimpleRec-TEST").add_parent(simpleRT).add_property(
    singleTextProp, "SomeValue")
listTextProp = db.Property("ListTextProp-TEST", datatype=db.LIST(db.TEXT))
c.extend([
    listTextProp,
    singleTextProp,
    singleDoubleUnitProp,
    simpleRT,
    simpleRec,
    db.RecordType("Parent1-TEST").add_property(singleDoubleUnitProp,
                                               importance=db.OBLIGATORY),
    db.RecordType("Parent2-TEST").add_property(
        singleTextProp,
        importance=db.OBLIGATORY).add_property(simpleRT,
                                               importance=db.RECOMMENDED),
    db.Record("Two-Parents-TEST").add_parent(
        "Parent1-TEST").add_parent("Parent2-TEST").add_property(
            singleTextProp, "SomeTextValue").add_property(
                singleDoubleUnitProp, "15.5").add_property(simpleRT, simpleRec),
])

c.insert()
