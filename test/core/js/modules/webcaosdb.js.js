/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* testing webcaosdb's javascript sources */

QUnit.testStart(function(details) {
    connection._init();
        markdown.toHtml = function(textElement){
            return textElement;
        };
});

/* SETUP general module */
QUnit.module("webcaosdb.js", {
    before: function(assert) {
    }
});

/* TESTS */
QUnit.test("xslt", function(assert) {
    let xml_str = '<root/>';
    let xsl_str = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html" /><xsl:template match="root"><newroot/></xsl:template></xsl:stylesheet>';
    xml = str2xml(xml_str);
    xsl = str2xml(xsl_str);
    broken_xsl = str2xml('<blabla/>');

    html = xslt(xml, xsl);
    assert.ok(html);
    assert.equal(html.firstChild.localName, "newroot", "<root/> transformed to <newroot/>");

    // broken xsl throws exception
    assert.throws(() => {
        xslt(xml, broken_xsl)
    }, "broken xsl throws exc.");

    // string throws exception
    assert.throws(() => {
        xslt(xml_str, xsl)
    }, "xml_str throws exc.");
    assert.throws(() => {
        xslt(xml, xsl_str)
    }, "xsl_str throws exc.");

    assert.throws(() => {
        xslt(undefined, xsl)
    }, "null xml throws exc.");
    assert.throws(() => {
        xslt(xml, undefined)
    }, "nu ll xsl throws exc.");
});

QUnit.test("getEntityId", function(assert) {
    assert.ok(getEntityId, "function available");
    let okElem = $('<div><div class="caosdb-id">1234</div></div>')[0];
    let notOkElem = $('<div><div class="caosdb-id">asdf</div></div>')[0];
    let emptyElem = $('<div></div>')[0];

    assert.throws(() => {
        getEntityId();
    }, "no parameter throws");
    assert.throws(() => {
        getEntityId(null);
    }, "null parameter throws");
    assert.throws(() => {
        getEntityId(notOkElem);
    }, "on-integer ID throws");
    assert.throws(() => {
        getEntityId(empty);
    }, "empty elem throws");

    assert.equal("1234", getEntityId(okElem), "ID found");
});

QUnit.test("asyncXslt", function(assert) {
    let xml_str = '<root/>';
    let xsl_str = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html" /><xsl:template match="root"><newroot/></xsl:template></xsl:stylesheet>';
    xml = str2xml(xml_str);
    xsl = str2xml(xsl_str);
    xslProm = new Promise((resolve, reject) => {
        setTimeout(resolve, 1000, xsl);
    });
    broken_xsl = str2xml('<blabla/>');

    let done = assert.async(2);
    asyncXslt(xml, xslProm).then((html) => {
        assert.ok(html);
        assert.equal(html.firstChild.localName, "newroot", "<root/> transformed to <newroot/>");
        done();
    });

    // broken xsl throws exception
    asyncXslt(xml, broken_xsl).catch((error) => {
        assert.equal(/^\[Exception.*\]$/.test(error.toString()), true, "broken xsl thros exc.");
        done();
    });
});

QUnit.test("xml2str", function(assert) {
    xml = str2xml('<root/>');
    assert.equal(xml2str(xml), '<root/>');
});

QUnit.test("str2xml", function(assert) {
    xml = str2xml('<root/>');
    assert.ok(xml);

    // make sure this is a document:
    assert.equal(xml.contentType, "text/xml", "has contentType=text/xml");
    assert.ok(xml.documentElement, "has documentElement");
    assert.equal(xml.documentElement.outerHTML, '<root/>', "has outerHTML");

    // TODO: there is no mechanism to throw an error when the string is not
    // valid.
});

QUnit.test("postXml", function(assert) {
    assert.ok(postXml, "function exists.");
});

QUnit.test("createErrorNotification", function(assert) {
    assert.ok(createErrorNotification, "function available");
    let err = createErrorNotification("test");
    assert.ok($(err).hasClass(preview.classNameErrorNotification), "has class caosdb-preview-error-notification");
});

/* MODULE connection */
QUnit.module("webcaosdb.js - connection", {
    before: function(assert) {
        window.sessionStorage.caosdbBasePath = "../../";
        assert.ok(connection, "connection module is defined");
    }
});

QUnit.test("get", function(assert) {
    assert.expect(4);
    assert.ok(connection.get, "function available");
    let done = assert.async(2);
    connection.get("webinterface/xsl/entity.xsl").then(function(resolve) {
        assert.equal(resolve.toString(), "[object XMLDocument]", "entity.xsl returned.");
        done();
    });
    connection.get("webinterface/non-existent").then((resolve) => resolve, function(error) {
        assert.equal(error.toString().split(" - ",1)[0], "Error: GET webinterface/non-existent returned with HTTP status 404", "404 error thrown");
        done();
    });
});


/* MODULE transformation */
QUnit.module("webcaosdb.js - transformation", {
    before: function(assert) {
        assert.ok(transformation, "transformation module is defined");
    }
});

QUnit.test("removePermissions", function(assert) {
    assert.ok(transformation.removePermissions, "function available");
});

QUnit.test("retrieveXsltScript", function(assert) {
    assert.ok(transformation.retrieveXsltScript, "function available");
    let done = assert.async(2);
    transformation.retrieveXsltScript("entity.xsl").then(xsl => {
        assert.equal(xsl.toString(), "[object XMLDocument]", "entity.xsl returned");
        done();
    });
    transformation.retrieveXsltScript("asdfasdfasdf.xsl").then(xsl => xsl, err => {
        assert.equal(err.toString().split(" - ")[0], "Error: GET webinterface/xsl/asdfasdfasdf.xsl returned with HTTP status 404", "not found.");
        done();
    });
});

QUnit.test("retrieveEntityXsl", function(assert) {
    assert.ok(transformation.retrieveEntityXsl, "function available");
    let done = assert.async();
    transformation.retrieveEntityXsl().then(xsl => {
        let xml = str2xml('<Response/>');
        let html = xslt(xml, xsl);
        assert.equal(html.firstElementChild.tagName, "DIV");
        done();
    });
});

QUnit.test("transformEntities", function(assert) {
    assert.ok(transformation.transformEntities, "function available");
    let done = assert.async();
    let xml = str2xml('<Response><Record id="142"><Warning description="asdf"/></Record></Response>');
    transformation.transformEntities(xml).then(htmls => {
        assert.ok($(htmls[0]).hasClass("caosdb-entity-panel"), "entity has been transformed");
        assert.equal($(htmls[0]).find('.caosdb-messages .alert-warning').length, 1, "entity has warning.");
        done();
    }, err => {
        globalError(err);
    });
});

QUnit.test("mergeXsltScripts", function(assert) {
    assert.ok(transformation.mergeXsltScripts, 'function available.');
    let xslMainStr = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"/>';
    assert.equal(xml2str(transformation.mergeXsltScripts(str2xml(xslMainStr), [])), xslMainStr, 'no includes returns same as xslMain.');
    let xslIncludeStr = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:template name="bla"/></xsl:stylesheet>'
    let xslInclude = str2xml(xslIncludeStr);
    assert.ok($(transformation.mergeXsltScripts(str2xml(xslMainStr), [xslInclude])).find("[name='bla']")[0], 'template bla is there.');
});

/* MODULE transaction */
QUnit.module("webcaosdb.js - transaction", {
    before: function(assert) {
        assert.ok(transaction, "transaction module is defined");
    },
});

QUnit.test("generateEntitiesUri", function(assert) {
    assert.ok(transaction.generateEntitiesUri, "function available");

    assert.throws(() => {
        transaction.generateEntitiesUri()
    }, 'no param throws');
    assert.throws(() => {
        transaction.generateEntitiesUri(null)
    }, 'null param throws');
    assert.equal(transaction.generateEntitiesUri(["asdf", "qwer"]), "Entity/asdf&qwer", "works");
});

QUnit.test("updateEntitiesXml", function(assert) {
    assert.ok(transaction.updateEntitiesXml, "function available");
    var done = assert.async();
    connection.put = function(uri, data) {
        assert.equal(uri, 'Entity/', "updateEntitiesXml calls connection.put");
        assert.equal(xml2str(data), '<Update/>');
        done();
    };
    transaction.updateEntitiesXml(str2xml('<Update/>'));
});

QUnit.test("retrieveEntitiesById", function(assert) {
    assert.ok(transaction.retrieveEntitiesById, "function available");
    var done = assert.async();
    connection.get = function(uri) {
        assert.equal(uri, 'Entity/1234&2345', "retrieveEntitiesById calls connection.get");
        done();
    };
    transaction.retrieveEntitiesById(["1234", "2345"]);
});

QUnit.test("retrieveEntityById", function(assert) {
    assert.ok(transaction.retrieveEntityById, "function available");
    var done = assert.async();
    connection.get = function(uri) {
        assert.equal(uri, 'Entity/1234', "retrieveEntityById calls connection.get");
        return new Promise((ok, fail) => {
            setTimeout(() => ok(str2xml('<Response><Entity id="1234" name="new"/></Response>')), 200);
        });
    };
    transaction.retrieveEntityById("1234").then(ret => {
        done();
    });
});

/* MODULE transaction.update */
QUnit.module("webcaosdb.js - transaction.update", {
    before: function(assert) {
        assert.ok(transaction.update, "transaction.update module is defined");
    }
});

QUnit.test("createWaitRetrieveNotification", function(assert) {
    assert.ok(transaction.update.createWaitRetrieveNotification(), 'function available and returns non-null');
});

QUnit.test("createWaitUpdateNotification", function(assert) {
    assert.ok(transaction.update.createWaitUpdateNotification(), 'function available and returns non-null');
});

QUnit.test("createUpdateForm", function(assert) {
    let done = assert.async();
    let cuf = transaction.update.createUpdateForm;
    assert.ok(cuf, "function available");
    assert.throws(() => cuf(null, function(xml) {}), "null entityXmlStr throws");
    assert.throws(() => cuf("", null), "null putCallback throws");
    assert.throws(() => cuf("", ""), "non-function putCallback throws");
    assert.throws(() => cuf("", function() {}), "putCallback function without parameters throws");

    let form = cuf("<root/>", function(xml) {
        assert.equal(xml, '<newroot/>', "modified xml is submitted.");
        done();
    });
    assert.equal(form.tagName, "FORM", "returns form");
    assert.ok($(form).hasClass(transaction.classNameUpdateForm), "has correct class");
    assert.equal($(form).find('textarea').length, 1, "has one textarea");
    let textarea = $(form).find('textarea')[0];
    assert.equal(textarea.value, "<root/>", "textarea contains xml");
    assert.equal(textarea.name, "updateXml", "textarea has name updateXml");
    assert.equal($(form).find(':submit').length, 1, "has one submit button");
    assert.equal($(form).find(':reset').length, 1, "has one reset button");

    textarea.value = "<toberesetroot/>"
    $(form).trigger('reset');
    assert.equal(textarea.value, "<root/>", "after reset, old xml is there again");
    textarea.value = "<newroot/>";
    $(form).submit();

    //$(document.body).append(form);
});

QUnit.test("createUpdateEntityHeading", function(assert) {
    let cueh = transaction.update.createUpdateEntityHeading;
    assert.ok(cueh, "function available");
    let eh = $('<div class="panel-heading"><div class="1strow"/><div class="2ndrow"/></div>')[0];
    assert.equal($(eh).children('.1strow').length, 1, "eh has 1st row");
    assert.equal($(eh).children('.2ndrow').length, 1, "eh has 2nd row");
    let uh = cueh(eh);
    assert.equal($(uh).children('.2ndrow').length, 0, "uh has no 2nd row");
    assert.equal($(uh).children('.1strow').length, 1, "uh has 1st row");
});

QUnit.test("createUpdateEntityPanel", function(assert) {
    let cued = transaction.update.createUpdateEntityPanel;
    assert.ok(cued, "function available");
    let div = $(cued($('<div id="headingid">heading</div>')));
    assert.ok(div.hasClass("panel"), "panel has class panel.");
    assert.equal(div.children(":first-child")[0].id, "headingid", "heading is first child element");
});

QUnit.test("updateSingleEntity - success", function(assert) {
    let done = assert.async();
    let use = transaction.update.updateSingleEntity;
    assert.ok(use, "function available");
    let entityPanel = $('<div class="panel panel-default caosdb-entity-panel"><div class="panel-heading caosdb-entity-panel-heading"><div>heading<div class="caosdb-id">1234</div></div><div>other stuff in the heading</div></div>body</div>')[0];
    connection.get = function(uri) {
        assert.equal(uri, 'Entity/1234', 'get was called with correct uri');
        return new Promise((ok, fail) => {
            setTimeout(() => {
                ok(str2xml('<Response><Entity id="1234" name="old"><Permissions/></Entity></Response>'));
            }, 200);
        });
    };


    $(document.body).append(entityPanel);
    let app = use(entityPanel);

    assert.equal(app.state, 'waitRetrieveOld', "in waitRetrieveOld state");

    setTimeout(() => {
        connection._init();
        connection.put = function(uri, xml) {
            assert.equal(xml2str(xml), '<Update><Entity id="1234" name="new"/></Update>', "put was called with correct xml");
            return new Promise((ok, fail) => {
                setTimeout(() => ok(str2xml('<Response><Record id="1234" name="new"></Record></Response>')), 200);
            });
        };
        let form = $(document.body).find('form.' + transaction.classNameUpdateForm);
        assert.ok(form[0], "form is there.");
        assert.equal(form[0].updateXml.value, '<Entity id="1234" name="old"/>', "Permisions have been removed.");
        form[0].updateXml.value = '<Entity id="1234" name="new"/>';
        form.submit();
        assert.equal(app.state, 'waitPutEntity', "in waitPutEntity state");
        assert.equal($(document.body).find('form.' + transaction.classNameUpdateForm).length, 0, "form has been removed.");

    }, 400);



    app.onEnterFinal = function(e) {
        done();
        $(entityPanel).remove();
    };

});

QUnit.test("updateSingleEntity - with errors in the server's response", function(assert) {
    let done = assert.async();
    let use = transaction.update.updateSingleEntity;
    let entityPanel = $('<div class="panel panel-default caosdb-entity-panel"><div class="panel-heading caosdb-entity-panel-heading"><div>heading<div class="caosdb-id">1234</div></div><div>other stuff in the heading</div></div>body</div>')[0];
    connection.get = function(uri) {
        return new Promise((ok, fail) => {
            setTimeout(() => {
                ok(str2xml('<Response><Entity id="1234" name="old"/></Response>'));
            }, 200);
        });
    };

    $(document.body).append(entityPanel);
    let app = use(entityPanel);

    // submit form -> server response contains error tag.
    setTimeout(() => {
        connection._init();
        connection.put = function(uri, xml) {
            return new Promise((ok, fail) => {
                setTimeout(() => ok(str2xml('<Response><Record id="1234" name="new"><Error description="This is an error."/></Record></Response>')), 200);
            });
        };
        $(document.body).find('form.' + transaction.classNameUpdateForm).submit();
    }, 400);

    app.onLeaveWaitPutEntity = function(e) {
        assert.equal(e.transition, "openForm", "app returns to form again due to errors.");
        assert.equal($(app.updatePanel).find('.panel-heading .' + preview.classNameErrorNotification).length, 0, "has no error notification before the response is processed.");

        setTimeout(() => {
            assert.equal($(app.updatePanel).find('.panel-heading .' + preview.classNameErrorNotification).length, 1, "has an error notification after the response is processed.");
            $(app.updatePanel).remove();
            done();
        }, 200);
    };

});

QUnit.test("createErrorInUpdatedEntityNotification", function(assert) {
    assert.ok(transaction.update.createErrorInUpdatedEntityNotification, "function available.");
});

QUnit.test("addErrorNotification", function(assert) {
    assert.ok(transaction.update.addErrorNotification, "function available");
});

/* MODULE preview */
QUnit.module("webcaosdb.js - preview", {
    before: function(assert) {
        // load xmlTestCase
        var done = assert.async(2);
        var qunit_obj = this;
        $.ajax({
            cache: true,
            dataType: 'xml',
            url: "xml/test_case_preview_entities.xml",
        }).done(function(data, textStatus, jdXHR) {
            qunit_obj.testXml = data;
        }).always(function() {
            done();
        });
        // load entity.xsl
        preview.getEntityXsl("../").then(function(data) {
            insertParam(data, "entitypath", "/entitypath/");
            insertParam(data, "filesystempath", "/filesystempath/");
            qunit_obj.entityXSL = injectTemplate(data, '<xsl:template match="/"><root><xsl:apply-templates select="/Response/*" mode="top-level-data"/></root></xsl:template>');
            done();
        });

        window.sessionStorage.caosdbBasePath = "../../"
        assert.ok(preview, "preview module is defined");
    },
    afterEach: function(assert) {
        connection._init();
    }
});

QUnit.test("halfArray", function(assert){
    assert.ok(preview.halfArray, "function available");
    assert.throws(() => {
        preview.halfArray([1]);
    }, "length < 2 throws.")
    assert.deepEqual(preview.halfArray([1,2]), [[1],[2]]);
    assert.deepEqual(preview.halfArray([1,2,3]), [[1],[2,3]]);
    assert.deepEqual(preview.halfArray([1,2,3,4]), [[1,2],[3,4]]);
});

QUnit.test("xslt file preview", function(assert) {
    let done = assert.async();
    let entityXSL = this.entityXSL;
    $.ajax({
        cache: true,
        dataType: 'xml',
        url: "xml/test_case_file_preview.xml",
    }).then((data) => {
        let html = xslt(data, entityXSL);
        assert.ok(html);
        done();
    }).catch((err) => {
        console.log(err);
        assert.ok(false, "error! see console.");
        done();
    });
});

QUnit.test("createShowPreviewButton", function(assert) {
    assert.ok(preview.createShowPreviewButton, "function available");
    let showPreviewButton = preview.createShowPreviewButton();
    assert.ok(showPreviewButton, "not null");
    assert.equal(showPreviewButton.tagName, "BUTTON", "is button element");
    assert.ok($(showPreviewButton).hasClass("caosdb-show-preview-button"), "has class caosdb-show-preview-button");
});

QUnit.test("createHidePreviewButton", function(assert) {
    assert.ok(preview.createHidePreviewButton, "function available");
    let hidePreviewButton = preview.createHidePreviewButton();
    assert.ok(hidePreviewButton, "not null");
    assert.equal(hidePreviewButton.tagName, "BUTTON", "is button element");
    assert.ok($(hidePreviewButton).hasClass("caosdb-hide-preview-button"), "has class 'caosdb-hide-preview-button'");
});

QUnit.test("addHidePreviewButton", function(assert) {
    assert.ok(preview.addHidePreviewButton, "function available");
    let okTestElem = $('<div><div class="caosdb-property-value"></div></div>')[0]
    let notOkTestElem = $('<div></div>')[0]

    assert.throws(() => {
        preview.addHidePreviewButton();
    }, "no argument throws.")
    assert.throws(() => {
        preview.addHidePreviewButton(null, null);
    }, "null arguments throws.");
    assert.throws(() => {
        preview.addHidePreviewButton(okTestElem, null);
    }, "null button_elem parameter throws.");
    assert.throws(() => {
        preview.addHidePreviewButton(null, $('<div/>')[0]);
    }, "null ref_property_elem parameter throws.");
    assert.throws(() => {
        preview.addHidePreviewButton(notOkTestElem, $('<div/>')[0]);
    }, "ref_property_elem w/o caosdb-value-list throws.");
    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addHidePreviewButton(okTestElem, preview.createHidePreviewButton()), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("addShowPreviewButton", function(assert) {
    assert.ok(preview.addShowPreviewButton, "function available");
    let okTestElem = $('<div><div class="caosdb-property-value"></div></div>')[0]
    let notOkTestElem = $('<div></div>')[0]

    assert.throws(() => {
        preview.addShowPreviewButton();
    }, "no argument throws.")
    assert.throws(() => {
        preview.addShowPreviewButton(null, null);
    }, "null arguments throws.");
    assert.throws(() => {
        preview.addShowPreviewButton(okTestElem, null);
    }, "null button_elem parameter throws.");
    assert.throws(() => {
        preview.addShowPreviewButton(null, $('<div/>')[0]);
    }, "null ref_property_elem parameter throws.");
    assert.throws(() => {
        preview.addShowPreviewButton(notOkTestElem, $('<div/>')[0]);
    }, "ref_property_elem w/o caosdb-value-list throws.");
    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addShowPreviewButton(okTestElem, preview.createShowPreviewButton()), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("addWaitingNotification", function(assert) {
    assert.ok(preview.addWaitingNotification, "function available");
    let testWaiting = $('<div>Waiting!</div>')[0];
    let okTestElem = $('<div><div class="caosdb-preview-notification-area"></div></div>')[0];
    let notOkTestElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.addWaitingNotification();
    }, "no arguments throws");
    assert.throws(() => {
        preview.addWaitingNotification(null, null);
    }, "null arguments throws");
    assert.throws(() => {
        preview.addWaitingNotification(null, testWaiting);
    }, "null first argument throws");
    assert.throws(() => {
        preview.addWaitingNotification(okTestElem, null);
    }, "null second argument throws");
    assert.throws(() => {
        preview.addWaitingNotification(notOkTestElem, testWaiting);
    }, "ref_property_elem w/o notification area throws");

    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addWaitingNotification(okTestElem, testWaiting), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("addErrorNotification", function(assert) {
    assert.ok(preview.addErrorNotification, "function available");
    let testError = $('<div>Error!</div>')[0];
    let okTestElem = $('<div><div class="caosdb-preview-notification-area"></div></div>')[0];
    let notOkTestElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.addErrorNotification();
    }, "no arguments throws");
    assert.throws(() => {
        preview.addErrorNotification(null, null);
    }, "null arguments throws");
    assert.throws(() => {
        preview.addErrorNotification(null, testError);
    }, "null first argument throws");
    assert.throws(() => {
        preview.addErrorNotification(okTestElem, null);
    }, "null second argument throws");
    assert.throws(() => {
        preview.addErrorNotification(notOkTestElem, testError);
    }, "ref_property_elem w/o notification area throws");

    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addErrorNotification(okTestElem, testError), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("createWaitingNotification", function(assert) {
        assert.ok(preview.createWaitingNotification, "function available");
        let welem = preview.createWaitingNotification();
        assert.ok(welem, "not null");
        assert.ok($(welem).hasClass("caosdb-preview-waiting-notification"), "element has class 'caosdb-preview-waiting-notification'");
    }),

    QUnit.test("createNotificationArea", function(assert) {
        assert.ok(preview.createNotificationArea, "function available");
        let narea = preview.createNotificationArea();
        assert.ok(narea, "not null");
        assert.equal(narea.tagName, "DIV", "element is div");
        assert.ok($(narea).hasClass("caosdb-preview-notification-area"), "has class caosdb-preview-notification-area");
    });

QUnit.test("getHidePreviewButton", function(assert) {
    assert.ok(preview.getHidePreviewButton, "function available");
    let okElem = $('<div><button class="caosdb-hide-preview-button">click</button></div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getHidePreviewButton();
    }, "no parameter throws.");
    assert.throws(() => {
        preview.getHidePreviewButton(null);
    }, "null parameter throws.");

    assert.notOk(preview.getHidePreviewButton(notOkElem), "parameter w/o button returns null");
    assert.equal(preview.getHidePreviewButton(okElem), okElem.firstChild, "button found");
});

QUnit.test("getRefLinksContainer", function(assert) {
    assert.ok(preview.getRefLinksContainer, "function available");
    // TODO: references or lists of references should have a special class, not just
    // caosdb-value-list. -> entity.xsl
    let okElem = $('<div><div class="caosdb-value-list"><a><span class="caosdb-id">1234</span></a></div></div>')[0];
    let okSingle = $('<div><div class="caosdb-property-value"><a class="btn"><span class="caosdb-id">1234</span></a></div><</div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getRefLinksContainer();
    }, "no parameter throws");
    assert.throws(() => {
        preview.getRefLinksContainer(null);
    }, "null parameter throws");

    assert.notOk(preview.getRefLinksContainer(notOkElem), "parameter w/o elem returns null");
    assert.equal(preview.getRefLinksContainer(okElem), okElem.firstChild, "links found");
    assert.equal(preview.getRefLinksContainer(okSingle), okSingle.firstChild.firstChild, "single link found");
});

QUnit.test("getPreviewCarousel", function(assert) {
    assert.ok(preview.getPreviewCarousel, "function available");
    let okElem = $('<div><div class="' + preview.classNamePreview + '"></div></div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getPreviewCarousel();
    }, "no parameter throws");
    assert.throws(() => {
        preview.getPreviewCarousel(null);
    }, "null parameter throws");

    assert.notOk(preview.getPreviewCarousel(notOkElem), "parameter w/o carousel returns null");
    assert.equal(preview.getPreviewCarousel(okElem), okElem.firstChild, "carousel found");
});

QUnit.test("getShowPreviewButton", function(assert) {
    assert.ok(preview.getShowPreviewButton, "function available");
    let okElem = $('<div><button class="caosdb-show-preview-button">click</button></div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getShowPreviewButton();
    }, "no parameter throws.");
    assert.throws(() => {
        preview.getShowPreviewButton(null);
    }, "null parameter throws.");

    assert.notOk(preview.getShowPreviewButton(notOkElem), "parameter w/o button returns null");
    assert.equal(preview.getShowPreviewButton(okElem), okElem.firstChild, "button found");
});

QUnit.test("removeAllErrorNotifications", function(assert) {
    assert.ok(preview.removeAllErrorNotifications, "function available");
    let okElem = $('<div><div class="caosdb-preview-error-notification">Error1</div>' +
        '<div class="caosdb-preview-error-notification">Error2</div>' +
        '<div class="caosdb-preview-waiting-notification">Please wait!</div></div>')[0];
    let emptyElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.removeAllErrorNotifications();
    }, "no parameter throws");
    assert.throws(() => {
        preview.removeAllErrorNotifications(null);
    }, "null parameter throws");

    assert.equal(okElem.childNodes.length, 3, "before: three children");
    assert.equal(okElem, preview.removeAllErrorNotifications(okElem), "return first parameter");
    assert.equal(okElem.childNodes.length, 1, "after: one child");
    assert.equal(okElem.firstChild.className, "caosdb-preview-waiting-notification", "waiting notification still there");

    assert.equal(emptyElem, preview.removeAllErrorNotifications(emptyElem), "empty elem works");
});

QUnit.test("removeAllWaitingNotifications", function(assert) {
    assert.ok(removeAllWaitingNotifications, "function available");
    let okElem = $('<div><div class="caosdb-preview-waiting-notification">Waiting1</div>' +
        '<div class="caosdb-preview-waiting-notification">Waiting2</div>' +
        '<div class="caosdb-preview-error-notification">Error!</div></div>')[0];
    let emptyElem = $('<div></div>')[0];

    assert.throws(() => {
        removeAllWaitingNotifications();
    }, "no parameter throws");
    assert.throws(() => {
        removeAllWaitingNotifications(null);
    }, "null parameter throws");

    assert.equal(okElem.childNodes.length, 3, "before: three children");
    assert.equal(okElem, removeAllWaitingNotifications(okElem), "return first parameter");
    assert.equal(okElem.childNodes.length, 1, "after: one child");
    assert.equal(okElem.firstChild.className, "caosdb-preview-error-notification", "error notification still there");

    assert.equal(emptyElem, removeAllWaitingNotifications(emptyElem), "empty elem works");
});

QUnit.test("createSlideItem", function(assert) {
    assert.ok(preview.createSlideItem, "function available");
    let p_elem = $('<div>preview</div>')[0];

    assert.throws(() => {
        preview.createSlideItem();
    }, "no parameter throws");
    assert.throws(() => {
        preview.createSlideItem(null);
    }, "null parameter throws");

    let item = preview.createSlideItem(p_elem);
    assert.ok($(item).hasClass("item"), "has class item");
    assert.equal(p_elem, item.firstChild, "is wrapped");
});

QUnit.test("getActiveSlideItemIndex", function(assert) {
    assert.ok(preview.getActiveSlideItemIndex, "function available");
    let okElem0 = $('<div><div class="carousel-inner">' +
        '<div class="item active"></div>' // index 0
        +
        '<div class="item"></div>' +
        '<div class="item"></div>' +
        '</div></div>')[0];
    let okElem1 = $('<div><div class="carousel-inner">' +
        '<div class="item"></div>' +
        '<div class="item active"></div>' // index 1
        +
        '<div class="item"></div>' +
        '</div></div>')[0];
    let okElem2 = $('<div><div class="carousel-inner">' +
        '<div class="item"></div>' +
        '<div class="item"></div>' +
        '<div class="item active"></div>' // index 2
        +
        '</div></div>')[0];
    let noInner = $('<div></div>')[0];
    let noActive = $('<div><div class="carousel-inner"><div class="item"></div></div></div>')[0];

    assert.throws(() => {
        preview.getActiveSlideItemIndex()
    }, "no params throws");
    assert.throws(() => {
        preview.getActiveSlideItemIndex(null)
    }, "null param throws");
    assert.throws(() => {
        preview.getActiveSlideItemIndex(noInner)
    }, "param w/o .carousel-inner throws");
    assert.throws(() => {
        preview.getActiveSlideItemIndex(noActive)
    }, "param w/o .active throws");

    assert.equal(0, preview.getActiveSlideItemIndex(okElem0));
    assert.equal(1, preview.getActiveSlideItemIndex(okElem1));
    assert.equal(2, preview.getActiveSlideItemIndex(okElem2));
});

QUnit.test("getEntityById", function(assert) {
    assert.ok(preview.getEntityById, "function available");
    let e1 = $('<div><div class="caosdb-id">1</div></div>')[0];
    let e2 = $('<div><div class="caosdb-id">2</div></div>')[0];
    let e3 = $('<div><div class="caosdb-id">3</div><div><div class="caosdb-id">1</div></div></div>')[0];

    let es = [e1, e2, e3];

    assert.throws(() => {
        preview.getEntityById()
    }, "no param throws.");
    assert.throws(() => {
        preview.getEntityById(null, 1)
    }, "null first param throws.");
    assert.throws(() => {
        preview.getEntityById("asdf", 1)
    }, "string first param throws.");
    assert.throws(() => {
        preview.getEntityById(es, null)
    }, "null second param throws.");
    assert.throws(() => {
        preview.getEntityById(es, "asdf")
    }, "string second param throws.");

    assert.equal(e1, preview.getEntityById(es, 1), "find 1");
    assert.equal(e2, preview.getEntityById(es, 2), "find 2");
    assert.equal(e3, preview.getEntityById(es, 3), "find 3");
    assert.equal(null, preview.getEntityById(es, 4), "find 4 -> null");
});

QUnit.test("createEmptyInner", function(assert) {
    assert.ok(preview.createEmptyInner, "function available");

    assert.throws(() => {
        preview.createEmptyInner();
    }, "no param throws");
    assert.throws(() => {
        preview.createEmptyInner(null);
    }, "null param throws");
    assert.throws(() => {
        preview.createEmptyInner("asdf");
    }, "NaN param throws");
    assert.throws(() => {
        preview.createEmptyInner(-5);
    }, "smaller 1 param throws");

    let inner = preview.createEmptyInner(3);
    assert.equal(inner.children.length, 3, "three items");
    assert.equal(inner.children[0].className, "item active", "first item is active");
    assert.equal(inner.children[1].className, "item", "second item is not active");
    assert.equal(inner.children[2].className, "item", "third item is not active");
});

QUnit.test("createCarouselNav", function(assert) {
    assert.ok(preview.createCarouselNav, "function available");
    let refLinks = $('<div style="display: none;" class="caosdb-value-list"><a><span class="caosdb-id">1234</span></a><a><span class="caosdb-id">2345</span></a><a><span class="caosdb-id">3456</span></a><a><span class="caosdb-id">4567</span></a></div>')[0];
    assert.throws(() => {
        preview.createCarouselNav();
    }, "no param throws");
    assert.throws(() => {
        preview.createCarouselNav(null, "asdf");
    }, "null 1st param throws");
    assert.throws(() => {
        preview.createCarouselNav(refLinks, null);
    }, "null 2nd param throws");

    let nav = preview.createCarouselNav(refLinks, "cid");
    assert.equal(nav.className, "caosdb-preview-carousel-nav", "caosdb-carousel-nav");
    assert.ok($(nav).find('[data-slide="prev"][href="#cid"]')[0], "has prev button");
    assert.ok($(nav).find('[data-slide="next"][href="#cid"]')[0], "has next button");
    let selectors = preview.getRefLinksContainer(nav);
    assert.equal(selectors.children.length, 4, '4 selctor buttons');
    $(document.body).append(nav);
    assert.equal($(selectors).is(':hidden'), false, "selectors not hidden.");
    $(nav).remove();
    $(selectors).find('a').each((index, button) => {
        assert.equal(button.getAttribute("data-slide-to"), index, "buttons have correct data-slide-to attribute");
        assert.equal(button.getAttribute("data-target"), "#cid", "buttons have correct data-target attribute");
        assert.notOk(button.getAttribute("href"), "button dont have href");
    });
    assert.equal($(selectors).find('a:first').hasClass('active'), true, "first button is active");
    assert.equal($(selectors).find('.active').length, 1, "no other bu tton is active");
});

{
    let refLinks = $('<div class="caosdb-value-list"><a><span class="caosdb-id">1234</span></a><a><span class="caosdb-id">2345</span></a><a><span class="caosdb-id">3456</span></a><a><span class="caosdb-id">4567</span></a></div>')[0];
    let e1 = $('<div><div class="caosdb-id">1234</div></div>')[0];
    let e2 = $('<div><div class="caosdb-id">2345</div></div>')[0];
    let e3 = $('<div><div class="caosdb-id">3456</div><div><div class="caosdb-id">1234</div></div></div>')[0];
    let e4 = $('<div><div class="caosdb-id">4567</div></div>')[0];
    let entities = [e1, e3, e4, e2];
    let carousel = preview.createPreviewCarousel(entities, refLinks);
    let correct_order_id = ["1234", "2345", "3456", "4567"];
    let preview3Links = preview.createPreview(entities, refLinks);
    let preview1Link = preview.createPreview([e1], refLinks.children[0]);

    QUnit.test("createPreviewCarousel", function(assert) {
        assert.ok(preview.createPreviewCarousel, "function available");


        assert.throws(() => {
            preview.createPreviewCarousel()
        }, 'no param throws');
        assert.throws(() => {
            preview.createPreviewCarousel(null, refLinks)
        }, 'null 1st param throws');
        assert.throws(() => {
            preview.createPreviewCarousel(entities, null)
        }, 'null 2nd param throws');
        assert.throws(() => {
            preview.createPreviewCarousel([], null)
        }, 'missing entities in 1st param throws');

        assert.equal($(carousel).find("." + preview.classNamePreviewCarouselNav).length, 1, "carousel has nav");
        assert.equal($(carousel).find(".carousel-inner").length, 1, "carousel has inner");
        for (let i = 0; i < correct_order_id.length; i++) {
            assert.equal(getEntityId($(carousel).find('.item')[i]), correct_order_id[i], "entities ids are in order")
        }

        assert.ok(carousel.id, "has id");
        assert.equal($(carousel).attr("data-interval"), "false", "no auto-sliding");
    });

    QUnit.test("getSelectorButtons", function(assert) {
        assert.ok(preview.getSelectorButtons, "function available");
        assert.equal(preview.getSelectorButtons($(carousel).find('.' + preview.classNamePreviewCarouselNav)[0])[0].getAttribute('data-slide-to'), "0", "found selector button");
    });

    QUnit.test("setActiveSlideItemSelector", function(assert) {
        assert.ok(preview.setActiveSlideItemSelector, "function available");

        assert.throws(() => {
            preview.setActiveSlideItemSelector()
        }, "no param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(null, 1)
        }, "null 1st param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, null)
        }, "null 2nd param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, 15)
        }, "too big 2nd param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, "asdf")
        }, "NaN 2nd param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, -5)
        }, "negative 2nd param throws");

        assert.equal(preview.setActiveSlideItemSelector(carousel, 1), carousel, "returns carousel");
        for (let i = 0; i < correct_order_id.length; i++) {
            preview.setActiveSlideItemSelector(carousel, i);
            assert.equal($($(carousel).find('[data-slide-to]')[i]).hasClass("active"), true, "button " + i + " is active");
            assert.equal($(carousel).find('.item.active').length, 1, "and none else");
        }
    });

    QUnit.test("triggerUpdateActiveSlideItemSelector", function(assert) {
        assert.ok(preview.triggerUpdateActiveSlideItemSelector, "function available");

        preview.setActiveSlideItemSelector(carousel, 1);
        assert.equal(preview.getActiveSlideItemIndex(carousel), 0, "before: active item is 0");
        assert.equal($(carousel).find('.' + preview.classNamePreviewCarouselNav).find('.active')[0].getAttribute('data-slide-to'), 1, 'before: active selector is 1.');
        $(carousel).on('slid.bs.carousel', preview.triggerUpdateActiveSlideItemSelector);
        $(carousel).trigger('slid.bs.carousel');
        assert.equal(preview.getActiveSlideItemIndex(carousel), 0, "after: active item is 0");
        assert.equal($(carousel).find('.' + preview.classNamePreviewCarouselNav).find('.active')[0].getAttribute('data-slide-to'), 0, 'after: active selector is 0.');
    });

    QUnit.test("createPreview", function(assert) {
        assert.ok($(preview3Links).hasClass(preview.classNamePreview), "3 links class name.");
        assert.ok($(preview1Link).hasClass(preview.classNamePreview), "1 links class name.");
        assert.ok($(preview1Link).hasClass(preview.classNamePreview), "1 links returns entity element");
    });

    let xmlResponse = str2xml('<Response><Record id="1234"/><Record id="2345"/><Record id="3456"/><Record id="4567"/></Response>');
    let ref_property_elem = $('<div><div class="caosdb-property-value"></div></div>');
    let original_get = connection.get;
    ref_property_elem.find('div').append(refLinks);
    QUnit.test("initProperty", function(assert) {
        //$(document.body).append(ref_property_elem);
        let done = assert.async();
        assert.ok(preview.initProperty, "function available");
        let app = preview.initProperty(ref_property_elem[0]);
        assert.equal(app.state, 'showLinks', 'app returned');

        let showPreviewButton = $(ref_property_elem).find('.' + preview.classNameShowPreviewButton);
        assert.equal(showPreviewButton.length, 1, 'one show preview button.');

        connection.get = function(uri) {
            assert.equal(uri, "Entity/1234&2345&3456&4567", "get called with correct uri");
            return new Promise(function(ok, err) {
                setTimeout(() => {
                    err('test error');
                }, 200);
            });
        };

        showPreviewButton.click();

        setTimeout(() => {
            assert.equal(app.state, 'showLinks', 'after reset in showLinks state');
            connection.get = function(uri) {
                if (uri === "webinterface/xsl/entity.xsl" || uri === "webinterface/xsl/messages.xsl") {
                    console.log("returning");
                    return original_get(uri);
                }
                return new Promise(function(ok, err) {
                    setTimeout(() => {
                        ok(xmlResponse);
                    }, 200);
                });
            };
            showPreviewButton.click();
            assert.equal(app.state, 'waiting', 'app is now in waiting state');

            setTimeout(() => {
                assert.equal(app.state, 'showPreview', 'app is now in showPreview state');

                let hidePreviewButton = $(ref_property_elem).find('.' + preview.classNameHidePreviewButton);
                assert.equal(hidePreviewButton.length, 1, 'one hidePreviewButton');
                hidePreviewButton.click();
                assert.equal(app.state, 'showLinks', 'again in showLinks state.');

                connection.get = function(uri) {
                    assert.ok(null, 'get was called: ' + uri);
                }
                showPreviewButton.click();
                assert.equal(app.state, 'showPreview', 'again in showPreview state but without calling connection.get again.');
                app.resetApp();
                assert.equal($(ref_property_elem).find('.' + preview.classNamePreview).length, 0, 'no carousel after reset');
                done();

            }, 600);

        }, 400);


    });


};

QUnit.test("preparePreviewEntity", function(assert){
    assert.ok(preview.preparePreviewEntity, "function available");
    let e = $('<div><div class="label caosdb-id">1234</div></div>')[0];
    let prepared = preview.preparePreviewEntity(e);
    assert.equal($(prepared).find('a.caosdb-id')[0].href, connection.getBasePath() + "Entity/1234", "link is correct.");
});

QUnit.test("getEntitiyIds", function(assert) {
    assert.ok(preview.getEntityIds, 'function available');
});

QUnit.test("retrievePreviewEntities", function(assert) {
    let done = assert.async(3);
    connection.get = function(url){
        if(url.length>15) {
            assert.equal(url, "Entity/1&2&3&4&5", "All five entities are to be retrieved.");
            done();
            throw new Error("UriTooLongException")
        } else {
            assert.equal(url, "Entity/1&2", "Only the first two entities are to be retrieved.");
            done();
            throw new Error("Terminate this test!");
        }
    }
    assert.ok(preview.retrievePreviewEntities, "function available");
    preview.retrievePreviewEntities([1,2,3,4,5]).catch(err=>{assert.equal(err.message, "Terminate this test!", "The url had been split up.");done();});
});

QUnit.test("transformXmlToPreviews", function(assert) {
    assert.ok(preview.transformXmlToPreviews, "function available");
    assert.ok(this.entityXSL, "xsl there");
    assert.ok(this.testXml, "xml there");

    let done = assert.async();
    let asyncTestCase = function(resolve) {
        done();
    };
    let asyncErr = function(error) {
        console.log(error);
        done();
        done();
    }
    preview.transformXmlToPreviews(this.testXml, this.entityXSL).then(asyncTestCase).catch(asyncErr);
});

QUnit.test("init", function(assert) {
    assert.ok(preview.init, "function available");
});

QUnit.test("initEntity", function(assert) {
    assert.ok(preview.initEntity, "function available");
});

/* MODULE queryForm */
QUnit.module("webcaosdb.js - queryForm", {
    before: function(assert) {
        assert.ok(queryForm, "queryForm is defined");
    }
});

QUnit.test("removePagingField", function(assert) {
    assert.ok(queryForm.removePagingField, "function available.");
    assert.throws(() => queryForm.removePagingField(), "null param throws.");
    let form = $('<form><input name="P"></form>')[0];
    assert.ok(form.P, "before: paging available.");
    queryForm.removePagingField(form);
    assert.notOk(form.P, "after: paging removed.");

});

QUnit.test("isSelectQuery", function(assert) {
    assert.ok(queryForm.isSelectQuery, "function available.");
    assert.throws(() => queryForm.isSelectQuery(), "null param throws.");
    assert.equal(queryForm.isSelectQuery("SELECT asdf"), true);
    assert.equal(queryForm.isSelectQuery("select asdf"), true);
    assert.equal(queryForm.isSelectQuery("SEleCt"), true);
    assert.equal(queryForm.isSelectQuery("FIND"), false);
    assert.equal(queryForm.isSelectQuery("asd"), false);
    assert.equal(queryForm.isSelectQuery("SEL ECT"), false);
});

QUnit.test("init", function(assert) {
    assert.ok(queryForm.init, "init available");
});

QUnit.test("restoreLastQuery", function(assert) {
    assert.ok(queryForm.restoreLastQuery, "available");

    let form = document.createElement("form");
    form.innerHTML = '<textarea name="query"></textarea><div class="caosdb-search-btn"></div>';

    assert.throws(() => queryForm.restoreLastQuery(form, null), "null getter throws exc.");
    assert.throws(() => queryForm.restoreLastQuery(null, () => "test"), "null form throws exc.");
    assert.throws(() => queryForm.restoreLastQuery(null, () => undefined), "null form throws exc.");

    assert.equal(form.query.value, "", "before1: field is empty");
    queryForm.restoreLastQuery(form, () => undefined);
    assert.equal(form.query.value, "", "after1: field is still empty");

    assert.equal(form.query.value, "", "before2: field is empty");
    queryForm.restoreLastQuery(form, () => "this is the old query");
    assert.equal(form.query.value, "this is the old query", "after2: field is not empty");
});

QUnit.test("bindOnClick", function(assert) {
    assert.ok(queryForm.bindOnClick, "available");
    var done = assert.async(2);
    queryForm.redirect = function(a, b) {done();};

    let form = document.createElement("form");
    let submitButton = $("<input type=\"submit\">");
    form.innerHTML = '<textarea name="query"></textarea><div class="caosdb-search-btn"></div>';

    assert.throws(() => queryForm.bindOnClick(form, null), "null setter throws exc.");
    assert.throws(() => queryForm.bindOnClick(form, "asdf"), "non-function setter throws exc.");
    assert.throws(() => queryForm.bindOnClick(form, () => undefined), "setter with zero params throws exc.");
    assert.throws(() => queryForm.bindOnClick(null, (set) => undefined), "null form throws exc.");
    assert.throws(() => queryForm.bindOnClick("asdf", (set) => undefined), "string form throws exc.");

    let storage = function() {
        let x = undefined;
        return function(set) {
            if (set) {
                x = set;
            }
            return x;
        }
    }();
    // test empty query, function should return silently
    queryForm.bindOnClick(form, storage);
    assert.equal(storage(), undefined, "before1: storage empty.");
    form.getElementsByClassName("caosdb-search-btn")[0].onclick();
    assert.equal(storage(), undefined, "after1: storage still empty.");


    // test the click handler of the button
    form.query.value = "free text";
    assert.equal(storage(), undefined, "before2: storage empty.");
    form.getElementsByClassName("caosdb-search-btn")[0].onclick();

    assert.equal(storage(), "FIND ENTITY WHICH HAS A PROPERTY LIKE '*free text*'", "after2: storage not empty.");

    // test the form submit handler analogously
    form.query.value = "free text 2";
    $("body").append(form);
    $(form).append(submitButton);
    submitButton.click();
    assert.equal(storage(), "FIND ENTITY WHICH HAS A PROPERTY LIKE '*free text 2*'", "after3: storage not empty.");
    $(form).remove();
})

/* MODULE paging */
QUnit.module("webcaosdb.js - paging", {
    before: function(assert) {}
});

QUnit.test("initPaging", function(assert) {
    let initPaging = paging.initPaging;
    let getPageHref = paging.getPageHref;
    assert.ok(initPaging, "function exists.");
    assert.equal(initPaging(), false, "no parameter returs false.");
    assert.equal(initPaging(null), false, "null parameter returns false.");
    assert.equal(initPaging(null, null), false, "null,null parameter returns false.");
    assert.throws(() => {
        initPaging(getPageHref(window.location.href, "0L10"), "asdf")
    }, "string parameter throws exc.");
    assert.equal(initPaging(window.location.href, 1234), true, "no paging.");
    assert.equal(initPaging(getPageHref(window.location.href, "0L10"), 1234), true, "1234 returns true.");
    assert.equal(initPaging(getPageHref(window.location.href, "0L10"), '1234'), true, "'1234' returns true.");

    // test effectiveness
    let $pagingPanel = $('<div>', {
        "class": "caosdb-paging-panel"
    });
    let $prevButton = $('<a>', {
        "class": "caosdb-prev-button"
    });
    let $nextButton = $('<a>', {
        "class": "caosdb-next-button"
    });

    $pagingPanel.append($prevButton).append($nextButton);
    $(document.body).append($pagingPanel);

    $prevButton.hide();
    $nextButton.hide();
    $pagingPanel.hide();


    // no paging at all:
    let hidden_prev = $('.caosdb-prev-button').css("display") == "none";
    let hidden_next = $('.caosdb-next-button').css("display") == "none";
    let hidden_panel = $('.caosdb-paging-panel').css("display") == "none";

    initPaging(window.location.href);
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_next = hidden_next && $('.caosdb-next-button').css("display") == "none";
    hidden_panel = hidden_panel && $('.caosdb-paging-panel').css("display") == "none";

    initPaging(window.location.href, null);
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_next = hidden_next && $('.caosdb-next-button').css("display") == "none";
    hidden_panel = hidden_panel && $('.caosdb-paging-panel').css("display") == "none";

    initPaging(null, null);
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_next = hidden_next && $('.caosdb-next-button').css("display") == "none";
    hidden_panel = hidden_panel && $('.caosdb-paging-panel').css("display") == "none";

    initPaging(window.location.href, "100");
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_next = hidden_next && $('.caosdb-next-button').css("display") == "none";
    hidden_panel = hidden_panel && $('.caosdb-paging-panel').css("display") == "none";

    initPaging(window.location.href, 100);
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_next = hidden_next && $('.caosdb-next-button').css("display") == "none";
    hidden_panel = hidden_panel && $('.caosdb-paging-panel').css("display") == "none";

    initPaging(getPageHref(window.location.href, "0L100"), "100");
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_next = hidden_next && $('.caosdb-next-button').css("display") == "none";
    hidden_panel = hidden_panel && $('.caosdb-paging-panel').css("display") == "none";

    assert.equal(hidden_prev, true, "prev button has display=none");
    assert.equal(hidden_next, true, "next button has display=none");
    assert.equal(hidden_panel, true, "paging panel has display=none");

    // show next button
    initPaging(getPageHref(window.location.href, "0L10"), 100);
    hidden_prev = $('.caosdb-prev-button').css("display") == "none";
    hidden_panel = $('.caosdb-paging-panel').css("display") != "block";
    hidden_next = $('.caosdb-next-button').css("display") != "inline";
    let nextHrefOk = $('.caosdb-next-button').attr('href') == getPageHref(window.location.href, "10L10");

    initPaging(getPageHref(window.location.href, "0L10"), "100");
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_panel = hidden_panel || $('.caosdb-paging-panel').css("display") != "block";
    hidden_next = hidden_next || $('.caosdb-next-button').css("display") != "inline";
    nextHrefOk = nextHrefOk && $('.caosdb-next-button').attr('href') == getPageHref(window.location.href, "10L10");

    initPaging(getPageHref(window.location.href, "0L99"), "100");
    hidden_prev = hidden_prev && $('.caosdb-prev-button').css("display") == "none";
    hidden_panel = hidden_panel || $('.caosdb-paging-panel').css("display") != "block";
    hidden_next = hidden_next || $('.caosdb-next-button').css("display") != "inline";
    nextHrefOk = nextHrefOk && $('.caosdb-next-button').attr('href') == getPageHref(window.location.href, "99L99");

    assert.equal(hidden_prev, true, "prev button has display=none");
    assert.equal(hidden_next, false, "next button has display=inline");
    assert.equal(hidden_panel, false, "paging panel has display=block");
    assert.equal(nextHrefOk, true, "next buttons href is ok");

    // show prev button
    initPaging(getPageHref(window.location.href, "10L100"), 100);
    hidden_prev = $('.caosdb-prev-button').css("display") != "inline";
    hidden_panel = $('.caosdb-paging-panel').css("display") != "block";
    hidden_next = $('.caosdb-next-button').css("display") == "none";
    let prevHrefOk = $('.caosdb-prev-button').attr('href') == getPageHref(window.location.href, "0L100");

    initPaging(getPageHref(window.location.href, "1L100"), 100);
    hidden_prev = hidden_prev || $('.caosdb-prev-button').css("display") != "inline";
    hidden_panel = hidden_panel || $('.caosdb-paging-panel').css("display") != "block";
    hidden_next = hidden_next && $('.caosdb-next-button').css("display") == "none";
    prevHrefOk = prevHrefOk && $('.caosdb-prev-button').attr('href') == getPageHref(window.location.href, "0L100");

    initPaging(getPageHref(window.location.href, "20L10"), 100);
    hidden_prev = hidden_prev || $('.caosdb-prev-button').css("display") != "inline";
    hidden_panel = hidden_panel || $('.caosdb-paging-panel').css("display") != "block";
    prevHrefOk = prevHrefOk && $('.caosdb-prev-button').attr('href') == getPageHref(window.location.href, "10L10");

    assert.equal(hidden_prev, false, "prev button has display=inline");
    assert.equal(hidden_next, true, "next button has display=none");
    assert.equal(hidden_panel, false, "paging panel has display=block");
    assert.equal(prevHrefOk, true, "prev buttons href is ok");

    // show both
    initPaging(getPageHref(window.location.href, "10L10"), 100);
    hidden_prev = $('.caosdb-prev-button').css("display") != "inline";
    hidden_panel = $('.caosdb-paging-panel').css("display") != "block";
    hidden_next = $('.caosdb-next-button').css("display") != "inline";
    nextHrefOk = nextHrefOk && $('.caosdb-next-button').attr('href') == getPageHref(window.location.href, "20L10");
    prevHrefOk = prevHrefOk && $('.caosdb-prev-button').attr('href') == getPageHref(window.location.href, "0L10");

    initPaging(getPageHref(window.location.href, "1L100"), 200);
    hidden_prev = hidden_prev || $('.caosdb-prev-button').css("display") != "inline";
    hidden_panel = hidden_panel || $('.caosdb-paging-panel').css("display") != "block";
    hidden_next = hidden_next || $('.caosdb-next-button').css("display") != "inline";
    nextHrefOk = nextHrefOk && $('.caosdb-next-button').attr('href') == getPageHref(window.location.href, "101L100");
    prevHrefOk = prevHrefOk && $('.caosdb-prev-button').attr('href') == getPageHref(window.location.href, "0L100");

    assert.equal(hidden_prev, false, "prev button has display=inline");
    assert.equal(hidden_next, false, "next button has display=inline");
    assert.equal(hidden_panel, false, "paging panel has display=block");
    assert.equal(prevHrefOk, true, "prev buttons href is ok");
    assert.equal(nextHrefOk, true, "next buttons href is ok");

    document.body.removeChild($pagingPanel[0]);
});


QUnit.test("getNextPage", function(assert) {
    let getNextPage = paging.getNextPage;
    assert.ok(getNextPage, "function exists.");
    assert.throws(() => {
        getNextPage(null, 100)
    }, "null P throws exc.");
    assert.throws(() => {
        getNextPage("0L10", null)
    }, "null n throws exc.");
    assert.throws(() => {
        getNextPage("asdf", 100)
    }, "P with wrong format 1.");
    assert.throws(() => {
        getNextPage("1234l2345", 100)
    }, "P with wrong format 2.");
    assert.throws(() => {
        getNextPage("1234Lasdf", 100)
    }, "P with wrong format 3.");
    assert.throws(() => {
        getNextPage("0L10", -100)
    }, "n is negative.");
    assert.throws(() => {
        getNextPage("0L10", "asdf")
    }, "string n throws exc.");
    assert.equal(getNextPage("23L11", 30), null, "n is smaller than index+length -> no next page.");
    assert.equal(getNextPage("0L10", 10), null, "n equals index+length -> no next page.");
    assert.equal(getNextPage("0L22", 30), "22L22", "0L22 to 22L22.")
    assert.equal(getNextPage("5L22", 30), "27L22", "5L22 to 27L22.")
    assert.equal(getNextPage("5L10", 30), "15L10", "5L10 to 15L10.")

});

QUnit.test("getPrevPage", function(assert) {
    let getPrevPage = paging.getPrevPage;
    assert.ok(getPrevPage, "function exists.");
    assert.throws(() => {
        getPrevPage(null)
    }, "null P throws exc.");
    assert.throws(() => {
        getPrevPage("asdf")
    }, "P with wrong format 1.");
    assert.throws(() => {
        getPrevPage("1234l2345")
    }, "P with wrong format 2.");
    assert.throws(() => {
        getPrevPage("1234Lasdf")
    }, "P with wrong format 3.");
    assert.equal(getPrevPage("0L10"), null, "Begins with 0 -> No previous page.");
    assert.equal(getPrevPage("10L10"), "0L10", "Index 10 to index 0.");
    assert.equal(getPrevPage("5L10"), "0L10", "Index 5 to index 0.");
    assert.equal(getPrevPage("23L11"), "12L11", "Index 5 to index 0.");
});

QUnit.test("getPSegmentFromUri", function(assert) {
    let getPSegmentFromUri = paging.getPSegmentFromUri;
    assert.ok(getPSegmentFromUri, "function exists.");
    assert.throws(() => {
        getPSegmentFromUri(null)
    }, "null uri throws exc.");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla"), null, "asdf has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf"), null, "asdf?asdf has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?P"), null, "asdf?P has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blablaP=0L10"), null, "asdfP=0L10 has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?P=0L10"), "0L10", "asdf?P=0L10 -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdfP=0L10"), null, "asdf?asdfP=0L10 has no P segment.");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf&P=0L10"), "0L10", "asdf?asdf&P=0L10 -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?&P=0L10"), "0L10", "asdf?&P=0L10 -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf&P=0L10&"), "0L10", "asdf?asdf&P=0L10& -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf&P=0L10&asdfasdf"), "0L10", "asdf?asdf&P=0L10&asdfasdf -> P=0L10");
});

QUnit.test("getPageHref", function(assert) {
    let getPageHref = paging.getPageHref;
    assert.ok(getPageHref, "function exists.");
    assert.throws(() => {
        getPageHref(null, "asdf")
    }, "null uri_old throws exc.");
    assert.equal(getPageHref("1234?P=1234", null), null, "null page returns null.");
    assert.equal(getPageHref("https://example:1234/blabla?P=page1&", "page2"), "https://example:1234/blabla?P=page2&", "replace page1 with page2");
    assert.equal(getPageHref("https://example:1234/blabla?P=page1", "page2"), "https://example:1234/blabla?P=page2", "replace page1 with page2");
    assert.equal(getPageHref("https://example:1234/blabla?asdf&P=page1", "page2"), "https://example:1234/blabla?asdf&P=page2", "replace page1 with page2");
    assert.equal(getPageHref("https://example:1234/blabla?asdfP=page1", "page2"), "https://example:1234/blabla?asdfP=page1&P=page2", "append page2");
    assert.equal(getPageHref("https://example:1234/blabla", "page2"), "https://example:1234/blabla?P=page2", "append page2");
});

/* MODULE annotation */
QUnit.module("webcaosdb.js - annotation", {
    before: function(assert) {
        // overwrite (we don't actually want to send any post requests)
        annotation.postCommentXml = function(xml) {
            return new Promise(resolve => setTimeout(resolve, 1000, str2xml("<Response/>")));
        }
        window.sessionStorage.caosdbBasePath = "../../";
    }
});

QUnit.test("loadAnnotationXsl", function(assert) {
    assert.ok(annotation.loadAnnotationXsl, "function exists");
});

QUnit.test("getAnnotationsForEntity", function(assert) {
    let xsl_str = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html" /><xsl:template match="annotation"><div><xsl:value-of select="@id"/></div></xsl:template><xsl:template match="Response"><root><xsl:apply-templates select="annotation"/></root></xsl:template></xsl:stylesheet>';
    let xslPromise = str2xml(xsl_str);
    let xml_str = '<Response><annotation id="1"/><annotation id="2"/><annotation id="3"/></Response>';
    let response = str2xml(xml_str);
    let databaseRequest = (id) => {
        return response;
    };

    let done = assert.async();
    let asyncTestCase = function(result) {
        assert.equal(result.length, 3, "3 divs");
        assert.equal(result[0].tagName, "DIV", "is DIV");
        assert.equal(result[0].childNodes[0].nodeValue, "1", "test is '1'");
        assert.equal(result[1].tagName, "DIV", "is DIV");
        assert.equal(result[1].childNodes[0].nodeValue, "2", "test is '2'");
        assert.equal(result[2].tagName, "DIV", "is DIV");
        assert.equal(result[2].childNodes[0].nodeValue, "3", "test is '3'");
        done();
    }
    annotation.getAnnotationsForEntity(1337, databaseRequest, xslPromise).then(asyncTestCase).catch((error) => {
        console.log(error);
        assert.ok(false, "failure!");
        done();
    });
});

QUnit.test("async/await behavior", (assert) => {
    let af = async function() {
        return await "returnval";
    };

    let done = assert.async(3);
    af().then((result) => {
        assert.equal(result, "returnval", "af() called once.");
        done();
    });
    af().then((result) => {
        assert.equal(result, "returnval", "af() called twice.");
        done();
    });

    let er = async function() {
        throw "asyncerror";
    };

    er().catch((error) => {
        assert.equal(error, "asyncerror", "er() called.");
        done();
    });
});

QUnit.test("convertNewCommentForm", function(assert) {
    assert.ok(annotation.convertNewCommentForm, "function exists.");
    assert.equal(xml2str(annotation.convertNewCommentForm(annotation.createNewCommentForm(2345))), "<Insert><Record><Parent name=\"CommentAnnotation\"/><Property name=\"comment\"/><Property name=\"annotationOf\">2345</Property></Record></Insert>", "conversion ok.");
});


QUnit.test("convertNewCommentResponse", function(assert) {
    let convertNewAnnotationResponse = annotation.convertNewCommentResponse;
    assert.ok(convertNewAnnotationResponse, "function exists.");
    let done = assert.async();
    let testResponse = '<Response><Record><Property name="annotationOf"/><History transaction="INSERT" datetime="2015-12-24T20:15:00" username="someuser"/><Property name="comment">This is a comment</Property></Record></Response>';
    let expectedResult = "<li xmlns=\"http://www.w3.org/1999/xhtml\" class=\"list-group-item\"><div class=\"media\"><div class=\"media-left\"><h3>»</h3></div><div class=\"media-body\"><h4 class=\"media-heading\">someuser<small><i> posted on 2015-12-24T20:15:00</i></small></h4><p class=\"caosdb-comment-annotation-text\">This is a comment</p></div></div></li>";
    convertNewAnnotationResponse(str2xml(testResponse), annotation.loadAnnotationXsl("../../")).then(function(result) {
        assert.equal(result.length, 1, "one element returned.");
        assert.equal(xml2str(result[0]), expectedResult, "result converted correctly");
        done();
    }, function(error) {
        console.log(error);
        assert.ok(false, "see console.log");
        done();
    });
});

QUnit.test("getEntityId", function(assert) {
    let annotationSection = $('<div data-entity-id="dfgh"/>')[0];
    assert.ok(annotation.getEntityId, "function exists.");
    assert.equal(annotation.getEntityId($('<div/>')[0]), null, "no data-entity-id attribute returns null");
    assert.equal(annotation.getEntityId(annotationSection), "dfgh", "returns correct entityId.");
    assert.equal(annotation.getEntityId(), null, "no param returns null.");
});

QUnit.test("createNewCommentForm", function(assert) {
    let createNewCommentForm = annotation.createNewCommentForm;
    assert.ok(createNewCommentForm, "function exists.");
    assert.equal(createNewCommentForm(1234).tagName, "FORM", "returns form");
    assert.equal(createNewCommentForm(1234).elements["annotationOf"].value, "1234", "annotationOf there");
    assert.equal($(createNewCommentForm(1234)).find("button[type='submit']")[0].name, "submit", "has submit button");
    assert.equal($(createNewCommentForm(1234)).find("button[type='submit']")[0].innerHTML, "Submit", "is labled 'Submit'");
    assert.equal($(createNewCommentForm(1234)).find("button[type='reset']")[0].name, "cancel", "has cancel button");
    assert.equal($(createNewCommentForm(1234)).find("button[type='reset']")[0].innerHTML, "Cancel", "is labled 'Cancel'");
    assert.equal($(createNewCommentForm(1234)).find("button[type='asdf']")[0], null, "no asdf button");
});

QUnit.test("getNewCommentButton", function(assert) {
    assert.ok(annotation.getNewCommentButton, "function exists.");
    assert.equal(annotation.getNewCommentButton($('<div/>')[0]), null, "not present");
    assert.equal(annotation.getNewCommentButton($('<div><button class="otherclass"/></div>')[0]), null, "not present");
    assert.equal(annotation.getNewCommentButton($('<div><button id="asdf" class="caosdb-new-comment-button"/></div>')[0]).id, "asdf", "button found.");
    assert.equal(annotation.getNewCommentButton(), null, "no parameter");
    assert.equal(annotation.getNewCommentButton(null), null, "null parameter");
});

QUnit.test("createPleaseWaitNotification", function(assert) {
    assert.ok(annotation.createPleaseWaitNotification, "function exists.");
    assert.ok($(annotation.createPleaseWaitNotification()).hasClass("caosdb-please-wait-notification"), "has class caosdb-please-wait-notification");
});

QUnit.test("getNewCommentForm", function(assert) {
    let annotationSection = $('<div><form id="sdfg" class="caosdb-new-comment-form"></form></div>')[0];
    assert.ok(annotation.getNewCommentForm, "function exists");
    assert.equal(annotation.getNewCommentForm(annotationSection).id, "sdfg", "NewCommentForm found.");
    assert.equal(annotation.getNewCommentForm(), null, "no param returns null");
});

QUnit.test("validateNewCommentForm", function(assert) {
    assert.ok(annotation.validateNewCommentForm, "function exists.");
    let entityId = "asdf";
    let form = annotation.createNewCommentForm(entityId);

    assert.throws(annotation.validateNewCommentForm, "no param throws exc.");
    assert.equal(annotation.validateNewCommentForm(form), false, "empty returns false");
    form.newComment.value = "this is a new comment";
    assert.equal(annotation.validateNewCommentForm(form, 50), false, "to short returns false");
    assert.equal(annotation.validateNewCommentForm(form), true, "long enough returns true");
});

QUnit.test("getPleaseWaitNotification", function(assert) {
    assert.ok(annotation.getPleaseWaitNotification, "function exists");
    assert.equal(annotation.getPleaseWaitNotification(), null, "no param returns null");
    assert.equal(annotation.getPleaseWaitNotification($('<div><div class="blablabla" id="asdf"></div></div>')[0]), null, "does not exist");
    assert.equal(annotation.getPleaseWaitNotification($('<div><div class="caosdb-please-wait-notification" id="asdf"></div></div>')[0]).id, "asdf", "found.");
});

QUnit.test("NewCommentApp exception", function(assert) {
    try {
        var original = annotation.createNewCommentForm;
        annotation.createNewCommentForm = function() {
            throw new TypeError("This is really bad!");
        }

        let annotationSection = $('<div data-entity-id="tzui"><button class="caosdb-new-comment-button"></button></div>')[0];
        let app = annotation.initNewCommentApp(annotationSection);
        app.openForm();

        let done = assert.async();
        setTimeout(() => {
            assert.equal(app.state, "read");
            done();
        }, 2000);
    } finally {
        // clean up
        annotation.createNewCommentForm = original
    }
});

QUnit.test("convertNewCommentResponse error", function(assert) {
    let errorStr = '<Response username="tf" realm="PAM" srid="dc1df091045eca7bd6940b88aa6db5b6" timestamp="1499814014684" baseuri="https://baal:8444/mpidsserver" count="1">\
        <Error code="12" description="One or more entities are not qualified. None of them have been inserted/updated/deleted." />\
        <Record>\
        <Error code="114" description="Entity has unqualified properties." />\
        <Warning code="0" description="Entity has no name." />\
        <Parent name="CommentAnnotation">\
        <Error code="101" description="Entity does not exist." />\
        </Parent>\
        <Property name="comment" importance="FIX">\
        sdfasdfasdf\
        <Error code="101" description="Entity does not exist." />\
        <Error code="110" description="Property has no datatype." />\
        </Property>\
        <Property name="annotationOf" importance="FIX">\
        20\
        <Error code="101" description="Entity does not exist." />\
        <Error code="110" description="Property has no datatype." />\
        </Property>\
        </Record>\
        </Response>';

    let done = assert.async();
    let expectedResult = "<divxmlns=\"http://www.w3.org/1999/xhtml\"class=\"alertalert-dangercaosdb-new-comment-erroralert-dismissable\"><buttonclass=\"close\"data-dismiss=\"alert\"aria-label=\"close\">×</button><strong>Error!</strong>Thiscommenthasnotbeeninserted.<pclass=\"small\"><pre><code>&lt;record&gt;&lt;errorcode=\"114\"description=\"Entityhasunqualifiedproperties.\"&gt;&lt;/error&gt;&lt;warningcode=\"0\"description=\"Entityhasnoname.\"&gt;&lt;/warning&gt;&lt;parentname=\"CommentAnnotation\"&gt;&lt;errorcode=\"101\"description=\"Entitydoesnotexist.\"&gt;&lt;/error&gt;&lt;/parent&gt;&lt;propertyname=\"comment\"importance=\"FIX\"&gt;sdfasdfasdf&lt;errorcode=\"101\"description=\"Entitydoesnotexist.\"&gt;&lt;/error&gt;&lt;errorcode=\"110\"description=\"Propertyhasnodatatype.\"&gt;&lt;/error&gt;&lt;/property&gt;&lt;propertyname=\"annotationOf\"importance=\"FIX\"&gt;20&lt;errorcode=\"101\"description=\"Entitydoesnotexist.\"&gt;&lt;/error&gt;&lt;errorcode=\"110\"description=\"Propertyhasnodatatype.\"&gt;&lt;/error&gt;&lt;/property&gt;&lt;/record&gt;</code></pre></p></div>";
    annotation.convertNewCommentResponse(str2xml(errorStr), annotation.loadAnnotationXsl("../../")).then(function(result) {
        assert.equal(xml2str(result[0]).replace(/[\t\n\ ]/g, ""), expectedResult.replace(/[\t\n\ ]/g, ""), "transformed into an error div.");
        done();
    }, function(error) {
        console.log(error);
        assert.ok(false, "see console.log");
        done();
    });
})

QUnit.test("NewCommentApp convertNewCommentResponse", function(assert) {
    var done = assert.async(2);
    var original = annotation.convertNewCommentResponse;
    annotation.convertNewCommentResponse = function(xmlPromise, xslPromise) {
        done(1); // was called;
        return original(xmlPromise, xslPromise);
    }
    let originalPost = annotation.postCommentXml;
    annotation.postCommentXml = function(xml) {
        let testResponse = '<Response><Record><Property name="annotationOf"/><History transaction="INSERT" datetime="2015-12-24T20:15:00" username="someuser"/><Property name="comment">This is a comment</Property></Record></Response>';
        return new Promise(resolve => setTimeout(resolve, 1000, str2xml(testResponse)));
    }

    // prepare app
    var annotationSection = $('<div data-entity-id="tzui"><button class="caosdb-new-comment-button"></button></div>')[0];
    var app = annotation.initNewCommentApp(annotationSection);
    // prepare form
    app.openForm();
    var form = annotation.getNewCommentForm(annotationSection);
    form.newComment.value = "This is a new comment qwerasdf.";

    app.observe("onEnterRead", () => {
        assert.equal(app.state, "read", "finally in read state");
        assert.equal($(annotationSection).find(".caosdb-comment-annotation-text")[0].innerHTML, "This is a comment", "new comment appended.");

        let button = annotation.getNewCommentButton(annotationSection);
        assert.ok(button.onclick, "button click is enabled.");
        assert.notOk($(button).hasClass('disabled'), "button is not visually disabled.");
        assert.equal(annotation.getNewCommentForm(annotationSection), null, "form is not there");
        assert.equal(annotation.getPleaseWaitNotification(annotationSection), null, "please wait is not there");

        done();
    });
    app.submitForm(form);

    // clean up
    annotation.convertNewCommentResponse = original;
    annotation.postCommentXml = originalPost;
});

QUnit.test("NewCommentApp convertNewCommentForm/postCommentXml", function(assert) {
    let done = assert.async(2);

    // do not actually post the comment, just wait 1 second and return
    // something.
    let originalPost = annotation.postCommentXml;
    annotation.postCommentXml = function(xml) {
        assert.equal(xml2str(xml), "<Insert><Record><Parent name=\"CommentAnnotation\"/><Property name=\"comment\">This is a new comment qwerasdf.</Property><Property name=\"annotationOf\">tzui</Property></Record></Insert>", "the conversion was sucessful");
        done(2); // postCommentXml was called
        return new Promise(resolve => setTimeout(resolve, 1000, str2xml("<Response/>")));
    }


    // prepare app
    let annotationSection = $('<div data-entity-id="tzui"><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.onBeforeReceiveResponse = null; // because the response is empty.
    // prepare form
    app.openForm();
    let form = annotation.getNewCommentForm(annotationSection);
    form.newComment.value = "This is a new comment qwerasdf.";

    let originalConvert = annotation.convertNewCommentForm;
    annotation.convertNewCommentForm = function(sendform) {
        assert.ok(sendform == form, "form is still the same");
        done(1); // convertNewCommentForm was called
        return originalConvert(sendform);
    }

    app.submitForm(form);
    assert.equal(app.state, "send", "in send state");


    // clean up
    annotation.convertNewCommentForm = originalConvert;
    annotation.postCommentXml = originalPost;
});

QUnit.test("NewCommentApp waitingNotification", function(assert) {
    // prepare app
    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.onBeforeReceiveResponse = null; // because the response is empty
    // prepare form
    app.openForm();
    let form = annotation.getNewCommentForm(annotationSection);
    form.newComment.value = "This is a new comment";


    $(form).submit();
    assert.equal(app.state, "send", "in send state");
    assert.equal(annotation.getPleaseWaitNotification(annotationSection).className, "caosdb-please-wait-notification", "please wait is there");
});


QUnit.test("NewCommentApp form.onsubmit", function(assert) {
    let done = assert.async(2);

    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    // add to body, otherwise click event would not be working
    $(document.body).append(annotationSection);
    let app = annotation.initNewCommentApp(annotationSection);
    app.onEnterSend = null; // remove form submission

    app.openForm();

    let form = annotation.getNewCommentForm(annotationSection);
    let submitButton = annotation.getSubmitNewCommentButton(annotationSection);

    // test with empty form -> rejected
    app.observe("onBeforeTransition", function(e) {
        done("1&2");
    });

    assert.equal(app.state, "write", "before in write state");
    submitButton.click();
    assert.equal(app.state, "write", "after emtpy submit still in write state");

    form.newComment.value = "This is a new comment";
    submitButton.click();
    assert.equal(app.state, "send", "after non-empty submit in send state");


    // clean up
    $(annotationSection).remove();
});

QUnit.test("NewCommentApp form.onreset", function(assert) {
    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.openForm();

    let done = assert.async();
    assert.equal(annotation.getNewCommentForm(annotationSection).className, "caosdb-new-comment-form", "form is there");
    app.observe("onBeforeCancelForm", function(e) {
        assert.equal(e.from, "write", "leaving write state");
        done();
    });

    // add to body since the click event isn't working otherwise (damn!)
    $(document.body).append(annotationSection);

    annotation.getCancelNewCommentButton(annotationSection).click();
    assert.equal(annotation.getNewCommentForm(annotationSection), null, "form is not there");

    app.openForm();
    assert.equal($(annotationSection).find('form').length, 1, "form is not duplicated")

    // clean up
    $(annotationSection).remove();
});

QUnit.test("getCancelNewCommentButton", function(assert) {
    let annotationSection = $('<div><form class="caosdb-new-comment-form"><button id="fghj" type="reset"/></form></div>')[0];
    assert.ok(annotation.getCancelNewCommentButton, "function exists.");
    assert.equal(annotation.getCancelNewCommentButton(), null, "no param returns null");
    assert.equal(annotation.getCancelNewCommentButton(null), null, "null param returns null");
    assert.equal(annotation.getCancelNewCommentButton(annotationSection).id, "fghj", "returns correctly");
    assert.equal(annotation.getCancelNewCommentButton($('<div><form class="caosdb-new-comment-form"><button type="submit"/></form></div>')[0]), null, "button does not exist");
});

QUnit.test("getSubmitNewCommentButton", function(assert) {
    let annotationSection = $('<div><form class="caosdb-new-comment-form"><button id="fghj" type="submit"/></form></div>')[0];
    assert.ok(annotation.getSubmitNewCommentButton, "function exists.");
    assert.equal(annotation.getSubmitNewCommentButton(), null, "no param returns null");
    assert.equal(annotation.getSubmitNewCommentButton(null), null, "null param returns null");
    assert.equal(annotation.getSubmitNewCommentButton(annotationSection).id, "fghj", "returns correctly");
    assert.equal(annotation.getSubmitNewCommentButton($('<div><form class="caosdb-new-comment-form"><button type="reset"/></form></div>')[0]), null, "button does not exist");
});

QUnit.test("NewCommentApp newCommentButton.onclick", function(assert) {
    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    let button = annotation.getNewCommentButton(annotationSection);

    assert.equal(app.state, "read", "in read state");
    assert.ok(button.onclick, "button click is enabled.");
    assert.notOk($(button).hasClass('disabled'), "button is not visually disabled.");
    assert.equal(annotation.getNewCommentForm(annotationSection), null, "form is not there");

    $(button).click();
    assert.equal(app.state, "write", "after click in write state");
    assert.equal(button.onclick, null, "button click is disabled");
    assert.ok($(button).hasClass("disabled"), "button is visually disabled.")
    assert.ok(annotation.getNewCommentForm(annotationSection), "form is there");
    assert.equal(annotation.getNewCommentForm(annotationSection).parentNode.className, "list-group-item", "form is wrapped into list-group-item");
});

QUnit.test("NewCommentApp transitions", function(assert) {
    assert.throws(annotation.initNewCommentApp, "null parameter throws exc.");

    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.onBeforeSubmitForm = null; // remove validation
    app.onEnterSend = null; // remove form submission
    app.onLeaveWrite = null; // remove form deletion
    app.onBeforeReceiveResponse = null; // remove appending the result

    assert.ok(app, "app ok");
    assert.equal(app.state, "read", "initial state is read");
    app.openForm();
    assert.equal(app.state, "write", "open form -> state write");
    app.cancelForm();
    assert.equal(app.state, "read", "cancel -> state read");
    app.openForm();
    assert.equal(app.state, "write", "open form -> state write");
    app.submitForm();
    assert.equal(app.state, "send", "submit -> state send");
    app.receiveResponse();
    assert.equal(app.state, "read", "receiveRequest -> state read");
    app.resetApp("no error");
    assert.equal(app.state, "read", "reset -> state read");
});

QUnit.test("annotation module", function(assert) {
    assert.ok(annotation, "module exists.");
    assert.ok(annotation.createNewCommentForm, "createNewCommentForm exists.");
    assert.ok(annotation.initNewCommentApp, "initNewCommentApp exists.");
});

/* MISC FUNCTIONS */
