/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the welcome screen */

/* SETUP */
QUnit.module("welcome.xsl", {
	beforeEach : function(assert) {
		// load welcome.xsl
		var done = assert.async();
		var qunit_obj = this;
		$.ajax({
			cache : true,
			dataType : 'xml',
			url : "xsl/welcome.xsl",
		}).done(function(data, textStatus, jdXHR) {
			qunit_obj.welcomeXSL = data;
		}).always(function() {
			done();
		});
	}
});

/* TESTS */
QUnit.test("availability", function(assert) {
	assert.ok(this.welcomeXSL);
})

QUnit.test("squarefield template available, produces div.caosdb-square which has actually width=height.",function(assert){
	// inject an entrance rule
	var entry_t = this.welcomeXSL.createElement("xsl:template");
	this.welcomeXSL.firstElementChild.appendChild(entry_t);
	entry_t.outerHTML = '<xsl:template match="/"><div style="width: 145px;"><xsl:call-template name="squarefield"><xsl:with-param name="content">content</xsl:with-param></xsl:call-template></div></xsl:template>';

	// check template works at all
	var xsl = this.welcomeXSL;
	var xml = str2xml("<root/>");
	var html = xslt(xml,xsl);
	assert.ok(html,"document fragment exists");
	
	// check output
	var match = xml2str(html).match(/^<div.*><div class="caosdb-square"><div class="caosdb-square-content">content<\/div><\/div><\/div>$/);
	assert.ok(match,"div created");
	
	// check actual size
	document.body.appendChild(html);

	var square_e = $(".caosdb-square")[0];
	assert.equal(square_e.offsetWidth,"145","width is 145px");
	assert.equal(square_e.offsetHeight,"145","height is 145px");
	
	var elem =document.getElementsByClassName("caosdb-square")[0].parentNode; 
	elem.parentNode.removeChild(elem);
});


/* MISC FUNCTIONS */
