/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the html elements for annotations of entities via xsl transformation */

/* SETUP */
QUnit.module("annotation.xsl", {
	before : function(assert) {
		// load entity.xsl
		var done = assert.async();
		var qunit_obj = this;
		annotation.loadAnnotationXsl("../../").then(function(data){
			qunit_obj.annotationXSL = data;
			done();
		});
		
		
		this.testCases = [];
		this.testCases[0] = '<Response><Record><Property name="annotationOf"/><History transaction="INSERT" datetime="2015-12-24T20:15:00" username="someuser"/><Property name="comment">This is a comment</Property></Record><Record><Property name="annotationOf"/></Record></Response>';
		assert.ok(str2xml(this.testCases[0]));
	}
});

QUnit.test("Root rule returns DIV element", function(assert){
	var xml_str = this.testCases[0];
	var xml = str2xml(xml_str);
	
	assert.equal(xslt(xml, this.annotationXSL).firstChild.tagName, "DIV");
});

QUnit.test("Record rule returns li elements", function(assert){
	var xml_str = this.testCases[0];
	var xml = str2xml(xml_str);
	
	var annos = xslt(xml, this.annotationXSL).firstChild.children;
//	assert.equal(annossec.length, 1, "one child");
//	assert.equal(annossec[0].tagName, "UL", "annossec is ul");
//	var annos = annossec[0].children;
	assert.equal(annos.length, 2, "two li");
	assert.equal(annos[0].tagName, "LI", "1st");
	assert.equal(annos[0].className, "list-group-item", "heading found");
	assert.equal(annos[0].children.length, 1, "1st has one child.");
	assert.equal(annos[1].tagName, "LI", "2nd");

	var media = annos[0].children[0];
	assert.equal(media.tagName, "DIV", "is DIV");
	assert.equal(media.className, "media", "className is media");
	assert.equal(media.children.length, 2, "media has two children");
	assert.equal(media.children[0].className, "media-left");
	assert.equal(media.children[1].className, "media-body");
	
});

QUnit.test("History element", function(assert){
	var xml_str = this.testCases[0];
	var xml = str2xml(xml_str);
	
	var html = xslt(xml, this.annotationXSL);
	var mediaBody = html.firstChild.getElementsByClassName("media-body")[0];
	assert.ok(mediaBody, "media-body is there");
	assert.ok(mediaBody.children.length>0,"media-body has children");
	var mediaHeading = mediaBody.getElementsByClassName("media-heading")[0];
	assert.ok(mediaHeading, "media-heading is there");
	assert.equal(mediaHeading.parentNode, mediaBody, "media-heading is child of media-body");
	
	assert.ok(xml2str(mediaHeading).indexOf("someuser")!==-1, "username is there");
	assert.ok(xml2str(mediaHeading).indexOf("2015-12-24T20:15:00")!==-1, "datetime is there");
});

QUnit.test("Comment text", function(assert){
	var xml_str = this.testCases[0];
	var xml = str2xml(xml_str);
	
	var html = xslt(xml, this.annotationXSL);
	
	var mediaBody = html.firstChild.getElementsByClassName("media-body")[0];
	assert.ok(mediaBody, "media-body is there");
	assert.ok(mediaBody.children.length>0,"media-body has children");
	var commentText = mediaBody.getElementsByClassName("caosdb-comment-annotation-text")[0];
	assert.ok(commentText, "comment-text element is there");
	assert.equal(commentText.children.length + commentText.childNodes.length, 1, "comment-text element has one child.");
	assert.equal(commentText.innerHTML, "This is a comment", "comment text is correct.");
});
