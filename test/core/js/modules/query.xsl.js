/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the query panel via xsl transformation */

/* SETUP */
QUnit.module("query.xsl", {
    before: function(assert) {
        // load query.xsl
        var done = assert.async();
        var qunit_obj = this;
        $.ajax({
            cache: true,
            dataType: 'xml',
            url: "xsl/query.xsl",
        }).done(function(data, textStatus, jdXHR) {
            insertParam(data, "entitypath", "/entitypath/");
            qunit_obj.queryXSL = data;
        }).always(function() {
            done();
        });
    }
});

/* TESTS */
QUnit.test("availability", function(assert) {
    assert.ok(this.queryXSL);
});

QUnit.test("basic properties of select-table feature", function(assert) {
    assert.equal(this.queryXSL.getElementsByClassName("caosdb-select-table")[0].tagName, "div", "xsl sheet defines a caosdb-select-table div");
    var xsl = injectTemplate(this.queryXSL, '<xsl:template match="/"><xsl:apply-templates select="Selection" mode="select-table"/></xsl:template>');
    var xml = str2xml("<Selection/>");
    var html = xslt(xml, xsl);
    assert.equal(html.firstElementChild.tagName, "DIV", "first child is div.");
    assert.equal(html.firstElementChild.className, "panel panel-default caosdb-select-table", "first child has class caosdb-select-table.");
});

/* Test table of empty result sets of queries */


QUnit.test("empty results in SELECT create no table", function(assert) {
    var testresult = '\
<?xml version="1.0" encoding="UTF-8"?>\
<Response count="0">\
  <Query string="select test from bla" results="0">\
    <ParseTree>(cq select (prop_sel (prop_subsel (selector_txt   t e s t  ))) from (entity bla) &lt;EOF&gt;)</ParseTree>\
    <Role />\
    <Entity>bla</Entity>\
    <Selection>\
      <Selector name="test" />\
    </Selection>\
  </Query>\
</Response>\
';
    var xml = str2xml(testresult);
    var html = applyTemplates(xml, this.queryXSL, "query-results");
    assert.equal(html.children.length, 1);
    // For select queries actually containing results, this would be 2.
});

// This uses results from the demo server.
QUnit.test("one result in SELECT create table", function(assert) {
    var testresult = '\
<?xml version="1.0" encoding="UTF-8"?>\
<Response count="12">\
  <Query string="SELECT Width, Result, Success FROM MySimulation" results="1">\
    <ParseTree>(cq SELECT (prop_sel (prop_subsel (selector_txt   W i d t h)) , (prop_subsel (selector_txt   R e s u l t)) , (prop_subsel (selector_txt   S u c c e s s  ))) FROM (entity MySimulation) &lt;EOF&gt;)</ParseTree>\
    <Role />\
    <Entity>MySimulation</Entity>\
    <Selection>\
      <Selector name="Width" />\
      <Selector name="Result" />\
      <Selector name="Success" />\
    </Selection>\
  </Query>\
  <Record id="243">\
    <Permissions />\
    <Property id="235" name="Width" datatype="INTEGER" importance="FIX" unit="px">\
      200\
      <Permissions />\
    </Property>\
    <Property id="237" name="Result" datatype="DOUBLE" importance="FIX">\
      1.25\
      <Permissions />\
    </Property>\
    <Property id="238" name="Success" datatype="BOOLEAN" importance="FIX">\
      TRUE\
      <Permissions />\
    </Property>\
    </Record>\
    </Response>';
    var xml = str2xml(testresult);
    var html = applyTemplates(xml, this.queryXSL, "query-results");
    assert.equal(html.children.length, 2);
});

/* --  -- */

QUnit.test("Query tag is transformed via xslt", function(assert) {
    assert.equal(this.queryXSL.getElementsByClassName("caosdb-query-response")[0].tagName, "div", "xsl sheet defines a caosdb-query response div");

    let html = applyTemplates(str2xml('<Query/>'), this.queryXSL, 'query-results');
    //var html = xslt(xml, xsl);
    assert.equal(html.firstElementChild.tagName, "DIV", "first child is div.");
    assert.equal(html.firstElementChild.className, "panel panel-default caosdb-query-response", "first child has class caosdb-query-reponse.");
});

QUnit.test("xsl defines id 'caosdb-query-form'", function(assert) {
    assert.ok(this.queryXSL.getElementById("caosdb-query-form"));
});

QUnit.test("xsl defines id 'caosdb-query-form' with tagName=form", function(assert) {
    assert.equal(this.queryXSL.getElementById("caosdb-query-form").tagName, "form");
});

QUnit.test("xsl script's 'caosdb-query-form' has a hidden input, with name=P and value=0L10", function(assert) {
    var e = this.queryXSL.getElementById("caosdb-query-form").children[1];
    assert.equal(e.tagName, "input", "tagName = input");
    assert.equal(e.getAttribute("name"), "P", "name = P");
    assert.equal(e.getAttribute("value"), "0L10", "value = 0L10");
});


QUnit.test("Query is available, contained by a div.", function(assert) {
    var cont = getQueryFormContainer(this.queryXSL);
    assert.equal(cont.tagName, "DIV", "contained by a div");
    assert.equal(cont.className, "container caosdb-query-panel", "container has classname 'container caosdb-query-panel'");
    assert.equal(cont.firstElementChild.tagName, "FORM", "form element is available");
    assert.equal(cont.firstElementChild.className, "panel", "FORM has class 'panel'");
    assert.equal(cont.firstElementChild.id, "caosdb-query-form", "FORM has id 'caosdb-query-form'");
});
QUnit.test("Query is send with a paging of 0L10 by default", function(assert) {
    var form_e = getQueryForm(this.queryXSL);

    var input_e = form_e.firstElementChild;
    assert.equal(input_e.tagName, "INPUT", "input there.");
    assert.equal(input_e.getAttribute("type"), "hidden", "input is hidden");
    assert.equal(input_e.getAttribute("name"), "P", "name = P");
    assert.equal(input_e.getAttribute("value"), "0L10", "value = 0L10");
    assert.equal(input_e.id, "caosdb-query-paging-input", "id = caosdb-query-paging-input");
});

QUnit.test("Query form has action attribute", function(assert) {
    var form_e = getQueryForm(this.queryXSL);
    assert.equal(form_e.getAttribute("action"), "/entitypath/");
});

QUnit.test("template entity-link", function(assert){
    let link = callTemplate(this.queryXSL, "entity-link", {"entity-id": "asdf"});
    assert.equal(link.firstElementChild.tagName, "A", "tagName = A");
    assert.equal(link.firstElementChild.getAttribute("href"), "/entitypath/asdf", "href is /entitypath/asdf");
});

QUnit.test("template select-table-row ", function(assert){
    let row = callTemplate(this.queryXSL, "select-table-row", {"entity-id": "sdfg"}, str2xml('<Response>'));
    assert.equal(row.firstElementChild.tagName, "TR", "tagName = TR");
    assert.equal(row.firstElementChild.firstElementChild.tagName, "TD", "tagName = TD");
    assert.equal(row.firstElementChild.firstElementChild.firstElementChild.tagName, "A", "tagName = A");
});

/* MISC FUNCTIONS */
function getQueryForm(queryXSL) {
    var cont = getQueryFormContainer(queryXSL);
    return cont.getElementsByTagName("form")[0];
}

function getQueryFormContainer(queryXSL) {
    var xsl = injectTemplate(queryXSL, '<xsl:template match="/"><xsl:call-template name="caosdb-query-panel"/></xsl:template>"')
    var xml = str2xml("<root/>");
    var html = xslt(xml, xsl);
    return html.firstElementChild;
}
