/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the html elements for entities via xsl transformation */

/* SETUP */
QUnit.module("entity.xsl", {
    before: function(assert) {
        // load entity.xsl
        var done = assert.async();
        var qunit_obj = this;
        _retrieveEntityXSL().then(function(xsl) {
            qunit_obj.entityXSL = xsl
            done();
        });
    }
});

async function _retrieveEntityXSL() {
    var entityXsl = await transformation.retrieveXsltScript("entity.xsl");
    var commonXsl = await transformation.retrieveXsltScript("common.xsl");
    var xsl = transformation.mergeXsltScripts(entityXsl, [commonXsl]);
    insertParam(xsl, "entitypath", "/entitypath/");
    insertParam(xsl, "filesystempath", "/filesystempath/");
    return xsl;
}


/* TESTS */
QUnit.test("availability", function(assert) {
    assert.ok(this.entityXSL);
});

QUnit.test("Property names are not links anymore", function(assert) {
    var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-collapsed"><xsl:with-param name="collapseid" select="5678"/></xsl:apply-templates></xsl:template>');
    var xml_str = '<Property name="pname" id="2345" datatype="TEXT">pvalue</Property>';
    var xml = str2xml(xml_str);
    var html = xslt(xml, xsl);
    assert.equal(html.firstElementChild.getElementsByClassName("caosdb-property-name")[0].outerHTML, '<strong class=\"caosdb-property-name\">pname</strong>', "link there");
});

// QUnit.test("parent name is bold link", function(assert) {
//     // make this xsl sheet accessible
//     let html = applyTemplates(str2xml('<Parent name="TestParent" id="1234" description="DESC"/>'), this.entityXSL, 'entity-body');
//     assert.ok(html);

//     var name_e = html.firstElementChild.getElementsByClassName("caosdb-parent-name")[0];
//     assert.ok(name_e, "element is there");
//     assert.equal(name_e.tagName, "A", "is link");
//     assert.equal(name_e.getAttribute("href"), "/entitypath/1234", "href location");
//     assert.equal(window.getComputedStyle(name_e)["font-weight"], "700", "font is bold");
// });

QUnit.test("TestRecordType data type is recognized as a reference", function(assert) {
    // inject an entrance rule
    var xsl = getXSLScriptClone(this.entityXSL);
    var entry_t = xsl.createElement("xsl:template");
    xsl.firstElementChild.appendChild(entry_t);
    entry_t.outerHTML = '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-value"/></xsl:template>';

    var xml_str = '<Property name="TestProperty" id="1234" description="DESC" type="TestRecordType">5678</Property>';
    var xml = str2xml(xml_str);
    var params = {
        entitypath: "/entitypath/"
    };
    html = xslt(xml, xsl, params);
    assert.ok(html, "html is ok.");

    var link_e = html.firstElementChild;
    assert.equal(link_e.tagName, "A", "<a> tag is there.");
    assert.equal(link_e.getAttribute("href"), "/entitypath/5678", "href location");
});

QUnit.test("Property with Permissions tag and type='ExperimentSeries' is recognized as a reference", function(assert) {
    // inject an entrance rule
    var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-value"/></xsl:template>');

    var xml_str = '<Property id="129954" name="ExperimentSeries" description="A collection of [Experiment] records which have the same [aim]." type="ExperimentSeries" importance="FIX">' +
        '\n129950' +
        '\n<Permissions>' +
        '\n<Permission name="USE:AS_REFERENCE" />' +
        '\n<Permission name="UPDATE:QUERY_TEMPLATE_DEFINITION" />' +
        '\n</Permissions>' +
        '\n</Property>';
    var xml = str2xml(xml_str);
    var params = {
        entitypath: "/entitypath/"
    };
    var html = xslt(xml, xsl, params);
    assert.ok(html, "html is ok.");

    var link_e = html.firstElementChild;
    assert.equal(link_e.tagName, "A", "<a> tag is there.");
    assert.equal(link_e.getAttribute("href"), "/entitypath/129950", "href location");
});

QUnit.test("File path is not converted to lower case.", function(assert) {
    let xml_str = '<File path="UPPERCASE.JPG" id="1234"></File>';
    let xml = str2xml(xml_str);
    let html = applyTemplates(xml, this.entityXSL, 'entities');

    // inject an entrance rule
    //var xsl_tmp = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="File" mode="top-level-data"/></xsl:template>');


    //var params = {
    //    entitypath: "/entitypath/",
    //     filesystempath: "/filesystempath/"
    //};
    //var html = xslt(xml, xsl, params);
    assert.ok(html, "html is ok.");

    var link_e = html.firstElementChild.getElementsByTagName("IMG")[0]
    assert.ok(link_e, "<img> tag is there.");
    assert.equal(link_e.getAttribute("src"), "/filesystempath/UPPERCASE.JPG", "src location is UPPERCASE.JPG");
});

QUnit.test("back references", function(assert) {
    // inject an entrance rule
    var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="*/@id" mode="backreference-link"/></xsl:template>');

    var xml_str = '<Entity id="265"/>';
    var xml = str2xml(xml_str);

    var link = xslt(xml, xsl).firstChild;

    assert.equal(link.tagName, "A", "is a link");
    assert.equal(link.getAttribute("href"), "/entitypath/?P=0L10&query=FIND+Entity+which+references+265", "query correct.");
});

QUnit.test("Entities have a caosdb-annotation-section", function(assert) {
    let xml_str = '<Record id="2345"></Record>';
    let xml = str2xml(xml_str)
    let html = applyTemplates(xml, this.entityXSL, "entities");
    // inject an entrance rule
    //var xsl = injectTemplate(this.entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Record" mode="top-level-data"/></xsl:template>');

    let secs = html.firstChild.getElementsByClassName("caosdb-annotation-section");
    assert.equal(secs.length, 1, "found one.");
    assert.equal(secs[0].tagName, "UL", "section element is DIV");

    assert.equal(secs[0].getAttribute("data-entity-id"), "2345", "data-entity-id is correct.");
});

QUnit.test("LIST Property", function(assert) {
    var done = assert.async();
    var entityXSL = this.entityXSL;
    assert.expect(2);
    $.ajax({
        cache: true,
        dataType: 'xml',
        url: "xml/test_case_list_of_myrecordtype.xml",
    }).done(function(data, textStatus, jdXHR) {
        var xsl = injectTemplate(entityXSL, '<xsl:template match="/"><xsl:apply-templates select="Property" mode="property-value"/></xsl:template>');
        var params = {
            entitypath: "/entitypath/"
        };
        var ret = xslt(data, xsl, params);
        assert.ok(ret);
        assert.equal(ret.firstChild.className, "caosdb-value-list", "property value contains a list.")
    }).always(function() {
        done();
    });
});

QUnit.test("single-value template with reference property.", function(assert) {
    assert.equal(xml2str(callTemplate(this.entityXSL, 'single-value', {
        'value': '',
        'reference': 'true',
        'boolean': 'false'
    })), "", "empty value produces nothing.");
    let link = callTemplate(this.entityXSL, 'single-value', {
        'value': '1234',
        'reference': 'true',
        'boolean': 'false'
    }).firstElementChild;
    assert.equal(link.tagName, 'A', "is link");
    assert.equal(link.getAttribute('href'), "/entitypath/1234", 'link to 1234');
    assert.equal($(link).find('.caosdb-id').length, 1, 'has caosdb-id span');
})

/* MISC FUNCTIONS */
function applyTemplates(xml, xsl, mode, select = "*") {
    let entryRule = '<xsl:template priority="9" match="/"><xsl:apply-templates select="' + select + '" mode="' + mode + '"/></xsl:template>';
    let modXsl = injectTemplate(xsl, entryRule);
    return xslt(xml, modXsl);
}

function callTemplate(xsl, template, params) {
    let entryRuleStart = '<xsl:template priority="9" match="/"><xsl:call-template name="' + template + '">';
    let entryRuleEnd = '</xsl:call-template></xsl:template>';
    var entryRule = entryRuleStart;
    for (name in params) {
        entryRule += '<xsl:with-param name="' + name + '"><xsl:value-of select="\'' + params[name] + '\'"/></xsl:with-param>';
    }
    entryRule += entryRuleEnd;
    let modXsl = injectTemplate(xsl, entryRule);
    return xslt(str2xml('<root/>'), modXsl);
}
