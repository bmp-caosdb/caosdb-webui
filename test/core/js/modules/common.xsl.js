/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the html elements for entities via xsl transformation */

/* SETUP */
QUnit.module("common.xsl", {
    before: function(assert) {
        // load common.xsl
        var done = assert.async();
        var qunit_obj = this;
        connection.get("webinterface/xsl/common.xsl").then(function(data) {
            qunit_obj.commonXSL = data;
            done();
        });
    }
});

/* TESTS */
QUnit.test("availability", function(assert) {
    console.log(this);
    assert.ok(this.commonXSL);
});

QUnit.test("trim", function(assert) {
    var inject = '<xsl:template match="root"><xsl:call-template name="trim"><xsl:with-param name="str" select="text()"/></xsl:call-template></xsl:template>';
    console.log(inject);
    var xsl = injectTemplate(this.commonXSL, inject);
    var xml_str = '<root>  \n \t \n test\n\ttest\n   test\n \t  </root>';
    var xml = str2xml(xml_str);
    console.log(xml);
    var trimmed = xslt(xml, xsl);
    console.log(trimmed);
    assert.equal(trimmed.firstChild.textContent, 'test\n\ttest\n   test', "trimmed");
});


QUnit.test("reverse", function(assert) {
    var inject = '<xsl:template match="root"><xsl:call-template name="reverse"><xsl:with-param name="str" select="text()"/></xsl:call-template></xsl:template>';
    console.log(inject);
    var xsl = injectTemplate(this.commonXSL, inject);
    var xml_str = '<root>abcd</root>';
    var xml = str2xml(xml_str);
    console.log(xml);
    var trimmed = xslt(xml, xsl);
    console.log(trimmed);
    assert.equal(trimmed.firstChild.textContent, 'dcba', "reversed");
});
