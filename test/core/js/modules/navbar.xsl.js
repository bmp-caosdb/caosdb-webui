/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* Testing the construction of the query panel via xsl transformation */

/* SETUP */
QUnit.module("navbar.xsl", {
	before : function(assert) {
		// load query.xsl
		var done = assert.async();
		var qunit_obj = this;
		$.ajax({
			cache : true,
			dataType : 'xml',
 			url : "xsl/navbar.xsl",
		}).done(function(data, textStatus, jdXHR) {
			insertParam(data, "entitypath", "/entitypath/");
			insertParam(data, "filesystempath", "/filesystempath/");
			insertParam(data, "basepath", "/basepath/");	
 			qunit_obj.navbarXsl = injectTemplate(injectTemplate(data, '<xsl:template name="make-filesystem-link"><filesystemlink/></xsl:template>'), '<xsl:template name="caosdb-query-panel"><query/></xsl:template>');
 		}).always(function() {
			done();
 		});
 	}
});

/* TESTS */
QUnit.test("availability", function(assert) {
	assert.ok(this.navbarXsl);
});

QUnit.test("create navbar", function(assert){
	var xml_str = "<Response/>";
	var xml = str2xml(xml_str);
	var xsl = injectTemplate(this.navbarXsl, '<xsl:template match="/"><xsl:call-template name="caosdb-top-navbar"/></xsl:template>');
	
	var html = xslt(xml, xsl);
	assert.ok(html, "html ok");
	assert.equal(html.firstChild.tagName, "NAV", "is nav element");
});

/* MISC FUNCTIONS */
function getXSLScriptClone(source){
	return str2xml(xml2str(source))
}

function injectTemplate(orig_xsl, template){
	var xsl = getXSLScriptClone(orig_xsl);	
	var entry_t = xsl.createElement("xsl:template");
	xsl.firstElementChild.appendChild(entry_t);
	entry_t.outerHTML = template;
	return xsl;
}

function insertParam(xsl, name, value=null){
	var param = xsl.createElement("xsl:param");
	param.setAttribute("name", name);
	if (value != null) {
		param.setAttribute("select", "'"+value+"'");
	}
	xsl.firstElementChild.append(param);
}
