/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* testing webcaosdb's javascript sources */

/* SETUP ext_references module */
QUnit.module("edit_mode.js", {
    before: function(assert) {
        var done = assert.async();
        retrieveTestEntities("edit_mode/getProperties_1.xml").then(entities => {
            this.testEntity_getProperties_1 = entities[0];
            this.testEntity_make_property_editable_1 = entities[0];
             console.log(this.testEntity_getProperties_1);
            done();
        }, err => {
            globalError(err);
            done();
        });
    }
});

/**
 * Retrieve the test case xml and return an array of HTMLElement of entities in
 * the view mode representation.
 *
 * @return {HTMLElement[]}
 */
async function retrieveTestEntities(testCase, transform=true) {
    entities = await connection.get("xml/"+ testCase);
    return transformation.transformEntities(entities);
}

QUnit.test("available", function(assert) {
    assert.ok(edit_mode);
});

QUnit.test("init", function(assert){
    assert.ok(edit_mode.init);
});

QUnit.test("scroll_edit_panel", function(assert){
    assert.ok(edit_mode.scroll_edit_panel);
});

QUnit.test("prop_dragstart", function(assert){
    assert.ok(edit_mode.prop_dragstart);
});

QUnit.test("prop_dragleave", function(assert){
    assert.ok(edit_mode.prop_dragleave);
});

QUnit.test("prop_dragover", function(assert){
    assert.ok(edit_mode.prop_dragover);
});

QUnit.test("add_new_property", function(assert){
    assert.ok(edit_mode.add_new_property);
});

QUnit.test("add_dropped_property", function(assert){
    assert.ok(edit_mode.add_dropped_property);
});

QUnit.test("add_dropped_parent", function(assert){
    assert.ok(edit_mode.add_dropped_parent);
});

QUnit.test("set_entity_dropable", function(assert){
    assert.ok(edit_mode.set_entity_dropable);
});

QUnit.test("unset_entity_dropable", function(assert){
    assert.ok(edit_mode.unset_entity_dropable);
});

QUnit.test("remove_save_button", function(assert){
    assert.ok(edit_mode.remove_save_button);
});

QUnit.test("add_save_button", function(assert){
    assert.ok(edit_mode.add_save_button);
});

QUnit.test("add_trash_button", function(assert){
    assert.ok(edit_mode.add_trash_button);
});

QUnit.test("add_parent_trash_button", function(assert){
    assert.ok(edit_mode.add_parent_trash_button);
});

QUnit.test("add_parent_delete_buttons", function(assert){
    assert.ok(edit_mode.add_parent_delete_buttons);
});

QUnit.test("add_property_trash_button", function(assert){
    assert.ok(edit_mode.add_property_trash_button);
});

QUnit.test("update_entity_by_id", function(assert){
    assert.ok(edit_mode.update_entity_by_id);
});

QUnit.test("insert_entity", function(assert){
    assert.ok(edit_mode.insert_entity);
});

QUnit.test("getProperties", function(assert){
    assert.ok(edit_mode.getProperties);

    assert.equal(edit_mode.getProperties(undefined).length, 0, "undefined returns empty list");


    // test for correct parsing of datatypes
    var testEntity = this.testEntity_getProperties_1;
    console.log(testEntity);

    var properties = edit_mode.getProperties(testEntity);
    assert.equal(properties.length, 6);

    var datatypes = ["TEXT", "DOUBLE", "INTEGER", "FILE", "DATETIME", "BOOLEAN"];
    for (var i = 0; i < properties.length; i++) {
        var p = properties[i];
        var datatype = datatypes[i];
        assert.equal(p.datatype, datatype, "test " + datatype);
    }

});

QUnit.test("update_entity", function(assert){
    assert.ok(edit_mode.update_entity);
});

QUnit.test("add_edit_mode_button", function(assert){
    assert.ok(edit_mode.add_edit_mode_button);
});

QUnit.test("toggle_edit_mode", function(assert){
    assert.ok(edit_mode.toggle_edit_mode);
});

QUnit.test("leave_edit_mode", function(assert){
    assert.ok(edit_mode.leave_edit_mode);
});

QUnit.test("enter_edit_mode", function(assert){
    assert.ok(edit_mode.enter_edit_mode);
});

QUnit.test("make_header_editable", function(assert){
    assert.ok(edit_mode.make_header_editable);
});

QUnit.test("isListDatatype", function(assert){
    assert.ok(edit_mode.unListDatatype);
});

QUnit.test("make_dataype_input_logic", function(assert){
    assert.ok(edit_mode.make_dataype_input_logic);
});

QUnit.test("make_datatype_input", function(assert){
    assert.ok(edit_mode.make_datatype_input);
});

QUnit.test("make_input", function(assert){
    assert.ok(edit_mode.make_input);
});

QUnit.test("smooth_replace", function(assert){
    assert.ok(edit_mode.smooth_replace);
});

QUnit.test("make_property_editable", function(assert) {
    assert.ok(edit_mode.make_property_editable);

    assert.throws(() => edit_mode.make_property_editable(undefined), /parameter `element` was undefined./, "undefined");


    // test for correct parsing of datatypes
    var testEntity = this.testEntity_make_property_editable_1;

    for (var element of $(testEntity).find('.caosdb-property-row')) {
        edit_mode.make_property_editable(element);
    }

});

QUnit.test("create_new_record", function(assert){
    assert.ok(edit_mode.create_new_record);
});

QUnit.test("init_edit_app", function(assert){
    assert.ok(edit_mode.init_edit_app);
});

QUnit.test("has_errors", function(assert){
    assert.ok(edit_mode.has_errors);
});

QUnit.test("freeze_but", function(assert){
    assert.ok(edit_mode.freeze_but);
});

QUnit.test("unfreeze", function(assert){
    assert.ok(edit_mode.unfreeze);
});

QUnit.test("retrieve_datatype_list", function(assert){
    assert.ok(edit_mode.retrieve_datatype_list);
});

QUnit.test("highlight", function(assert){
    assert.ok(edit_mode.highlight);
});

QUnit.test("unhighlight", function(assert){
    assert.ok(edit_mode.unhighlight);
});

QUnit.test("handle_error", function(assert){
    assert.ok(edit_mode.handle_error);
});

QUnit.test("get_edit_panel", function(assert){
    assert.ok(edit_mode.get_edit_panel);
});

QUnit.test("add_wait_datamodel_info", function(assert){
    assert.ok(edit_mode.add_wait_datamodel_info);
});

QUnit.test("toggle_edit_panel", function(assert){
    assert.ok(edit_mode.toggle_edit_panel);
});

QUnit.test("leave_edit_mode_template", function(assert){
    assert.ok(edit_mode.leave_edit_mode_template);
});

QUnit.test("is_edit_mode", function(assert){
    assert.ok(edit_mode.is_edit_mode);
});

QUnit.test("add_cancel_button", function(assert){
    assert.ok(edit_mode.add_cancel_button);
});

QUnit.test("create_new_entity", function(assert){
    assert.ok(edit_mode.create_new_entity);
});

QUnit.test("remove_cancel_button", function(assert){
    assert.ok(edit_mode.remove_cancel_button);
});

QUnit.test("freeze_entity", function(assert){
    assert.ok(edit_mode.freeze_entity);
});

QUnit.test("unfreeze_entity", function(assert){
    assert.ok(edit_mode.unfreeze_entity);
});

QUnit.test("filter", function(assert){
    assert.ok(edit_mode.filter);
});

QUnit.test("add_start_edit_button", function(assert){
    assert.ok(edit_mode.add_start_edit_button);
});

QUnit.test("remove_start_edit_button", function(assert){
    assert.ok(edit_mode.remove_start_edit_button);
});

QUnit.test("add_new_record_button", function(assert){
    assert.ok(edit_mode.add_new_record_button);
});

QUnit.test("remove_new_record_button", function(assert){
    assert.ok(edit_mode.remove_new_record_button);
});

QUnit.test("add_delete_button", function(assert){
    assert.ok(edit_mode.add_delete_button);
});

QUnit.test("remove_delete_button", function(assert){
    assert.ok(edit_mode.remove_delete_button);
});
