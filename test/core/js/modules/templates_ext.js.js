/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/* SETUP ext_references module */
QUnit.module("templates_ext.js", {
    before: function(assert) {
    }
});

QUnit.test("available", function(assert) {
    assert.ok(templates_ext);
});

QUnit.test("init", function(assert) {
    assert.ok(templates_ext.init);
});

QUnit.test("generate_template", function(assert) {
    assert.ok(templates_ext.generate_template);
});

QUnit.test("add_templates", function(assert) {
    assert.ok(templates_ext.add_templates);
});

QUnit.test("add_user_templates", function(assert) {
    assert.ok(templates_ext.add_user_templates);
});

QUnit.test("retrieve_templates", function(assert) {
    assert.ok(templates_ext.retrieve_templates);
});
