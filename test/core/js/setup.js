/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
"use strict";

function getQueryValue(key) {
    var match = RegExp('[?&]' + key + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

{
    let loggerPort = getQueryValue("loggerPort");
    console.log("found loggerPort: " + loggerPort);
    if (loggerPort) {
        console.log("logging QUnit results to http://127.0.0.1:" + loggerPort);
        QUnit.testDone( function( details ) {
          var result = {
            "Module name": details.module,
            "Test name": details.name,
            "Assertions": {
              "Total": details.total,
              "Passed": details.passed,
              "Failed": details.failed
            },
            "Skipped": details.skipped,
            "Todo": details.todo,
            "Runtime": details.runtime
          };

          $.post("http://127.0.0.1:" + loggerPort + "/log", JSON.stringify( result, null, 2 ));
            
        } );

        QUnit.done(function( details ) {
            console.log("done");
            let report = (details.failed === 0 ? "SUCCESS\n" : "") + "Total: " + details.total + "\nFailed: " + details.failed + "\nPassed: " + details.passed + "\nRuntime: " + details.runtime + "\n";
            console.log(report);
            $.post("http://127.0.0.1:" + loggerPort + "/done", report);
        });


    }
}
