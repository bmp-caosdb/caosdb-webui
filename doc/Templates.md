# Documentation of Query Templates for WebUI

The WebUI supports the creation of user templates which appear below the normal query input field. These templates facilitate looking for data as query strings which are used frequently can be stored and reused.

There are two ways to integrate query templates into the WebUI:

- Default templates are integrated by the webmaster only.
- Custom templates can be integrated using the "New Template" button.

# Template definition

Templates are defined using a special syntax. Every template has a description and a corresponding query.

The description is a verbose definition of the query, e.b. "Search for experiments and return a table.".
The corresponding query will probably be similar to "SELECT date, name FROM Experiment".

Templates can contain placeholders which will be used to generate an editable field. Suppose we want to search for experiments with a specific year and return them using SELECT. This would look like:

description: Search for experiments which were done in {text} and return a table.
query: SELECT date, name FROM Experiment with date in "$1"

The placeholder {text} can be used multiple times and will lead to the creation of variables called $1, $2, ... in query. The numbers specifying the variable are created in the order of appearance of {text}.
