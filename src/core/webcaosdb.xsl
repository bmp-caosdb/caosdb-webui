<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" />

    <xsl:include href="xsl/main.xsl" />
    <xsl:include href="xsl/navbar.xsl" />
    <xsl:include href="xsl/messages.xsl" />
    <xsl:include href="xsl/query.xsl" />
    <xsl:include href="xsl/entity.xsl" />
    <xsl:include href="xsl/filesystem.xsl" />

    <xsl:template match="/">
        <html lang="en">
            <head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <xsl:call-template name="caosdb-head-title" />
                <xsl:call-template name="caosdb-head-css" />
                <xsl:call-template name="caosdb-head-js" />
            </head>
            <body>
                <xsl:call-template name="caosdb-top-navbar" />
                <xsl:call-template name="paging-panel"/>
                <xsl:call-template name="caosdb-data-container" />
                <xsl:call-template name="paging-panel"/>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>