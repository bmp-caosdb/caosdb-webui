<?xml version="1.0" encoding="utf-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template name="make-filesystem-link">
    <xsl:param name="href"/>
    <xsl:param name="display" select="$href"/>
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($filesystempath,$href)"/>
      </xsl:attribute>
      <xsl:value-of select="$display"/>
    </a>
  </xsl:template>

  <xsl:template name="trim">
    <xsl:param name="str"/>
    <xsl:call-template name="reverse">
      <xsl:with-param name="str">
        <xsl:call-template name="remove_leading_ws">
          <xsl:with-param name="str">
            <xsl:call-template name="reverse">
              <xsl:with-param name="str">
                <xsl:call-template name="remove_leading_ws">
                  <xsl:with-param name="str" select="$str"/>
                </xsl:call-template>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="remove_leading_ws">
    <xsl:param name="str"/>
    <xsl:value-of select="substring($str,string-length(substring-before($str,substring(translate($str, ' &#009;&#xA;', ''), 1, 1))) +1)"/>
  </xsl:template>

  <xsl:template name="reverse">
     <xsl:param name="str"/>
     <xsl:variable name="length" select="string-length($str)"/>
     <xsl:choose>
          <xsl:when test="$length &lt; 2">
               <xsl:value-of select="$str"/>
          </xsl:when>
          <xsl:when test="$length = 2">
               <xsl:value-of select="substring($str,2,1)"/>
               <xsl:value-of select="substring($str,1,1)"/>
          </xsl:when>
          <xsl:otherwise>
               <xsl:variable name="center" select="floor($length div 2)"/>
               <xsl:call-template name="reverse">
                    <xsl:with-param name="str"
                         select="substring($str,$center+1,$center+1)"/>
               </xsl:call-template>
               <xsl:call-template name="reverse">
                    <xsl:with-param name="str"
                         select="substring($str,1,$center)"/>
               </xsl:call-template>
          </xsl:otherwise>
     </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
