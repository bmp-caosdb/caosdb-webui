<?xml version="1.0" encoding="utf-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template match="History" mode="comment-annotation-header">
    <h4 class="media-heading">
      <xsl:value-of select="@username"/>
      <small>
        <i>
          <xsl:text> posted on </xsl:text>
          <xsl:value-of select="@datetime"/>
        </i>
      </small>
    </h4>
  </xsl:template>
  <xsl:template match="Property" mode="comment-annotation-text">
    <p class="caosdb-comment-annotation-text">
      <xsl:value-of select="text()"/>
    </p>
  </xsl:template>
  <xsl:template match="Record" mode="comment-annotation">
    <div class="media">
      <div class="media-left">
        <h3>
          <xsl:text>»</xsl:text>
        </h3>
      </div>
      <div class="media-body">
        <xsl:apply-templates mode="comment-annotation-header" select="History[translate(@transaction,'insert','INSERT')='INSERT']"/>
        <xsl:apply-templates mode="comment-annotation-text" select="Property[@name='comment']"/>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="Record">
    <li class="list-group-item">
      <xsl:apply-templates mode="comment-annotation" select="."/>
    </li>
  </xsl:template>
  <xsl:template match="Record" mode="error">
    <div class="alert alert-danger caosdb-new-comment-error alert-dismissable">
      <button class="close" data-dismiss="alert" aria-label="close">×</button>
      <strong>Error!</strong>
            This comment has not been inserted.
            <p class="small"><pre><code><xsl:copy-of select="."/></code></pre></p></div>
  </xsl:template>
  <xsl:template match="Response">
    <div>
      <xsl:choose>
        <xsl:when test="Error[@code='12']">
          <xsl:apply-templates mode="error" select="Record[Property[@name='annotationOf']]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="Record[Property[@name='annotationOf']]"/>
        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>
</xsl:stylesheet>
