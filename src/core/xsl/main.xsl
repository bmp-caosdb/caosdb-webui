<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:include href="common.xsl"/>
  <xsl:variable name="basepath">
    <xsl:call-template name="uri_ends_with_slash">
      <xsl:with-param name="uri">
        <xsl:value-of select="/Response/@baseuri"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="entitypath" select="concat($basepath,'Entity/')"/>
  <xsl:variable name="filesystempath" select="concat($basepath,'FileSystem')"/>
  <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

  <xsl:template name="caosdb-head-title">
    <title>CaosDB</title>
  </xsl:template>
  <xsl:template name="caosdb-head-css">
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/css/bootstrap.css')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="concat($basepath,'webinterface/css/webcaosdb.css')"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  <xsl:template name="caosdb-head-js">
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/jquery.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/bootstrap.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/state-machine.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/showdown.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/webcaosdb.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/caosdb.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/preview.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/ext_references.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/templates_ext.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/annotation.js')"/>
      </xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      <xsl:attribute name="src">
        <xsl:value-of select="concat($basepath,'webinterface/js/edit_mode.js')"/>
      </xsl:attribute>
    </xsl:element>
    <!--EXTENSIONS-->
    <script>
            window.sessionStorage.caosdbBasePath = "<xsl:value-of select="$basepath"/>";
            $(document).ready(() => paging.initPaging(window.location.href, <xsl:value-of select="/Response/@count"/> ));
        </script>
  </xsl:template>
  <xsl:template name="caosdb-data-container">
    <div class="container caosdb-f-main">
      <div class="row">
        <div class="panel-group caosdb-f-main-entities">
          <xsl:apply-templates select="/Response/UserInfo"/>
          <xsl:apply-templates mode="top-level-data" select="/Response/*"/>
          <xsl:apply-templates mode="query-results" select="/Response/Query"/>
          <xsl:if test="not(/Response/Query/Selection)">
            <xsl:apply-templates mode="entities" select="/Response/*"/>
          </xsl:if>
        </div>
        <div class="panel panel-warning caosdb-f-edit caosdb-v-edit-panel caosdb-v-edit-panel hidden">
          <div class="panel-heading">
            <h3 class="panel-title">Edit Mode Toolbox</h3>
          </div>
          <div class="caosdb-f-edit-panel-body panel-body"></div>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="*" mode="entities"/>
  <xsl:template match="*" mode="top-level-data"/>
  <xsl:variable name="close-char" select="'×'"/>
  <!-- assure that this uri ends with a '/' -->
  <xsl:template name="uri_ends_with_slash">
    <xsl:param name="uri"/>
    <xsl:choose>
      <xsl:when test="substring($uri,string-length($uri),1)!='/'">
        <xsl:value-of select="concat($uri,'/')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$uri"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
