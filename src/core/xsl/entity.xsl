<?xml version="1.0" encoding="utf-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
--><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <!-- These little colored Rs, RTs, Ps, and Fs which hilite the beginning 
        of a new entity. -->
  <xsl:template match="Property" mode="entity-heading-label">
    <span class="label caosdb-f-entity-role caosdb-label-property" data-entity-role="Property">P</span>
  </xsl:template>
  <xsl:template match="Record" mode="entity-heading-label">
    <span class="label caosdb-f-entity-role caosdb-label-record" data-entity-role="Record">R</span>
  </xsl:template>
  <xsl:template match="RecordType" mode="entity-heading-label">
    <span class="label caosdb-f-entity-role caosdb-label-recordtype" data-entity-role="RecordType">RT</span>
  </xsl:template>
  <xsl:template match="File" mode="entity-heading-label">
    <span class="label caosdb-f-entity-role caosdb-label-file" data-entity-role="File">F</span>
  </xsl:template>
  <xsl:template match="@id" mode="backreference-link">
    <a class="caosdb-backref-link label caosdb-id-button" title="Find all referencing entities.">
      <xsl:attribute name="href">
        <xsl:value-of select="concat($entitypath, '?P=0L10&amp;query=FIND+Entity+which+references+', current())"/>
      </xsl:attribute>
      <span class="glyphicon glyphicon-share-alt flipped-horiz-icon"/> Backref
        </a>
    <span class="spacer"/>
  </xsl:template>
  <!-- special entity properties like type, checksum, path... -->
  <xsl:template match="@datatype" mode="entity-heading-attributes-datatype">
    <p class="caosdb-entity-heading-attr small text-justify">
      <em class="caosdb-entity-heading-attr-name">data type:</em>
      <xsl:value-of select="."/>
    </p>
  </xsl:template>
  <xsl:template match="@checksum" mode="entity-heading-attributes-checksum">
    <p class="caosdb-entity-heading-attr caosdb-overflow-box small text-justify">
      <em class="caosdb-entity-heading-attr-name">
        <xsl:value-of select="concat(name(),':')"/>
      </em>
      <span class="caosdb-checksum caosdb-overflow-content">
        <xsl:value-of select="."/>
      </span>
    </p>
  </xsl:template>
  <xsl:template match="@path" mode="entity-heading-attributes-path">
    <p class="caosdb-entity-heading-attr small text-justify">
      <em class="caosdb-entity-heading-attr-name">
        <xsl:value-of select="concat(name(),':')"/>
      </em>
      <xsl:call-template name="make-filesystem-link">
        <xsl:with-param name="href" select="."/>
      </xsl:call-template>
    </p>
  </xsl:template>
  <!-- Any further entity attributes -->
  <xsl:template match="@*" mode="entity-heading-attributes">
    <p class="caosdb-entity-heading-attr small text-justify">
      <em class="caosdb-entity-heading-attr-name">
        <xsl:value-of select="concat(name(),':')"/>
      </em>
      <xsl:value-of select="."/>
    </p>
  </xsl:template>
  <xsl:template match="*" mode="entity-action-panel">
    <div class="caosdb-entity-actions-panel text-right btn-group-xs"></div>
  </xsl:template>
  <!-- Main entry for ENTITIES -->
  <xsl:template match="Property|Record|RecordType|File" mode="entities">
    <div class="panel panel-default caosdb-entity-panel">
      <xsl:attribute name="id">
        <xsl:value-of select="@id"/>
      </xsl:attribute>
      <xsl:attribute name="data-entity-id">
        <xsl:value-of select="@id"/>
      </xsl:attribute>
      <div class="panel-heading caosdb-entity-panel-heading">
        <xsl:attribute name="data-entity-datatype">
          <xsl:value-of select="@datatype"/>
        </xsl:attribute>
        <div class="row">
          <div class="col-sm-8">
            <h5>
              <xsl:apply-templates mode="entity-heading-label" select="."/>
              <!-- Parents -->
              <span class="caosdb-f-parent-list">
                <xsl:if test="Parent">
                  <!-- <xsl:apply-templates select="Parent" mode="entity-body" /> -->
                  <xsl:for-each select="Parent">
                    <span class="caosdb-parent-item small">
                      <!-- TODO lots of code duplication with parent.xsl -->
                      <xsl:attribute name="id">
                        <xsl:value-of select="generate-id()"/>
                      </xsl:attribute>
                      <span class="caosdb-f-parent-actions-panel"></span>
                      <a class="caosdb-parent-name">
                        <xsl:attribute name="href">
                          <xsl:value-of select="concat($entitypath, @id)"/>
                        </xsl:attribute>
                        <xsl:value-of select="@name"/>
                      </a>
                    </span>
                  </xsl:for-each>
                </xsl:if>
              </span>
              <strong class="caosdb-label-name">
                <xsl:value-of select="@name"/>
              </strong>
            </h5>
          </div>
          <div class="col-sm-4 text-right">
            <h5>
              <xsl:apply-templates mode="backreference-link" select="@id"/>
              <span class="label caosdb-id caosdb-id-button">
                <xsl:value-of select="@id"/>
              </span>
            </h5>
          </div>
        </div>
        <xsl:apply-templates mode="entity-heading-attributes" select="@description"/>
        <xsl:apply-templates mode="entity-heading-attributes-datatype" select="@datatype"/>
        <xsl:apply-templates mode="entity-heading-attributes-path" select="@path"/>
        <xsl:apply-templates mode="entity-heading-attributes" select="@*[not(contains('+checksum+cuid+id+name+description+datatype+path+',concat('+',name(),'+')))]"/>
        <xsl:apply-templates mode="entity-heading-attributes-checksum" select="@checksum"/>
      </div>
      <xsl:apply-templates mode="entity-action-panel" select="."/>
      <div class="panel-body caosdb-entity-panel-body">
        <!-- Messages -->
        <div class="caosdb-messages">
          <xsl:apply-templates select="Error">
            <xsl:with-param name="class" select="'alert-danger'"/>
          </xsl:apply-templates>
          <xsl:apply-templates select="Warning">
            <xsl:with-param name="class" select="'alert-warning'"/>
          </xsl:apply-templates>
          <xsl:apply-templates select="Info">
            <xsl:with-param name="class" select="'alert-info'"/>
          </xsl:apply-templates>
        </div>
        <!-- Properties -->
        <ul class="list-group caosdb-properties">
          <xsl:if test="Property">
            <li class="list-group-item caosdb-properties-heading">
              <strong class="small">Properties</strong>
            </li>
            <xsl:apply-templates mode="entity-body" select="Property"/>
          </xsl:if>
        </ul>
        <!-- Thumbnail -->
        <xsl:if test="@path">
          <xsl:call-template name="entity-body-thumbnail">
            <xsl:with-param name="path" select="@path"/>
          </xsl:call-template>
          <xsl:call-template name="entity-body-video">
            <xsl:with-param name="path" select="@path"/>
          </xsl:call-template>
        </xsl:if>
        <!-- Annotations -->
        <xsl:call-template name="annotation-section">
          <xsl:with-param name="entityId" select="@id"/>
        </xsl:call-template>
      </div>
    </div>
  </xsl:template>
  <!-- Thumbnails of images -->
  <xsl:template name="entity-body-thumbnail">
    <xsl:param name="path"/>
    <xsl:if test="contains('.jpg.gif.png.svg',translate(substring($path, string-length($path) - 3), 'JPGIFNSV', 'jpgifnsv'))">
      <div class="row">
        <div class="col-sm-12">
          <img class="entity-image-preview" style="max-width: 200px; max-height: 140px;">
            <xsl:attribute name="src">
              <xsl:value-of select="concat($filesystempath,$path)"/>
            </xsl:attribute>
          </img>
        </div>
      </div>
    </xsl:if>
  </xsl:template>
  <!-- Video display -->
  <xsl:template name="entity-body-video">
    <xsl:param name="path"/>
    <xsl:if test="contains('.mp4.mov.webm',translate(substring($path, string-length($path) - 3), 'MPOVWEB', 'mpovweb'))">
      <div class="row">
        <div class="col-sm-12">
          <video controls="controls">
            <source>
              <xsl:attribute name="src">
                <xsl:value-of select="concat($filesystempath,$path)"/>
              </xsl:attribute>
            </source>
          </video>
        </div>
      </div>
    </xsl:if>
  </xsl:template>
  <!-- PROPERTIES -->
  <xsl:template match="Property" mode="entity-body">
    <li class="list-group-item caosdb-property-row">
      <xsl:attribute name="id">
        <xsl:value-of select="generate-id()"/>
      </xsl:attribute>
      <xsl:variable name="collapseid" select="concat('collapseid-',generate-id())"/>
      <!-- property heading and value -->
      <xsl:apply-templates mode="property-collapsed" select=".">
        <xsl:with-param name="collapseid" select="$collapseid"/>
      </xsl:apply-templates>
      <!-- messages -->
      <xsl:apply-templates select="Error">
        <xsl:with-param name="class" select="'alert-danger'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Warning">
        <xsl:with-param name="class" select="'alert-warning'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Info">
        <xsl:with-param name="class" select="'alert-info'"/>
      </xsl:apply-templates>
      <!-- collapsed data -->
      <div class="collapse">
        <xsl:attribute name="id">
          <xsl:value-of select="$collapseid"/>
        </xsl:attribute>
        <hr class="caosdb-subproperty-divider"/>
        <!--  <li> -->
              <!-- <a class="caosdb-property-name"> -->
              <!--           <xsl:attribute name="href"> -->
              <!--               <xsl:value-of select="concat($entitypath,@id)" /></xsl:attribute> -->
              <!-- </a> -->
              <!-- </li> -->

                <!-- property attributes -->
        <xsl:apply-templates mode="property-attributes" select="@description"/>
        <xsl:apply-templates mode="property-attributes-id" select="@id"/>
        <xsl:apply-templates mode="property-attributes-type" select="@datatype"/>
        <xsl:apply-templates mode="property-attributes" select="@*[not(contains('+cuid+id+name+description+datatype+',concat('+',name(),'+')))]"/>
      </div>
        </li>
  </xsl:template>
  <xsl:template match="Property" mode="property-collapsed">
    <xsl:param name="collapseid"/>
    <div class="row">
      <div class="col-sm-4">
        <h5>
          <xsl:if test="@*[not(contains('+cuid+id+name+',concat('+',name(),'+')))]">
            <span class="glyphicon glyphicon-collapse-down" data-toggle="collapse" style="margin-right: 10px;">
              <xsl:attribute name="data-target">
                <xsl:value-of select="concat('#',$collapseid)"/>
              </xsl:attribute>
            </span>
          </xsl:if>
          <strong class="caosdb-property-name"> <xsl:value-of select="@name"/></strong>
        </h5>
      </div>
      <!-- property value -->
      <div class="col-sm-6 caosdb-property-value">
        <xsl:apply-templates mode="property-value" select="."/>
      </div>
      <div class="col-sm-6 caosdb-property-edit-value" style="display: none;"></div>
      <div class="col-sm-2 caosdb-property-edit" style="text-align: right;"></div>
    </div>
  </xsl:template>
  <xsl:template name="single-value">
    <xsl:param name="value"/>
    <xsl:param name="reference"/>
    <xsl:param name="boolean"/>
    <xsl:if test="normalize-space($value)!=''">
      <xsl:choose>
        <xsl:when test="$reference='true' and normalize-space($value)!=''">
          <!-- this is a reference -->
          <a class="btn btn-default btn-sm caosdb-resolvable-reference">
            <xsl:attribute name="href">
              <xsl:value-of select="concat($entitypath,normalize-space($value))"/>
            </xsl:attribute>
            <span class="caosdb-id caosdb-id-button">
              <xsl:value-of select="normalize-space($value)"/>
            </span>
            <span class="caosdb-resolve-reference-target"/>
          </a>
        </xsl:when>
        <xsl:when test="$boolean='true'">
          <xsl:element name="span">
            <xsl:attribute name="class">
              <xsl:value-of select="concat('caosdb-boolean-',normalize-space($value)='TRUE')"/>
            </xsl:attribute>
            <xsl:value-of select="normalize-space($value)"/>
          </xsl:element>
        </xsl:when>
        <xsl:otherwise>
          <span class="caosdb-property-text-value">
            <xsl:call-template name="trim">
              <xsl:with-param name="str">
                <xsl:value-of select="$value"/>
              </xsl:with-param>
            </xsl:call-template>
          </span>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template match="Property" mode="property-reference-value-list">
    <xsl:param name="reference"/>
    <div class="caosdb-value-list">
      <xsl:element name="div">
        <xsl:attribute name="class">btn-group btn-group-sm caosdb-overflow-content</xsl:attribute>
        <xsl:for-each select="Value">
          <xsl:call-template name="single-value">
            <xsl:with-param name="reference">
              <xsl:value-of select="'true'"/>
            </xsl:with-param>
            <xsl:with-param name="value">
              <xsl:value-of select="text()"/>
            </xsl:with-param>
            <xsl:with-param name="boolean">
              <xsl:value-of select="'false'"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:element>
    </div>
  </xsl:template>
  <xsl:template match="Property" mode="property-value-list">
    <xsl:param name="reference"/>
    <div class="caosdb-value-list">
      <xsl:element name="ol">
        <xsl:attribute name="class">list-group list-inline</xsl:attribute>
        <xsl:for-each select="Value">
          <xsl:element name="li">
            <xsl:attribute name="class">list-group-item</xsl:attribute>
            <xsl:call-template name="single-value">
              <xsl:with-param name="reference">
                <xsl:value-of select="'false'"/>
              </xsl:with-param>
              <xsl:with-param name="value">
                <xsl:value-of select="text()"/>
              </xsl:with-param>
              <xsl:with-param name="boolean">
                <xsl:value-of select="../@datatype='LIST&lt;BOOLEAN>'"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </div>
  </xsl:template>
  <xsl:template match="Property" mode="property-value">
    <xsl:choose>
      <!-- filter out collection data types -->
      <xsl:when test="contains(concat('&lt;',@datatype),'&lt;LIST&lt;')">
        <!-- list -->
        <xsl:choose>
          <xsl:when test="translate(normalize-space(text()),'0123456789','')='' and not(contains('+LIST&lt;INTEGER>+LIST&lt;DOUBLE>+LIST&lt;TEXT>+LIST&lt;BOOLEAN>+LIST&lt;DATETIME>+',concat('+',@datatype,'+')))">
            <xsl:apply-templates mode="property-reference-value-list" select="."/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates mode="property-value-list" select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- hence, this is no collection -->
      <xsl:otherwise>
        <xsl:call-template name="single-value">
          <xsl:with-param name="value">
            <xsl:value-of select="text()"/>
          </xsl:with-param>
          <xsl:with-param name="reference">
            <xsl:value-of select="translate(normalize-space(text()),'0123456789','')='' and not(contains('+INTEGER+DOUBLE+TEXT+BOOLEAN+DATETIME+',concat('+',@datatype,'+')))"/>
          </xsl:with-param>
          <xsl:with-param name="boolean">
            <xsl:value-of select="@datatype='BOOLEAN'"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <!-- unit -->
    <xsl:if test="@unit">
      <span class="caosdb-unit">
        <xsl:value-of select="@unit"/>
      </span>
    </xsl:if>
  </xsl:template>
  <xsl:template match="Property" mode="property-value-plain">
    <xsl:choose>
      <!-- filter out collection data types -->
      <xsl:when test="contains(concat('&lt;',@datatype),'&lt;LIST&lt;')">
              <!-- ignore for now -->
            </xsl:when>
      <!-- hence, this is no collection -->
      <xsl:otherwise>
        <xsl:value-of select="text()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="@*" mode="property-attributes">
    <div class="row">
      <div class="col-sm-3 col-sm-offset-1">
        <strong>
          <xsl:value-of select="name()"/>
        </strong>
      </div>
      <div class="col-sm-8">
        <xsl:value-of select="."/>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="@datatype" mode="property-attributes-type">
    <div class="row">
      <div class="col-sm-3 col-sm-offset-1">
        <strong>data type</strong>
      </div>
      <div class="col-sm-8 caosdb-property-datatype">
        <xsl:value-of select="."/>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="@id" mode="property-attributes-id">
    <div class="row">
      <div class="col-sm-3 col-sm-offset-1">
        <strong>
          <xsl:value-of select="name()"/>
        </strong>
      </div>
      <div class="col-sm-8">
        <a class="caosdb-property-id">
          <xsl:attribute name="href">
            <xsl:value-of select="concat($entitypath,.)"/>
          </xsl:attribute>
          <xsl:value-of select="."/>
        </a>
      </div>
    </div>
  </xsl:template>
  <!-- ANNOTATIONS -->
  <xsl:template name="annotation-section">
    <xsl:param name="entityId"/>
    <ul class="list-group caosdb-annotation-section">
      <xsl:attribute name="data-entity-id">
        <xsl:value-of select="$entityId"/>
      </xsl:attribute>
      <li class="list-group-item caosdb-comments-heading">
        <strong class="small">Comments</strong>
        <button class="btn btn-link btn-xs pull-right caosdb-new-comment-button">
          <strong>add new comment</strong>
        </button>
      </li>
    </ul>
  </xsl:template>
</xsl:stylesheet>
