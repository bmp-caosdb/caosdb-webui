<?xml version="1.0" encoding="utf-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:param name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
  <xsl:param name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
  <xsl:template match="ParseTree" mode="query-results">
    <xsl:apply-templates select="ParsingError"/>
  </xsl:template>
  <xsl:template match="ParseTree/ParsingError" mode="query-results">
    <div class="panel-body">
      <div class="caosdb-overflow-box">
        <div class="caosdb-overflow-content">
          <span>ParseTree:</span>
          <xsl:value-of select="text()"/>
        </div>
        <div class="caosdb-overflow-contents">
          <span>
            <xsl:value-of select="@type"/>
            <xsl:text>, line </xsl:text>
            <xsl:value-of select="@line"/>
            <xsl:text>, character </xsl:text>
            <xsl:value-of select="@character"/>
            <xsl:text>:</xsl:text>
          </span>
          <xsl:value-of select="text()"/>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="Query" mode="query-results">
    <div class="panel panel-default caosdb-query-response">
      <div class="panel-heading caosdb-query-response-heading">
        <div class="row">
          <div class="col-sm-10 caosdb-overflow-box">
            <div class="caosdb-overflow-content">
              <span>Query: </span>
              <xsl:value-of select="@string"/>
            </div>
          </div>
          <div class="col-sm-2 text-right">
            <span>Results: </span>
            <span class="caosdb-query-response-results">
              <xsl:value-of select="@results"/>
            </span>
          </div>
        </div>
      </div>
      <xsl:if test="@results=0">
        <div class="panel panel-default caosdb-no-results">
          <div class="alert alert-warning" role="alert">
                There were no results for this query.
              </div>
        </div>
      </xsl:if>
      <xsl:apply-templates mode="query-results" select="./ParseTree"/>
    </div>
    <xsl:if test="@results!=0">
      <xsl:apply-templates mode="select-table" select="./Selection"/>
    </xsl:if>
  </xsl:template>
  <xsl:template match="Selection" mode="select-table">
    <div class="panel panel-default caosdb-select-table">
      <div class="panel-heading">
        <div class="container-fluid panel-container">
          <div class="col-xs-6">
            <h5>Table of selected fields</h5>
          </div>
          <div class="col-xs-6 text-right">
            <!-- Trigger the modal with a button -->
            <button class="btn btn-info btn-sm" data-target="#downloadModal" data-toggle="modal" type="button">Download this table</button>
            <!-- Modal -->
            <div class="modal fade text-left" id="downloadModal" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button class="close" data-dismiss="modal" type="button">×</button>
                    <h4 class="modal-title">Download this table</h4>
                  </div>
                  <div class="modal-body">
                    <p>
                      <a>
                        <xsl:attribute name="href">
                  data:text/csv;charset=utf-8,<xsl:for-each select="Selector"><xsl:value-of select="@name"/>%09</xsl:for-each><xsl:for-each select="/Response/*[@id]"><xsl:call-template name="select-table-row-plain"><xsl:with-param name="entity-id" select="@id"/></xsl:call-template></xsl:for-each></xsl:attribute>
                Download TSV File
                </a>
                    </p>
                    <hr/>
                    <p>
                      <small>Download this dataset in Python with:</small>
                    </p>
                    <p>
                      <code>
		    data = caosdb.execute_query('<xsl:value-of select="//Response/Query/@string"/>')
		  </code>
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="caosdb-select-table-actions-panel text-right btn-group-xs"></div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th></th>
                <xsl:for-each select="Selector[@name!='id']">
                  <th>
                    <xsl:value-of select="@name"/>
                  </th>
                </xsl:for-each>
              </tr>
            </thead>
            <tbody>
              <xsl:for-each select="/Response/*[@id]">
                <xsl:call-template name="select-table-row">
                  <xsl:with-param name="entity-id" select="@id"/>
                </xsl:call-template>
              </xsl:for-each>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="entity-link">
    <xsl:param name="entity-id"/>
    <a class="btn btn-default btn-sm caosdb-select-id">
      <xsl:attribute name="href">
        <xsl:value-of select="concat($entitypath, $entity-id)"/>
      </xsl:attribute>
      <!-- <xsl:value-of select="$entity-id" /> -->
      <span class="caosdb-select-id-target">
        <span class="glyphicon glyphicon-new-window"/>
      </span>
    </a>
  </xsl:template>
  <xsl:template name="select-table-row">
    <xsl:param name="entity-id"/>
    <tr>
      <td>
        <xsl:call-template name="entity-link">
          <xsl:with-param name="entity-id" select="$entity-id"/>
        </xsl:call-template>
      </td>
      <xsl:for-each select="/Response/Query/Selection/Selector[@name!='id']">
        <xsl:call-template name="select-table-cell">
          <xsl:with-param name="entity-id" select="$entity-id"/>
          <xsl:with-param name="field-name" select="translate(@name, $uppercase, $lowercase)"/>
        </xsl:call-template>
      </xsl:for-each>
    </tr>
  </xsl:template>
  <xsl:template name="select-table-cell">
    <xsl:param name="entity-id"/>
    <xsl:param name="field-name"/>
    <td>
      <div class="caosdb-v-property-value">
        <xsl:choose>
          <xsl:when test="/Response/*[@id=$entity-id]/@*[translate(name(),$uppercase, $lowercase)=$field-name]">
            <xsl:value-of select="/Response/*[@id=$entity-id]/@*[translate(name(), $uppercase, $lowercase)=$field-name]"/>
          </xsl:when>
          <xsl:when test="/Response/*[@id=$entity-id]/Property[translate(@name, $uppercase, $lowercase)=$field-name]">
            <xsl:apply-templates mode="property-value" select="/Response/*[@id=$entity-id]/Property[translate(@name, $uppercase, $lowercase)=$field-name]"/>
          </xsl:when>
        </xsl:choose>
      </div>
    </td>
  </xsl:template>
  <!-- For CSV download -->
    <!-- This block is responsible for generating the downloadable TSV. -->
  <xsl:template name="select-table-row-plain">
    <xsl:param name="entity-id"/>
%0A<xsl:for-each select="/Response/Query/Selection/Selector"><xsl:call-template name="select-table-cell-plain"><xsl:with-param name="entity-id" select="$entity-id"/><xsl:with-param name="field-name" select="translate(@name, $uppercase, $lowercase)"/></xsl:call-template><xsl:if test="position()!=last()">%09</xsl:if></xsl:for-each></xsl:template>
  <xsl:template name="select-table-cell-plain">
    <xsl:param name="entity-id"/>
    <xsl:param name="field-name"/>
    <xsl:choose>
      <xsl:when test="/Response/*[@id=$entity-id]/@*[translate(name(),$uppercase, $lowercase)=$field-name]">
        <xsl:value-of select="/Response/*[@id=$entity-id]/@*[translate(name(), $uppercase, $lowercase)=$field-name]"/>
      </xsl:when>
      <xsl:when test="/Response/*[@id=$entity-id]/Property[translate(@name, $uppercase, $lowercase)=$field-name]">
        <xsl:apply-templates mode="property-value-plain" select="/Response/*[@id=$entity-id]/Property[translate(@name, $uppercase, $lowercase)=$field-name]"></xsl:apply-templates>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="caosdb-query-panel">
    <div class="container caosdb-query-panel">
      <form class="panel" id="caosdb-query-form" method="GET">
        <xsl:attribute name="action">
          <xsl:value-of select="$entitypath"/>
        </xsl:attribute>
        <input id="caosdb-query-paging-input" name="P" type="hidden" value="0L10"/>
        <div class="input-group">
          <input class="form-control" id="caosdb-query-textarea" name="query" placeholder="E.g. 'FIND Experiment'" rows="1" style="resize: vertical;" type="text"></input>
          <span class="input-group-addon btn btn-default caosdb-search-btn">
            <a href="#" title="Click to execute the query.">
              <span class="glyphicon glyphicon-search"></span>
            </a>
          </span>
        </div>
      </form>
    </div>
  </xsl:template>
</xsl:stylesheet>
