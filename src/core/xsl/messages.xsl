<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template match="Error|Warning|Info">
    <xsl:param name="class"/>
    <div>
      <xsl:attribute name="class">alert
                <xsl:value-of select="$class"/> alert-dismissable fade in</xsl:attribute>
      <a class="close" data-dismiss="alert" href="#">
        <xsl:value-of select="$close-char"/>
      </a>
      <strong>
        <xsl:value-of select="name()"/>
      </strong>
      <xsl:value-of select="@description"/>
      <div class="small">
        <xsl:value-of select="text()"/>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="script" mode="entities">
    <div class="container" id="caosdb-container-script">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="row">
            <div class="col-sm-8" id="caosdb-caption-script">
                Output of the Script
              </div>
            <div class="col-sm-4 text-right">
                Code: <span id="caosdb-return-code"><xsl:value-of select="@code"/></span></div>
          </div>
        </div>
      </div>
      <xsl:apply-templates select="stderr"/>
      <xsl:apply-templates select="stdout"/>
    </div>
  </xsl:template>
  <xsl:template match="stderr">
    <div class="panel panel-default" id="caosdb-container-stderr">
      <div class="panel-heading" id="caosdb-caption-stderr">Errors:</div>
      <div class="alert" id="caosdb-stderr">
        <xsl:value-of select="text()"/>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="stdout">
    <div class="panel panel-default" id="caosdb-container-stdout">
      <div class="panel-heading" id="caosdb-caption-stdout">Standard Messages:</div>
      <div id="caosdb-stdout">
        <xsl:value-of select="text()"/>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
