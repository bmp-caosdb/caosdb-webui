<?xml version="1.0" encoding="UTF-8"?>
<!-- Simplified interface for editing data models --><!-- A. Schlemmer, 01/2019 --><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>

  <xsl:template match="/Response">
    <div class="btn-group-vertical">
      <button type="button" class="btn btn-default caosdb-f-edit-panel-new-button new-property">Create new Property</button>
      <button type="button" class="btn btn-default caosdb-f-edit-panel-new-button new-recordtype">Create new RecordType</button>
    </div>
    <div title="Drag and drop Properties from this panel to the Entities on the left." class="panel panel-default">
      <div class="panel-heading">
        <h5>Existing Properties</h5>
      </div>
      <div class="panel-body">
        <div class="input-group" style="width: 100%;">
          <input class="form-control" placeholder="filter..." title="Type a name (full or partial)." oninput="edit_mode.filter('properties');" id="caosdb-f-filter-properties" type="text"/>
          <span class="input-group-btn">
            <button class="btn btn-default caosdb-f-edit-panel-new-button new-property caosdb-f-hide-on-empty-input" title="Create this Property." ><span class="glyphicon glyphicon-plus"></span></button>
          </span>
        </div>
        <ul class="caosdb-v-edit-list">
          <xsl:apply-templates select="./Property"/>
        </ul>
      </div>
    </div>
    <div title="Drag and drop RecordTypes from this panel to the Entities on the left." class="panel panel-default">
      <div class="panel-heading">
        <h5>Existing RecordTypes</h5>
      </div>
      <div class="panel-body">
        <div class="input-group" style="width: 100%;">
          <input class="form-control" placeholder="filter..." title="Type a name (full or partial)." oninput="edit_mode.filter('recordtypes');" id="caosdb-f-filter-recordtypes" type="text"/>
          <span class="input-group-btn">
              <button class="btn btn-default caosdb-f-edit-panel-new-button new-recordtype caosdb-f-hide-on-empty-input" title="Create this RecordType"><span class="glyphicon glyphicon-plus"></span></button>
          </span>
        </div>
        <ul class="caosdb-v-edit-list">
          <xsl:apply-templates select="./RecordType"/>
        </ul>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="RecordType">
    <xsl:if test="string-length(@name)>0">
        <li class="caosdb-f-edit-drag list-group-item caosdb-v-edit-drag">
        <xsl:attribute name="id">caosdb-f-edit-rt-<xsl:value-of select="@id"/></xsl:attribute>
        <xsl:value-of select="@name"/>
        </li>
    </xsl:if>
  </xsl:template>

  <xsl:template match="Property">
    <li class="caosdb-f-edit-drag list-group-item caosdb-v-edit-drag">
      <xsl:attribute name="id">caosdb-f-edit-p-<xsl:value-of select="@id"/></xsl:attribute>
      <xsl:value-of select="@name"/>
    </li>
  </xsl:template>
</xsl:stylesheet>
