<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template name="make-retrieve-all-link">
    <xsl:param name="entity"/>
    <xsl:param name="display"/>
    <xsl:param name="paging" select="'0L10'"/>
    <li>
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="concat($entitypath, '?all=', normalize-space($entity))"/>
          <xsl:if test="$paging">
            <xsl:value-of select="concat('&amp;P=',$paging)"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:value-of select="$display"/>
      </a>
    </li>
  </xsl:template>
  <xsl:template name="caosdb-top-navbar">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button class="navbar-toggle" data-target="#top-navbar" data-toggle="collapse" type="button">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand">
            <xsl:element name="img">
              <xsl:attribute name="class">caosdb-logo</xsl:attribute>
              <xsl:attribute name="src">
                <xsl:value-of select="concat($basepath,'webinterface/pics/caosdb_logo_medium.png')"/>
              </xsl:attribute>
            </xsl:element>
                        CaosDB
                    </a>
        </div>
        <div class="collapse navbar-collapse" id="top-navbar">
          <xsl:if test="/Response/UserInfo">
            <ul class="nav navbar-nav caosdb-navbar">
              <li class="dropdown" id="caosdb-navbar-entities">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Entities
                                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li class="dropdown-header">Retrieve all:</li>
                  <xsl:call-template name="make-retrieve-all-link">
                    <xsl:with-param name="entity">
                                        Entity
                                    </xsl:with-param>
                    <xsl:with-param name="display">
                                        Entities
                                    </xsl:with-param>
                  </xsl:call-template>
                  <xsl:call-template name="make-retrieve-all-link">
                    <xsl:with-param name="entity">
                                        Record
                                    </xsl:with-param>
                    <xsl:with-param name="display">
                                        Records
                                    </xsl:with-param>
                  </xsl:call-template>
                  <xsl:call-template name="make-retrieve-all-link">
                    <xsl:with-param name="entity">
                                        RecordType
                                    </xsl:with-param>
                    <xsl:with-param name="display">
                                        RecordTypes
                                    </xsl:with-param>
                  </xsl:call-template>
                  <xsl:call-template name="make-retrieve-all-link">
                    <xsl:with-param name="entity">
                                        Property
                                    </xsl:with-param>
                    <xsl:with-param name="display">
                                        Properties
                                    </xsl:with-param>
                  </xsl:call-template>
                  <xsl:call-template name="make-retrieve-all-link">
                    <xsl:with-param name="entity">
                                        File
                                    </xsl:with-param>
                    <xsl:with-param name="display">
                                        Files
                                    </xsl:with-param>
                  </xsl:call-template>
                </ul>
              </li>
              <li id="caosdb-navbar-filesystem">
                <xsl:call-template name="make-filesystem-link">
                  <xsl:with-param name="href" select="'/'"/>
                  <xsl:with-param name="display" select="'File System'"/>
                </xsl:call-template>
              </li>
              <li id="caosdb-navbar-query">
                <button class="navbar-btn btn btn-link" data-target="#caosdb-query-panel" data-toggle="collapse">
                                Query
                            </button>
              </li>
            </ul>
          </xsl:if>
          <ul class="nav navbar-nav navbar-right">
            <xsl:call-template name="caosdb-user-menu"/>
          </ul>
        </div>
        <!-- query panel -->
        <div class="collapse" id="caosdb-query-panel">
          <xsl:call-template name="caosdb-query-panel"/>
        </div>
      </div>
      <xsl:apply-templates select="/Response/Error">
        <xsl:with-param name="class" select="'alert-danger'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="/Response/Warning">
        <xsl:with-param name="class" select="'alert-warning'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="/Response/Info">
        <xsl:with-param name="class" select="'alert-info'"/>
      </xsl:apply-templates>
    </nav>
    <div class="container" id="subnav"/>
  </xsl:template>
  <xsl:template match="Role" name="caosdb-user-roles">
    <div class="caosdb-user-role">
      <xsl:value-of select="text()"/>
    </div>
  </xsl:template>
  <xsl:template match="UserInfo" name="caosdb-user-info">
    <div class="caosdb-user-info" style="display: none;">
      <div class="caosdb-user-name">
        <xsl:value-of select="@username"/>
      </div>
      <div class="caosdb-user-realm">
        <xsl:value-of select="@realm"/>
      </div>
      <xsl:apply-templates select="Roles/Role"/>
    </div>
  </xsl:template>
  <xsl:template name="caosdb-user-menu">
    <xsl:choose>
      <xsl:when test="/Response/@username">
        <li class="dropdown" id="user-menu">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <xsl:value-of select="concat(/Response/@username,' ')"/>
            <span class="glyphicon glyphicon-user"/>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a title="Click to logout.">
                <xsl:attribute name="href">
                  <xsl:value-of select="concat($basepath, 'logout')"/>
                </xsl:attribute>
                                Logout
                                <span class="glyphicon glyphicon-log-out"/></a>
            </li>
          </ul>
        </li>
      </xsl:when>
      <xsl:otherwise>
        <li id="user-menu">
          <form class="navbar-form" method="POST">
            <xsl:attribute name="action">
              <xsl:value-of select="concat($basepath, 'login')"/>
            </xsl:attribute>
            <input class="form-control" id="username" name="username" placeholder="username" type="text"/>
            <input class="form-control" id="password" name="password" placeholder="password" type="password"/>
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-log-in"></span>
                            Login
                        </button>
          </form>
        </li>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="paging-panel">
    <div class="container caosdb-paging-panel" style="display: none">
      <ul class="pager">
        <li class="previous">
          <a class="caosdb-prev-button">Previous Page</a>
        </li>
        <li class="next">
          <a class="caosdb-next-button">Next Page</a>
        </li>
      </ul>
    </div>
  </xsl:template>
</xsl:stylesheet>
