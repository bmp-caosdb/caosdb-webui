<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template name="filesystem-cwd">
    <xsl:param name="pardir" select="concat($filesystempath, '/')"/>
    <xsl:param name="curdir-name" select="substring-before(substring-after(/Response/dir/@url,$pardir),'/')"/>
    <xsl:param name="curdir" select="concat($pardir, $curdir-name, '/')"/>
    <xsl:if test="string-length($curdir-name)">
      <xsl:choose>
        <xsl:when test="$curdir-name!=/Response/dir/@name">
          <a class="caosdb-fs-cwd" title="Go to this directory.">
            <xsl:attribute name="href">
              <xsl:value-of select="$curdir"/>
            </xsl:attribute>
            <xsl:value-of select="$curdir-name"/>
          </a>
          <xsl:call-template name="filesystem-cwd">
            <xsl:with-param name="pardir" select="$curdir"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <span class="caosdb-fs-cwd">
            <xsl:value-of select="$curdir-name"/>
          </span>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template match="dir" mode="filesystem-item">
    <li class="list-group-item">
      <a class="caosdb-fs-dir">
        <xsl:attribute name="href">
          <xsl:value-of select="concat(/Response/dir/@url, @name)"/>
        </xsl:attribute>
        <span class="glyphicon"></span>
        <xsl:value-of select="@name"/>
      </a>
    </li>
  </xsl:template>
  <xsl:template match="file" mode="filesystem-item">
    <xsl:param name="file-uri" select="concat(/Response/dir/@url, @name)"/>
    <li class="list-group-item">
      <div class="row">
        <div class="col-sm-6">
          <a class="caosdb-fs-file">
            <xsl:attribute name="href">
              <xsl:value-of select="$file-uri"/>
            </xsl:attribute>
            <span class="glyphicon"></span>
            <xsl:value-of select="@name"/>
          </a>
        </div>
        <div class="col-sm-6 text-right">
          <a class="btn caosdb-fs-btn-file">
            <xsl:attribute name="href">
              <xsl:value-of select="concat($entitypath, @id)"/>
            </xsl:attribute>
            <span class="label caosdb-label-file">F</span>
            <span class="label caosdb-id">
              <xsl:value-of select="@id"/>
            </span>
          </a>
        </div>
      </div>
      <xsl:call-template name="entity-body-thumbnail">
        <xsl:with-param name="path" select="substring-after($file-uri,$filesystempath)"/>
      </xsl:call-template>
    </li>
  </xsl:template>
  <xsl:template match="/Response/dir" mode="top-level-data">
    <div class="container">
      <div class="panel-group">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="row">
              <div class="col-sm-8">
                <a title="Go back to the root of the file system.">
                  <xsl:attribute name="href">
                    <xsl:value-of select="concat($filesystempath, '/')"/>
                  </xsl:attribute>
                  <strong>File System</strong>
                </a>
                <xsl:call-template name="filesystem-cwd"/>
              </div>
              <div class="col-sm-4 text-right">
                <xsl:value-of select="count(dir)"/> Directories and
                        <xsl:value-of select="count(file)"/> Files
                      </div>
            </div>
          </div>
          <div class="panel-body">
            <ul class="list-group">
              <xsl:apply-templates mode="filesystem-item" select="dir"/>
              <xsl:apply-templates mode="filesystem-item" select="file"/>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
