<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" />

    <xsl:variable name="base_uri">
        <xsl:call-template name="uri_ends_with_slash">
            <xsl:with-param name="uri" select="/Response/@baseuri" />
        </xsl:call-template>
    </xsl:variable>

    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css">
                    <xsl:attribute name="href">
                <xsl:value-of
                        select="concat($base_uri, 'webinterface/permissions.css')" />
                </xsl:attribute>
                </link>

            </head>
            <body>
                <xsl:apply-templates select="Response/*" />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="Property|Record|RecordType">
        <xsl:call-template name="perm_table">
            <xsl:with-param name="for">
                <xsl:value-of select="name()" />
                <xsl:text> </xsl:text>
                <xsl:value-of select="@id"></xsl:value-of>
            </xsl:with-param>
            <xsl:with-param name="entity_id" select="@id" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="EntityPermissions">
        <xsl:call-template name="perm_table">
            <xsl:with-param name="for">
                all entities
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="perm_table">
        <xsl:param name="for"></xsl:param>
        <xsl:param name="entity_id"></xsl:param>
        <table class="entity_acl">
            <caption>
                Permissions for
                <xsl:value-of select="$for" />
            </caption>
            <tr>
                <th />
                <th class="role">Role</th>
                <xsl:for-each select="//EntityPermissions/Permission">
                    <th class="permission">
                        <xsl:value-of select="@name" />
                    </th>
                </xsl:for-each>
            </tr>
            <xsl:apply-templates select="EntityACL/Grant|EntityACL/Deny"
                mode="table_row">
                <xsl:with-param name="entity_id" select="$entity_id" />
            </xsl:apply-templates>
        </table>
    </xsl:template>

    <xsl:template match="Grant|Deny" mode="table_row">
        <xsl:param name="entity_id" />
        <xsl:param name="role" select="@role" />
        <xsl:param name="g_or_d" select="name()" />

        <tr>
            <xsl:attribute name="class">
                <xsl:value-of select="name()" />
                <xsl:if test="@priority='true'">
                <xsl:text> priority</xsl:text>
                </xsl:if>
            </xsl:attribute>
            <td>
                <xsl:value-of select="name()" />
            </td>
            <td class="role">
                <xsl:value-of select="$role" />
            </td>
            <xsl:for-each select="//EntityPermissions/Permission">
                <td class="permission">
                    <input type="checkbox">
                        <xsl:call-template name="is_checked">
                            <xsl:with-param name="pname"
                                select="@name" />
                            <xsl:with-param name="entity_id"
                                select="$entity_id" />
                            <xsl:with-param name="role"
                                select="$role" />
                            <xsl:with-param name="g_or_d"
                                select="$g_or_d" />
                        </xsl:call-template>
                    </input>
                </td>
            </xsl:for-each>

        </tr>
    </xsl:template>

    <xsl:template name="is_checked">
        <xsl:param name="pname" />
        <xsl:param name="entity_id" />
        <xsl:param name="role" />
        <xsl:param name="g_or_d" />
        <xsl:choose>
            <xsl:when test="$entity_id">
                <xsl:if
                    test="/Response/*[@id=$entity_id]/EntityACL/*[name()=$g_or_d][@role=$role]/Permission[@name=$pname]">
                    <xsl:attribute name="checked"></xsl:attribute>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if
                    test="/Response/EntityPermissions/EntityACL/*[name()=$g_or_d][@role=$role]/Permission[@name=$pname]">
                    <xsl:attribute name="checked"></xsl:attribute>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- assure that this uri ends with a '/' -->
    <xsl:template name="uri_ends_with_slash">
        <xsl:param name="uri" />
        <xsl:choose>
            <xsl:when test="substring($uri,string-length($uri),1)!='/'">
                <xsl:value-of select="concat($uri,'/')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$uri" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>

