'use strict';

/**
 * JavaScript client for CaosDB
 * A. Schlemmer, 08/2018
 * T. Fitschen, 02/2019
 *
 * Dependency: jquery
 * Dependency: webcaosdb
 *
 * TODO:
 * - Check whether everything works when previews are opened.
 */

/**
 * Return true iff the current session has an authenticated user. This is the
 * case, iff there is a `user` attribute in the server's response and
 * subsequently a DIV.caosdb-user-name somewhere in the dom tree.
 *
 * @return {boolean}
 */
function isAuthenticated() {
    return document.getElementsByClassName("caosdb-user-name").length > 0 && document.getElementsByClassName("caosdb-user-name")[0].innerText.trim().length > 0;
}


/**
 * Return the name of the user currently logged in.
 *
 * @return Name of the user.
 */
function getUserName() {
    return document.getElementsByClassName("caosdb-user-name")[0].innerText;
}

/**
 * Return the realm of the user currently logged in.
 *
 * @return Realm of the user.
 */
function getUserRealm() {
    return document.getElementsByClassName("caosdb-user-realm")[0].innerText;
}

/**
 * Return the roles of the user currently logged in.
 *
 * @return Array containing the roles of the user.
 */
function getUserRoles() {
    return Array.from(document.getElementsByClassName("caosdb-user-role")
                     ).map(el => el.innerText);
}

/**
 * Find out, whether the user that is currently logged in is an administrator.
 *
 * @return true if the role administration is present.
 */
function userIsAdministrator() {
    return userHasRole("administration");
}

/**
 * Return true if the user has the role `anonymous`.
 */
function userIsAnonymous() {
    return userHasRole("anonymous");
}


/**
 * Return true if the user has the role `role`.
 *
 * @param role
 */
function userHasRole(role) {
    return getUserRoles().filter(el => el == role).length > 0;
}


/**
 * Return all entities in the current document.
 *
 * @return A list of document elements.
 */
function getEntities() {
    return document.getElementsByClassName("caosdb-entity-panel");
}

/**
 * Return the role (Record, RecordType, ...) of element.
 * @return A String holding the role or undefined if the role could not be found.
 */
function getEntityRole(element) {
    if (typeof element.dataset.entityRole !== 'undefined') {
        return element.dataset.entityRole;
    }

    let res = element.getElementsByClassName("caosdb-f-entity-role");
    if (res.length == 1) {
        return res[0].dataset.entityRole;
    }
    return undefined;
}

/**
 * Return the unit of element.
 * If the corresponding data-attribute can not be found undefined is returned.
 * @return A string containing the datatype of the element.
 */
function getEntityUnit(element) {
    var res = $(element).find("input.caosdb-f-entity-unit");
    if (res.length == 1) {
        var x = res.val();
        return (x == '' ? undefined : x);
    }
    return undefined;
}

/**
 * Return the datatype of element.
 * If the corresponding data-attribute can not be found undefined is returned.
 * @return A string containing the datatype of the element.
 */
function getEntityDatatype(element) {
    var is_list = ($(element).find("input.caosdb-f-entity-is-list:checked").length > 0);
    var res = $(element).find("select.caosdb-f-entity-datatype");
    if (res.length == 1) {
        var x = res.val();
        if (typeof x !== 'undefined' && x != '' && is_list) {
            return "LIST<" + x + ">";
        }
        return (x == '' ? undefined : x);
    }

    res = $(element).find(".caosdb-entity-panel-heading[data-entity-datatype]");
    if (res.length == 1) {
        var x = res.attr("data-entity-datatype");
        return (x == '' ? undefined : x);
    }
    return undefined;
}

/**
 * Return the name of element.
 * If the corresponding label can not be found or the label is ambigious undefined is returned.
 * @return A string containing the name of the element.
 */
function getEntityName(element) {
    // TODO deprecated class name
    if ($(element).find('[data-entity-name]').length == 1) {
        return $(element).find('[data-entity-name]')[0].dataset.entityName;
    } else if (typeof $(element).find('.caosdb-f-entity-name').val() !== 'undefined') {
        return $(element).find('.caosdb-f-entity-name').val();
    }
    var res = element.getElementsByClassName("caosdb-label-name");
    if (res.length == 1) {
        return res[0].textContent;
    }
    return undefined;
}

/**
 * Return the path of element.
 * If the corresponding label can not be found or the label is ambigious undefined is returned.
 * @return A string containing the name of the element.
 */
function getEntityPath(element) {
    return getEntityHeadingAttribute(element, "path");
}

/**
 * Return the id of an entity.
 * @param element The element holding the entity.
 * @return The id of the entity as a String or undefined if no ID could be found.
 * @throws An error if the ID was ambigous.
 */
function getEntityID(element) {
    if (typeof element.dataset.entityId !== 'undefined') {
        return element.dataset.entityId;
    }
    // var res = element.getElementsByClassName("caosdb-id");
    var res = findElementByConditions(element, x => x.classList.contains("caosdb-id"),
        x => x.classList.contains("caosdb-entity-panel-body"));
    if (res.length == 1)
        return res[0].textContent;
    else if (res.length == 0)
        return undefined;
    throw "id is ambigous for this element!"
}

/**
 * Take a date and a time and format it into a CaosDB compatible representation.
 * @param date A date
 * @param time A time
 * @return A CaosDB compatible representation.
 */
function input2caosdbDate(date, time) {
    return date + "T" + time;
}

/**
 * Take a datetime from caosdb and return a date and a time
 * suitable for html inputs.
 *
 * If the text contains only a date it is returned.
 *
 * @param text The string from CaosDB.
 * @return A new string for the input element.
 */
function caosdb2InputDate(text) {
    if (text.includes("T")) {
        var spl = text.split("T");
        return [spl[0], spl[1].substring(0, 8)];
    }
    return [text];
}

/**
 * Return the id of an href attribute.
 * This is needed for retrieving an ID that is contained in a link to another entity.
 *
 * Some anker tags have a data-entity-id tag instead which is returned.
 *
 * @param el The element holding the link.
 * @return A String holding the ID or undefined if no href attribute could be found.
 */
function getIDfromHREF(el) {
    if (el.hasAttribute("href")) {
        let attr = el["href"];
        let idstr = attr.substring(attr.lastIndexOf("/") + 1);
        // Currently the XSLT returns wrong links for missing IDs.
        if (idstr.length == 0)
            return undefined;
        return idstr;
    } else if (el.hasAttribute("data-entity-id")) {
        return el.getAttribute("data-entity-id");
    }
    return undefined;
}

/**
 * Return an entity heading attribute from an element.
 * @param element The element holding the entity.
 * @return The value of the entity heading attribute or undefined if it is not present.
 */
function getEntityHeadingAttribute(element, attribute_name) {
    var res = element.getElementsByClassName("caosdb-entity-heading-attr");

    for (var i = 0; i < res.length; i++) {
        var subres = res[i].getElementsByClassName("caosdb-entity-heading-attr-name");
        if (subres.length != 1) {
            throw "Entity heading attribute does not have a name.";
        }
        if (subres[0].textContent == attribute_name + ":") {
            return res[i].childNodes[1].textContent;
        }
    }
    return undefined;
}

/**
 * Return the entity attribute description from an element.
 * @param element The element holding the entity.
 * @return The value of the description or undefined if it is not present.
 */
function getEntityDescription(element) {
    if ($(element).find('[data-entity-description]').length == 1) {
        return $(element).find('[data-entity-description]')[0].dataset.entityDescription;
    } else if (typeof $(element).find('.caosdb-f-entity-description').val() !== 'undefined') {
        return $(element).find('.caosdb-f-entity-description').val();
    }

    return getEntityHeadingAttribute(element, "description");
}

/**
 * Return the parents of an entity.
 * @param element The element holding the entity.
 * @return A list of objects with name and id of the parents.
 */
function getParents(element) {
    var res = element.getElementsByClassName("caosdb-parent-name");
    var list = [];
    for (var i = 0; i < res.length; i++) {
        list.push({
            id: getIDfromHREF(res[i]),
            name: res[i].textContent
        });
    }
    return list;
}

/**
 * Find all elements that fulfill a condition.
 * Don't traverse elements if except condition is matched.
 * @param element The start node.
 * @param condition The condition.
 * @param except The stop condition.
 */
function findElementByConditions(element, condition, except) {
    let found = []
    let echild = element.children;

    for (var i = 0; i < echild.length; i++) {
        if (condition(echild[i])) {
            found.push(echild[i]);
        }

        if (!except(echild[i])) {
            found.push.apply(found, findElementByConditions(echild[i], condition, except));
        }
    }
    return found;
}

/**
 *  Return a list of property objects from the dom property element.
 *  @param propertyelement: A HTMLElement identifying the property element.
 *  @param names: a map of names tracking the count of usage for each name (optional)
 *
 * TODO: Retrieval when using list element preview is currently broken.
 **/
function getPropertyFromElement(propertyelement, names = undefined) {

    let property = new Object({});
    let namel = propertyelement.getElementsByClassName("caosdb-property-name")[0];
    let valel = propertyelement.getElementsByClassName("caosdb-property-value")[0];
    let dtel = propertyelement.getElementsByClassName("caosdb-property-datatype")[0];
    let idel = propertyelement.getElementsByClassName("caosdb-property-id")[0];
    let unitel = valel.getElementsByClassName("caosdb-unit")[0];

    if (typeof unitel == "undefined") {
        property.unit = undefined;
    } else {
        property.unit = unitel.textContent.trim();
    }
    if (namel === undefined) {
        property.name = undefined;
    } else {
        property.name = namel.textContent;
    }
    if (idel === undefined) {
        property.id = undefined;
    } else {
        property.id = idel.textContent;
    }
    if (dtel === undefined) {
        property.datatype = undefined;
        property.list = undefined;
        property.reference = undefined;
        property.value = "";
    } else {
        property.datatype = dtel.textContent;

        var base_datatypes = ["TEXT", "INTEGER", "DOUBLE", "DATETIME", "BOOLEAN"];
        if (property.datatype.substring(0, 4) == "LIST") {
            property.list = true;
            property.value = [];
            var list_datatype = property.datatype.substring(5, property.datatype.length - 1);
            property.reference = base_datatypes.filter(el => el == list_datatype).length == 0;
        } else {
            property.list = false;
            property.value = "";
            property.reference = base_datatypes.filter(el => el == property.datatype).length == 0;
        }

    }

    if (!property.list && valel.getElementsByClassName("caosdb-property-text-value").length == 1) {
        valel = valel.getElementsByClassName("caosdb-property-text-value")[0];
    }

    // Needed for multiple properties with the same name:
    // It is not set when names is undefined.
    if (!(names === undefined)) {
        if (names.has(property.name)) {
            names.set(property.name, names.get(property.name) + 1);
        } else {
            names.set(property.name, 0);
        }
        property.duplicateIndex = names.get(property.name);
    }

    if (valel !== undefined && valel.textContent.length > 0) {
        // This is set to true, when there is a reference or a list of references:
        property.reference = (valel.getElementsByClassName("caosdb-id").length > 0);
        if (property.list) {
            // list datatypes
            let listel;
            if (property.reference) {
                // list of referernces
                // TODO: Fix list preivew here. Fixed, but untested.
                listel = findElementByConditions(valel, x => x.classList.contains("caosdb-resolvable-reference"),
                    x => x.classList.contains("caosdb-preview-container"));
                for (var j = 0; j < listel.length; j++) {
                    property.value.push(getIDfromHREF(listel[j]));
                }
            } else {
                // list of anything but references
                // TODO: Fix list preivew here. Fixed, but untested.
                listel = findElementByConditions(valel, x => x.classList.contains("list-group-item"),
                    x => x.classList.contains("caosdb-preview-container"));
                for (var j = 0; j < listel.length; j++) {
                    property.value.push(listel[j].textContent);
                }
            }
        } else if (property.reference) {
            // reference datatypes
            // let el = findElementByConditions(valel, x => x.classList.contains("caosdb-id"),
            //                                  x => x.classList.contains("caosdb-preview-container"));
            property.value = getIDfromHREF(valel.getElementsByTagName("a")[0]);
        } else {
            // all other datatypes
            property.value = valel.textContent.trim();
        }
    }


    return property;
}

/**
 * Get the properties from an entity.
 * @param element The element holding the entity.
 * @return a list of dom elements containing the properties.
 */
function getPropertyElements(element) {
    return findElementByConditions(element,
        x => x.classList.contains("list-group-item") &&
        x.classList.contains("caosdb-property-row"),
        x => x.classList.contains("caosdb-preview-container"));
}

/**
 * Return high level representations of the properties of an entity.
 * @param element The element holding the entity.
 * @return a list of objects for the properties according to the following specification.
 *
 *
 * Specification of properties:
 * prop = {
 *     name: ... // String, name of the property
 *     id: ... // String, id of the property
 *     value: ... // value of the property
 *     datatype: ... // full datatype, e.g. INTEGER or LIST<Uboot>
 *     duplicateIndex: ... // Integer starting from 0 and increasing for multiple properties
 *     reference: ... // Is this holding an ID of a reference? (boolean)
 *     list: ... // Is this a list? (boolean)
 *   }
 *
 *
 */
function getProperties(element) {
    var res = getPropertyElements(element);
    var list = [];

    var names = new Map();
    for (var i = 0; i < res.length; i++) {
        let property = getPropertyFromElement(res[i], names);
        list.push(property);
    }
    return list;
}

/**
 * Sets a property with some basic type checking.
 *
 * @param valueelement The dom element where the text content is to be set.
 * @param property The new property.
 * @param propold The old property belonging to valueelement.
 *
 * TODO: Server string is hardcoded.
 */
function setPropertySafe(valueelement, property, propold) {
    const serverstring = connection.getBasePath() + "Entity/";
    if (propold.list) {
        if (property.value.length === undefined) {
            throw ("New property must be a list.");
        }

        // Currently problems, when list is set to [] and afterwards to something different.

        if (propold.reference) {
            var finalstring;
            if (property.value.length == 0) {
                finalstring = "";
            } else {
                finalstring = '';
                for (var i = 0; i < property.value.length; i++) {
                    finalstring += '<a class="btn btn-default btn-sm caosdb-resolvable-reference" href="' + serverstring + property.value[i] + '"><span class="caosdb-id">' + property.value[i] + '</span><span class="caosdb-resolve-reference-target" /></a>';
                }
            }
            valueelement.getElementsByClassName("caosdb-value-list")[0].getElementsByClassName("caosdb-overflow-content")[0].innerHTML = finalstring;
        } else {
            throw ("Not Implemented: Lists with no references.");
        }
    } else if (propold.reference) {
        var llist = valueelement.getElementsByTagName("a");
        if (llist.length > 0) {
            var ael = llist[0];
            ael.setAttribute("href", serverstring + property.value);
            ael.innerHTML = '<span class="caosdb-id">' + property.value + '</span><span class="caosdb-resolve-reference-target" />';
        } else {
            finalstring = '<a class="btn btn-default btn-sm caosdb-resolvable-reference" href="' + serverstring + property.value + '"><span class="caosdb-id">' + property.value + '</span><span class="caosdb-resolve-reference-target" /></a>';
            valueelement.innerHTML = finalstring;
            preview.init();
        }
    } else {
        valueelement.innerHTML = "<span class='caosdb-property-text-value'>" + property.value + "</span>";
    }
}

/**
 * Set a property in the dom model.
 * @author Alexander Schlemmer
 * @param element The document element of the record.
 * @param property The new property as an object.
 * @return The number of properties that were set.
 *
 * If multiple properties with the same name exist and the property
 * to be set has not duplicateIndex, all properties will be set to that value.
 */
function setProperty(element, property) {
    var elementp = element.getElementsByClassName("caosdb-properties")[0];
    var res = elementp.getElementsByClassName("list-group-item");
    var dindex = property.duplicateIndex;
    var counter = 0;
    for (var i = 0; i < res.length; i++) {
        if (res[i].classList.contains("caosdb-properties-heading")) continue;
        let propold = getPropertyFromElement(res[i]);
        if (propold.name === property.name) {
            if (dindex > 0) {
                dindex--;
                continue;
            }
            setPropertySafe(res[i].getElementsByClassName("caosdb-property-value")[0],
                property,
                propold);
            counter++;
        }
    }
    return counter;
}

/**
 * Get a property by name.
 * @param element The element holding the entity.
 * @param property_name The name of the property.
 * @param case_sensitive If true search for property names case-sensitively. Otherwise neglect the case ("A" and "a" are equivalent).
 *                         Optional. Default is true.
 * @return The value of the the property with property_name.
 * This function returns undefined when this property is not available for this entity.
 */
function getProperty(element, property_name, case_sensitive=true) {
    var props;
    if (case_sensitive) {
        props = getProperties(element).filter(el => el.name == property_name);
    } else {
        props = getProperties(element).filter(el => el.name.toLowerCase() == property_name.toLowerCase());
    }
    if (props.length == 0) {
        return undefined;
    }
    return props[0].value;
}

/**
 * Helper function for setting an id and or a name if contained in parent.
 * @param parentElement The element which is to recieve the attributes.
 * @param parent The object possibly containing an id and or a name.
 */
function setNameID(parentElement, parent) {
    if (typeof parent.id !== 'undefined' && parent.id !== '') {
        parentElement.setAttribute("id", parent.id);
    }
    if (typeof parent.name !== 'undefined' && parent.name !== '') {
        parentElement.setAttribute("name", parent.name);
    }
}

/**
 * Append a parent node to an XML document.
 * @see getParents
 * @param doc A document for the XML.
 * @param element The element to append to.
 * @param parent An object containing a name and or an id.
 */
function appendParent(doc, element, parent) {
    var parentElement = doc.createElement("Parent");
    setNameID(parentElement, parent);
    element.appendChild(parentElement);
}

/**
 * Append a text node with name name and value value to element element.
 * @param doc A document for the XML.
 * @param element
 * @param name
 * @param value
 */
function appendValueNode(doc, element, name, value) {
    let el = doc.createElement(name);
    let valel = doc.createTextNode(value);
    el.appendChild(valel);
    element.appendChild(el);
}

/**
 * Append a property node to an XML document.
 * @see getProperties
 * @param doc An document for the XML.
 * @param element The element to append to.
 * @param property An object specifying a property.
 */
function appendProperty(doc, element, property, append_datatype = false) {
    var propertyElement = doc.createElement("Property");
    setNameID(propertyElement, property);
    if (append_datatype == true) {
        propertyElement.setAttribute("datatype", property.datatype);
    }
    if (typeof property.unit !== 'undefined') {
        propertyElement.setAttribute("unit", property.unit);
    }

    if (!(property.value === undefined)) {
        if (("list" in property && property.list) || property.value instanceof Array) {
            if (property.value instanceof Array) {
                for (var i = 0; i < property.value.length; i++) {
                    appendValueNode(doc, propertyElement, "Value", property.value[i]);
                }
            } else {
                appendValueNode(doc, propertyElement, "Value", property.value);
            }
        } else {
            let valel = doc.createTextNode(property.value);
            propertyElement.appendChild(valel);
        }
    }

    element.appendChild(propertyElement);
}


/**
 * Create an XML for an entity.
 * This function uses the object notation.
 * @see getProperties
 * @see getParents
 * @param role Record, RecordType or Property
 * @param name The name of the entity. Can be undefined.
 * @param id The id of the entity. Can be undefined.
 * @param properties A list of properties.
 * @param parents A list of parents.
 * @return A document holding the newly created entity.
 *
 */
function createEntityXML(role, name, id, properties, parents,
    append_datatypes = false, datatype = undefined, description = undefined, unit = undefined) {
    var doc = document.implementation.createDocument(null, role, null);
    var nelnode = doc.children[0];
    setNameID(nelnode, {
        name: name,
        id: id
    });

    if (typeof datatype !== 'undefined' && datatype.length > 0) {
        $(nelnode).attr("datatype", datatype);
    }

    if (typeof description !== 'undefined' && description.length > 0) {
        $(nelnode).attr("description", description);
    }

    if (typeof unit !== 'undefined' && unit.length > 0) {
        $(nelnode).attr("unit", unit);
    }

    if (!(parents === undefined)) {
        for (var i = 0; i < parents.length; i++) {
            appendParent(doc, nelnode, parents[i]);
        }
    }

    if (!(properties === undefined)) {
        for (var i = 0; i < properties.length; i++) {
            appendProperty(doc, nelnode, properties[i], append_datatypes);
        }
    }
    return doc;
}

/**
 * Helper function to wrap xml documents into another node which could e.g. be
 * Update, Response, Delete.
 * @param The name of the newly created top level node.
 * @param The xml document.
 * @return A new xml document.
 */
function wrapXML(elementname, xml) {
    var doc = document.implementation.createDocument(null, elementname, null);

    doc.children[0].appendChild(xml.children[0]);
    return doc;
}

/**
 * Convert this xml document into an update.
 * @param The xml document.
 * @return A new xml document.
 */
function createUpdate(xml) {
    return wrapXML("Request", xml);
}

/**
 * Convert this xml document into an insert.
 * @param The xml document.
 * @return A new xml document.
 */
function createInsert(xml) {
    return wrapXML("Request", xml);
}

/**
 * Convert this xml document into a response.
 * @param The xml document.
 * @return A new xml document.
 */
function createResponse(xml) {
    return wrapXML("Response", xml);
}


/**
 * Retrieve an entity by using an id or a name.
 * @param id The id of the entity. Can be undefined when name is used.
 * @param name The name of the entity. Can be undefined when id is used.
 * @return The element holding that entity.
 */
async function retrieve(id = undefined, name = undefined) {
    var retstr = id;
    if (id === undefined) {
        retstr = name;
    }
    let entities = await connection.get("Entity/" + retstr);
    return transformation.transformEntities(entities);
}

/**
 * Query the database for querytext.
 * @param querytext The search query.
 * @return An array of entities.
 */
async function query(querytext) {
    let entities = await connection.get("Entity/?query=" + querytext);
    return transformation.transformEntities(entities);
}

/**
 * Retrive one entity given by ID id
 * and transform them into single entity objects.
 * @param id The id.
 * @return An array of entities.
 */
async function retrieve_dragged_property(id) {
    let entities = await connection.get("Entity/" + id);
    return transformation.transformProperty(entities);
}

/**
 * Retrieve all properties and record types and convert them into
 * a web page using a specialized XSLT named entity palette.
 * @return An array of entities.
 */
async function retrieve_data_model() {
    // TODO possibly allow single query
    let props = await connection.get("Entity/?query=FIND Property");
    let rts = await connection.get("Entity/?query=FIND RecordType");
    console.log("HERE");
    console.log(props);
    console.log(rts);
    for (var p of props.children[0].children) {
        rts.children[0].appendChild(p.cloneNode());
    }
    return transformation.transformEntityPalette(rts);
}


/**
 * Update an entity using its xml representation.
 * @param xml The xml of the entity which will be automatically wrapped with an Update.
 */
async function update(xml) {
    return transaction.updateEntitiesXml(createUpdate(xml));
}

/**
 * Insert an entity using its xml representation.
 * @param xml The xml of the entity which will be automatically wrapped with an Insert.
 */
async function insert(xml) {
    return transaction.insertEntitiesXml(createInsert(xml));
}
