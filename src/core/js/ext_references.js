/**
 * Resolve References
 * Alexander Schlemmer, 11/2018
 */


/*!
 * Check if an element is out of the viewport
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Node}  elem The element to check
 * @return {Object}     A set of booleans for each side of the element
 */
// Source address: https://gomakethings.com/how-to-check-if-any-part-of-an-element-is-out-of-the-viewport-with-vanilla-js/
var isOutOfViewport = function (elem) {

	// Get element's bounding
	var bounding = elem.getBoundingClientRect();

	// Check if it's out of the viewport on each side
	var out = {};
	out.top = bounding.top < 0;
	out.left = bounding.left < 0;
	out.bottom = bounding.bottom > (window.innerHeight || document.documentElement.clientHeight);
	out.right = bounding.right > (window.innerWidth || document.documentElement.clientWidth);
	out.any = out.top || out.left || out.bottom || out.right;
	out.all = out.top && out.left && out.bottom && out.right;
	return out;
};

// Variation that checks whether an element is inside of the viewport.
var isInViewport = function (elem) {
    var bounding = elem.getBoundingClientRect();
    var out = {};
    out.top = bounding.bottom < 0;
    out.left = bounding.left < 0;
    out.bottom = bounding.top > (window.innerHeight || document.documentElement.clientHeight);
    out.right = bounding.left > (window.innerWidth || document.documentElement.clientWidth);
    return !(out.top || out.bottom);
    // return !(out.top || out.left || out.bottom || out.right);
}



var resolve_references = new function() {
    this.init = function() {
        // this.references();
        this.update_visible_references();
    }

    this.get_person_str = function(el) {
        var valpr = getProperties(el);
        if (valpr == undefined) {
            return;
        }
        return valpr.filter(valprel => valprel.name.toLowerCase() == "firstname")[0].value +
            " " + valpr.filter(valprel => valprel.name.toLowerCase() == "lastname")[0].value;
    }

    /*
     * Function that retrieves meaningful information for a single element.
     *
     * This function needs to be customized for specific implementations.
     *
     * rs: Element having the class caosdb-resolvable-reference and including a caosdb-resolve-reference target.
     */
    this.update_single_resolvable_reference = async function(rs) {
        var rseditable = rs.getElementsByClassName("caosdb-resolve-reference-target")[0];
        var el = await retrieve(getIDfromHREF(rs));
        var pr = getParents(el[0]);
        if (getEntityHeadingAttribute(el[0], "path") !== undefined || pr[0].name == "Image") {
            var pths = getEntityHeadingAttribute(el[0], "path").split("/");
            rseditable.textContent = pths[pths.length - 1];
        } else if (pr[0].name == "Person") {
            rseditable.textContent = this.get_person_str(el[0]);
        } else if (pr[0].name == "ExperimentSeries") {
            rseditable.textContent = getEntityName(el[0]);
        } else if (pr[0].name == "BoxType") {
            rseditable.textContent = getEntityName(el[0]);
        } else if (pr[0].name == "Loan") {
            var persel = await retrieve(getProperty(el[0], "Borrower"));
            var loan_state = awi_demo.get_loan_state_string(getProperties(el[0]));
            rseditable.textContent = "Borrowed by " + this.get_person_str(persel[0]) + " (" + loan_state.replace("_", " ") + ")";
        } else if (pr[0].name == "Box") {
            rseditable.textContent = getProperty(el[0], "Number");
        } else if (pr[0].name == "Palette") {
            rseditable.textContent = getProperty(el[0], "Number");
        } else if (pr[0].name.includes("Model")) {
            rseditable.textContent = pr[0].name;
        } else {
            rseditable.textContent = el[0].id;
        }
    }

    /*
     * This function updates all references that are inside of the current viewport.
     *
     */
    this.update_visible_references = async function() {
        var rs = document.getElementsByClassName("caosdb-resolvable-reference");

        for (var i = 0; i < rs.length; i++) {
            if (isInViewport(rs[i])) {
                this.update_single_resolvable_reference(rs[i]);
            }
        }
    }

    this.references = async function() {
        var rs = document.getElementsByClassName("caosdb-resolvable-reference");

        for (var i = 0; i < rs.length; i++) {
            this.update_single_resolvable_reference(rs[i]);
        }
    }
}


$(document).ready(function() {
    resolve_references.init();
    var scrollTimeout = undefined;
    $(window).scroll(()=> {
        if (scrollTimeout) {
            clearTimeout(scrollTimeout);
        }
        scrollTimeout = setTimeout(function() {
            resolve_references.update_visible_references();
        }, 500);
    });
});
