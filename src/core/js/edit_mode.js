/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 Henrik tom Wörden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/**
 * Edit mode module
 */
var edit_mode = new function() {

    this.init = function() {
        if (isAuthenticated()) {
            this.add_edit_mode_button();
            if (this.is_edit_mode()) {
                this.enter_edit_mode();
                this.toggle_edit_panel();
            }
            this.scroll_edit_panel();
            window.onscroll = this.scroll_edit_panel;
            $('.caosdb-f-edit').css("transition", "top 1s");
        } else {
            window.localStorage.removeItem("edit_mode");
        }
    }

	// TODO: do all these listener need to be part of edit_mode or can they be
	// declared with "var"
    this.scroll_edit_panel = function(e) {
        $('.caosdb-f-edit').css("top", document.documentElement.scrollTop);
    }

    this.prop_dragstart = function(e) {
        e.dataTransfer.setData("text/plain", e.target.id);
    }

    this.prop_dragleave = function(e) {
        edit_mode.unhighlight();
    }

    this.prop_dragover = function(e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = "copy";
        edit_mode.highlight(this);
    }

	this.property_drop_listener = function(e) {
		e.preventDefault();
		console.log("inn drop listener")
		var entity = $(this).parent();
		edit_mode.app.startEdit(entity);
		edit_mode.add_dropped_property(e, edit_mode.app.entity);
	}

	// Define listeners for dropping record types or properties on existing entites.
	this.parent_drop_listener = function(e) {
		e.preventDefault();
		var entity = $(this).parent();
		edit_mode.app.startEdit(entity);
		edit_mode.add_dropped_parent(e, edit_mode.app.entity);
	}

    this.add_new_property = function(entity, new_prop) {
        var rt = entity.getElementsByClassName("caosdb-properties")[0];
        rt.appendChild(new_prop);
        edit_mode.make_property_editable(new_prop);
    }

    this.add_dropped_property = function(e, panel) {
        var propsrcid = e.dataTransfer.getData("text/plain");
        var tmp_id = propsrcid.split("-");
        var prop_id = tmp_id[tmp_id.length - 1];
        var entity_type = tmp_id[tmp_id.length - 2];
        if (entity_type == "p") {
            retrieve_dragged_property(prop_id).then(new_prop_doc => {
                edit_mode.add_new_property(panel, new_prop_doc.firstChild);
            }, edit_mode.handle_error);
        } else if (entity_type == "rt") {
            var name = $("#" + propsrcid).text();
            var dragged_rt = str2xml('<Response><Property id="' + prop_id + '" name="' + name + '" datatype="' + name + '"></Property></Response>');
            transformation.transformProperty(dragged_rt).then(new_prop_doc => {
                edit_mode.add_new_property(panel, new_prop_doc.firstChild);
            }, edit_mode.handle_error);
        }
    }

    this.add_dropped_parent = function(e, panel) {
        var propsrcid = e.dataTransfer.getData("text/plain");
        var parent_list = panel.getElementsByClassName("caosdb-f-parent-list")[0]
        var tmp_id = propsrcid.split("-");
        var prop_id = tmp_id[tmp_id.length - 1];
        var entity_type = tmp_id[tmp_id.length - 2];
        if (entity_type == "p") {
            console.log("SHOULD not happend")
        } else if (entity_type == "rt") {
            var name = $("#" + propsrcid).text();
            var dragged_rt = str2xml('<Response><RecordType id="' + prop_id + '" name="' + name + '"></RecordType></Response>');
            transformation.transformParent(dragged_rt).then(new_prop => {
                parent_list.appendChild(new_prop);
                /*
                edit_mode.add_one_delete_button(
                    parent_list.children[parent_list.children.length-1],
                    is_parent=true
                );
                */
                edit_mode.add_parent_delete_buttons(panel);
            }, edit_mode.handle_error);

        }
    }

    this.set_entity_dropable = function(entity) {
        var rts = entity.getElementsByClassName("caosdb-entity-panel-body");
        for (var rel of rts) {
            rel.addEventListener("dragleave", edit_mode.prop_dragleave);
            rel.addEventListener("dragover", edit_mode.prop_dragover);
            rel.addEventListener("drop", edit_mode.property_drop_listener);
        }
        var headings = entity.getElementsByClassName("caosdb-entity-panel-heading");
        for (var rel of headings) {
            rel.addEventListener("dragleave", edit_mode.prop_dragleave);
            rel.addEventListener("dragover", edit_mode.prop_dragover);
            rel.addEventListener("drop", edit_mode.parent_drop_listener);
        }

    }

    this.unset_entity_dropable = function(entity) {
        var rts = entity.getElementsByClassName("caosdb-entity-panel-body");
        for (var rel of rts) {
            rel.removeEventListener("dragleave", edit_mode.prop_dragleave);
            rel.removeEventListener("dragover", edit_mode.prop_dragover);
            rel.removeEventListener("drop", edit_mode.property_drop_listener);
        }
        var headings = entity.getElementsByClassName("caosdb-entity-panel-heading");
        for (var rel of headings) {
            rel.removeEventListener("dragleave", edit_mode.prop_dragleave);
            rel.removeEventListener("dragover", edit_mode.prop_dragover);
            rel.removeEventListener("drop", edit_mode.parent_drop_listener);
        }
    }


    this.remove_save_button = function(ent) {
        $(ent).find('.caosdb-f-entity-save-button').remove();
    }

    this.add_save_button = function(ent, callback) {
        var save_btn = $('<button class="btn btn-link caosdb-update-entity-button caosdb-f-entity-save-button">Save</button>');

        $(ent).find(".caosdb-f-edit-mode-entity-actions-panel").append(save_btn);

        $(save_btn).click(function() {
            callback();
            // edit_mode.update_entity(this.dataset.editid);
        });
    }

    /* TODO
    this.add_edit_xml_button = function(entity) {
        var button = $('<button class="btn btn-link caosdb-update-entity-button" title="Edit the XML representaion of this entity."><span class="glyphicon glyphicon-pencil"/> Edit XML</button>');
        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        var callback = (e) => {
            e.stopPropagation();
            transaction.update.initUpdate(entity); 
        }

        button.click(callback);
    }
    */

    /**
     * Remove all .caosdb-f-parent-trash-button in the entity header.
     * Then add a trash button to all parents if there are more than one
     * parents or if this is not a Record. Reason: Records must have at least
     * one parent whereas Properties and RecordTypes may come without parents.
     *
     * The newly created trash button removes the corresponding parent and
     * calls this function again to reset the trash buttons (and remove the
     * last trash button for the last parent if the header belongs to a
     * record).
     */
    this.add_parent_delete_buttons = function(header) {
        $(header).find(".caosdb-f-parent-trash-button").remove();
        var parents = $(header).find(".caosdb-parent-item");
        if ((parents.length > 1) || getEntityRole(header) != "Record") {
            for (var par of parents) {
                edit_mode.add_parent_trash_button($(par).find('.caosdb-f-parent-actions-panel')[0], par, () => {
                    edit_mode.add_parent_delete_buttons(header);
                });
            }
        }
    }

    /**
     * Append a trash button with class "caosdb-f-parent-trash-button", bind a
     * remove on the deletable, and bind a callback function to click.
     */
    this.add_parent_trash_button = function(appendable, deletable, callback = undefined) {
        edit_mode.add_trash_button(appendable, deletable, "caosdb-f-parent-trash-button", callback);
    }

    /**
     * Append a trash button with class "caosdb-f-property-trash-button" and
     * bind a remove on the deletable.
     */
    this.add_property_trash_button = function(appendable, deletable) {
        edit_mode.add_trash_button(appendable, deletable, "caosdb-f-property-trash-button");
    }

    /**
     * Append a trash button which removes the deletable and calls an optional
     * callback function
     */
    this.add_trash_button = function(appendable, deletable, className, callback = undefined) {
        var button = $('<button class="btn btn-link ' + className + ' caosdb-f-entity-trash-button"><span class="glyphicon glyphicon-trash"></span></button>');
        $(appendable).append(button);
        button.click((e) => {
            e.stopPropagation();
            deletable.remove();
            if (typeof callback == 'function') {
                callback();
            }
        });
    }


    this.update_entity_by_id = async function(ent_id) {
        var ent_element = $("#" + ent_id)[0];
        return this.update_entity(ent_element);
    }

    this.insert_entity = async function(ent_element) {
        var xml = createEntityXML(
            getEntityRole(ent_element),
            getEntityName(ent_element),
            undefined,
            edit_mode.getProperties(ent_element),
            getParents(ent_element),
            true,
            getEntityDatatype(ent_element),
            getEntityDescription(ent_element),
            getEntityUnit(ent_element),
        );
        return await insert(xml);
    }

    /**
     * Get a list of js objects representing the properties of the entity.
     * Calls getPropertyFromElement on each property row found.
     *
     * TODO merge with caosdb.js->getProperties
     *
     * ent_element : {HTMLElement} entity in view mode
     */
    this.getProperties = function(ent_element) {
        var properties = [];
        for (var element of $(ent_element).find('.caosdb-property-row')) {

            var valfield = $(element).find(".caosdb-property-value");
            var editfield = $(element).find(".caosdb-property-edit-value");
            var property = getPropertyFromElement(element);

            property.unit = editfield.find(".caosdb-unit").val();
            if (property.datatype == "TEXT" ||
                property.datatype == "DATE" ||
                property.datatype == "DOUBLE" ||
                property.datatype == "INTEGER" ||
                property.datatype == "BOOLEAN" ||
                property.datatype == "FILE") {
                property.value = editfield.find(":input").val()
            } else if (property.datatype == "DATETIME") {
                let es = editfield.find(":input");
                if (es.length == 2) {
                    property.value = input2caosdbDate(
                        es[0].value,
                        es[1].value);
                } else if (es[0]) {
                    property.value = es[0].value;
                }
            } else if (property.reference) {
                if (!property.list) {
                    property.value = $(editfield).find("select").first()[0].selectedOptions[0].value;
                } else {
                    throw ("Reference lists not supported. Please issue a feature request.");
                }
            } else {
                throw ("This property's data type is not supported by the webui. Please issue a feature request for support for `" + property.datatype + "`.");
            }
            properties.push(property);
        }
        return properties;

    }

    this.update_entity = async function(ent_element) {
        var xml = createEntityXML(
            getEntityRole(ent_element),
            getEntityName(ent_element),
            getEntityID(ent_element),
            edit_mode.getProperties(ent_element),
            getParents(ent_element),
            true,
            getEntityDatatype(ent_element),
            getEntityDescription(ent_element),
            getEntityUnit(ent_element),
        );
        return await update(xml);
    }

    this.add_edit_mode_button = function() {
        var edit_mode_li = $('<li><button class="navbar-btn btn btn-link caosdb-f-btn-toggle-edit-mode">Edit Mode</button></li>');

        $("#top-navbar").find("ul").first().append(edit_mode_li);

        $(".caosdb-f-btn-toggle-edit-mode").click(() => {
            console.log("toggle edit mode");
            edit_mode.toggle_edit_mode();
        });
    }

    this.toggle_edit_mode = function() {
        this.toggle_edit_panel();
        if (this.is_edit_mode()) {
            this.leave_edit_mode();
        } else {
            this.enter_edit_mode();
        }
    }

    /**
     * To be overridden by an instance of `leave_edit_mode_template` during the
     * `enter_edit_mode()` execution.
     */
    this.leave_edit_mode = function() {}

    this.enter_edit_mode = function(editApp = undefined) {
        console.log("enter edit mode");
        window.localStorage.edit_mode = "true";

        var editPanel = this.get_edit_panel();
        removeAllWaitingNotifications(editPanel);
        this.add_wait_datamodel_info();
        edit_mode.init_actions_panels();

        retrieve_data_model().then(model => {
            removeAllWaitingNotifications(editPanel);
            editPanel.innerHTML = xml2str(model);
            $(".caosdb-f-btn-toggle-edit-mode").text("Leave Edit Mode");
            edit_mode.init_dragable();
            if (typeof editApp == "undefined") {
                var editApp = this.init_edit_app();
            }
            this.leave_edit_mode = function() {
                edit_mode.leave_edit_mode_template(editApp);
            };
        }, edit_mode.handle_error);
    }


    this.init_actions_panels = function(){
        this.reset_actions_panels();
        $(".caosdb-entity-actions-panel").each(function(index) {
            var clone = $(this).clone(true)[0];
            $(clone).removeClass("caosdb-entity-actions-panel").addClass("caosdb-f-edit-mode-entity-actions-panel").insertBefore(this);
            $(clone).children().remove();
            $(this).hide();
        });
    }

    this.reset_actions_panels = function(){
        $(".caosdb-f-edit-mode-entity-actions-panel").remove();
        $(".caosdb-entity-actions-panel").show();
    }

    this.make_header_editable = function(entity) {
        var header = $(entity).find('.caosdb-entity-panel-heading');
        var roleElem = $(header).find('.caosdb-f-entity-role');
        roleElem.detach();
        var parentsElem = $(header).find('.caosdb-f-parent-list');
        parentsElem.detach();
        var temp = $('<div class="form-group"><label class="col-sm-2 control-label">parents</label><div class="col-sm-10"></div></div>');
        temp.find("div.col-sm-10").append(parentsElem);

        header.attr("title", "Drop parents from the right panel here.");
        header.data("toggle", "tooltip");

        // create inputs
        var inputs = [
            roleElem,
            temp,
            this.make_input("name", getEntityName(entity)),
            this.make_input("description", getEntityDescription(entity)),
        ];
        if (getEntityRole(roleElem[0]) == "Property") {
            for (input of this.make_datatype_input(getEntityDatatype(entity), getEntityUnit(entity))) {
                inputs.push(input);
            }
            temp.hide();
        } else if (getEntityRole(roleElem[0]) == "File") {
            inputs.push(this.make_input("path", getEntityPath(entity)));
        }
        // remove other stuff
        header.children().remove();
        header.append($('<form class="form-horizontal"></form>').append(inputs));
        edit_mode.make_dataype_input_logic(header);

        edit_mode.add_parent_delete_buttons(header[0]);
    }

    this.isListDatatype = function(datatype) {
        return (typeof datatype !== 'undefined' && datatype.substring(0, 5) == "LIST<");
    }

    this.unListDatatype = function(datatype) {
        return datatype.substring(5, datatype.length - 1);
    }

    this.make_dataype_input_logic = function(header) {
        var unitLabel = $(header).find(".caosdb-f-entity-unit-label");
        var unitInput = $(header).find(".caosdb-f-entity-unit");
        var isListLabel = $(header).find(".caosdb-f-entity-is-list-label");
        var isListInput = $(header).find(".caosdb-f-entity-is-list");
        var referenceLabel = $(header).find(".caosdb-f-entity-reference-label");
        var referenceInput = $(header).find(".caosdb-f-entity-reference");

        // TODO show on reference
        referenceInput.hide();
        referenceLabel.hide();

        // TODO show unit for double and integer
    }

    this.make_datatype_input = function(datatype, unit) {
        var is_list = edit_mode.isListDatatype(datatype);
        if (is_list) {
            datatype = edit_mode.unListDatatype(datatype);
        }

        // datatypes {name: has_unit?}
        var datatypes = {
            "TEXT": false,
            "DOUBLE": true,
            "INTEGER": true,
            "DATETIME": false,
            "BOOLEAN": false,
            /*TODO "REFERENCE":false*/
        }
        var select = $('<select></select>');
        for (dt in datatypes) {
            select.append('<option data-has-refid="' + (dt == "REFERENCE") + '" data-has-unit="' + datatypes[dt] + '"  value="' + dt + '" ' + (dt == datatype ? 'selected="true"' : '') + '>' + dt + '</option>');
        }

        return [
            $('<div class="form-group"><label class="col-sm-2 control-label caosdb-f-entity-datatype-label">datatype</label><div class="col-sm-3"><select class="form-control caosdb-f-entity-datatype">' + select.html() + '</select></div><label class="col-sm-2 control-label caosdb-f-entity-reference-label">reference</label><div class="col-sm-3"><input readonly="true" class="form-control caosdb-f-entity-reference" value="" placeholder="Drop a RT"></input></div><label class="col-sm-1 control-label caosdb-f-entity-is-list-label">list</label><div class="col-sm-1"><input class="caosdb-f-entity-is-list" type="checkbox" ' + (is_list ? 'checked="true" ' : "") + '/></div>')[0],
            $('<div class="form-group"><label class="col-sm-2 control-label caosdb-f-entity-unit-label">unit</label><div class="col-sm-2"><input type="text" class="form-control caosdb-f-entity-unit" value="' + (typeof unit == 'undefined' ? "" : unit) + '"></input></div></div>')[0],
        ];
    }

    this.make_input = function(label, value) {
        return $('<div class="form-group"><label class="col-sm-2 control-label">' + label + '</label><div class="col-sm-10"><input type="text" class="form-control caosdb-f-entity-' + label + '" value="' + (typeof value == 'undefined' ? "" : value) + '"></input></div></div>')[0];
    }

    this.smooth_replace = function(from, to) {
        $(to).hide();
        $(from).fadeOut();
        $(from).after(to);
        $(from).detach();
        $(to).fadeIn();
    }

    /**
     * @param {HTMLElement} entity property in view mode representation.
     * @return {undefined}
     */ 
    this.make_property_editable = function(element) {
        if (typeof element == "undefined") {
            throw Error("parameter `element` was undefined.");
        }

        edit_mode.add_property_trash_button($(element).find(".caosdb-property-edit")[0], element);
        var valfield = $(element).find(".caosdb-property-value");
        var editfield = $(element).find(".caosdb-property-edit-value");
        var property = getPropertyFromElement(element);

        valfield.hide();
        editfield.show();
        editfield.text("");
        var editelementstring;
        if (property.datatype == "TEXT") {
            //editelementstring = "<input type='text' value='" + property.value + "'></input>";
            editelementstring = "<textarea>" + property.value + "</textarea>";
        } else if (property.datatype == "DATE") {
            let date = caosdbq2InputDate(property.value)[0];
            editelementstring = "<input type='date' value='" + date + "'></input>";
        } else if (property.datatype == "DATETIME") {
            let dateandtime = caosdb2InputDate(property.value);
            let date = dateandtime[0];
            if (dateandtime.length == 2) {
                let time = dateandtime[1];
                editelementstring = "<input type='date' value='" + date + "'></input>" +
                    "<input type='time' value='" + time + "'></input>";
            } else {
                editelementstring = "<input type='date' value='" + date + "'></input>";
            }
        } else if (property.datatype == "DOUBLE") {
            editelementstring = "<input type='number' step='any' value='" + property.value + "'></input><input class='caosdb-unit' title='unit' style='width: 60px;' placeholder='unit' value='" + (typeof property.unit == 'undefined' ? "" : property.unit) + "' type='text'></input>";
        } else if (property.datatype == "INTEGER") {
            editelementstring = "<input type='number' value='" + property.value + "'></input><input class='caosdb-unit' title='unit' style='width: 60px;' placeholder='unit' value='" + (typeof property.unit == 'undefined' ? "" : property.unit) + "' type='text'></input>";
        } else if (property.datatype == "FILE") {
            editelementstring = "<input type='text' value='" + property.value + "'></input>";
        } else if (property.datatype == "BOOLEAN") {
            editelementstring = $('<select class="form-control caosdb-list-' + property.datatype + '"><option selected value=""></option><option value="FALSE">FALSE</option><option value="TRUE">TRUE</option></select>');
            $(editelementstring).val(property.value);
        } else if (property.reference) {
            editelementstring = '<select class="form-control caosdb-list-' + property.datatype + '" data-resolved="false"><option selected class="caosdb-f-option-default" value="' + property.value + '">' + property.value + '</option><option></option></select>';
            edit_mode.retrieve_datatype_list(property.datatype).then(() => {
                var elist = $(editfield).find(".caosdb-list-" + property.datatype);
                elist.find("[value='" + property.value + "'].caosdb-f-option-default").remove();
                elist.find("[value='" + property.value + "']").attr("selected", "selected");
            }, edit_mode.handle_error);
            edit_mode.retrieve_datatype_list(property.datatype);
        } else {
            throw ("Unsupported data type: `" + property.datatype + "`. Please issue a feature request.");
        }
        editfield.append($(editelementstring));
    }

    this.create_new_record = async function(recordtype_id, name = undefined) {
        var rt = await retrieve(recordtype_id);
        var newrecord = createEntityXML("Record", undefined, undefined,
            getProperties(rt[0]),
            [{
                name: getEntityName(rt[0])
            }], true);
        var doc = str2xml("<Response/>");
        doc.firstElementChild.appendChild(newrecord.firstElementChild);
        // TODO I dunno whats wrong here: xml -> str -> xml ???
        var x = await transformation.transformEntities(str2xml(xml2str(doc)));
        return x[0];
    }


    this.init_dragable = function(){
        var props = document.getElementsByClassName("caosdb-f-edit-drag");
        for (var pel of props) {
            pel.addEventListener("dragstart", edit_mode.prop_dragstart);
            pel.setAttribute("draggable", true);
        }
    }

	/*
	 * This function treats the deletion of entities, i.e. when the "delete"
	 * button is clicked.
	 */
	this.delete_action = async function(){
		var app = edit_mode.app;
		// this is the callback of the delete button
		// show waiting notification
		edit_mode.smooth_replace(entity, app.waiting);
		// send delete request
		var response = await transaction.deleteEntities([getEntityID(entity)]);
			// transform the delete response
		var entities = await transformation.transformEntities(response); 
		// error of delete
		// continue with transformed entities
		edit_mode.smooth_replace(app.waiting, entities[0]);
		app.entity = entities[0];
		// treating error msg of delete
		if (edit_mode.has_errors(app.entity)) {
			hintMessages.hintMessages(app.entity);
			edit_mode.set_entity_dropable(app.entity);
			edit_mode.init_actions_panels();
			// only add edit button, because a deletion would fail again
			edit_mode.add_start_edit_button(app.entity, () => {
				app.startEdit(app.entity)
			});
			if (getEntityRole(app.entity) == "RecordType") {
				edit_mode.add_new_record_button(app.entity, () => {
					edit_mode.create_new_record(getEntityID(app.entity)).then((entity) => {
						app.newEntity(entity);
					}, edit_mode.handle_error);
				});
			}
		} else {
			// no error msg of delete
			// removes trash bin etc
			$(app.entity).find('.caosdb-f-edit-mode-entity-actions-panel').remove();
			// creates an ok button, which removes
			// deleted entity on click
			var closeButton = $(app.entity).find('.alert-info .close');
			closeButton.text("Ok");
			closeButton.click((e) => {
				$(app.entity).remove();
			});
		}
	}
    /*
     *  Initialize the edit mode and create a state machine.
     */
    this.init_edit_app = function() {
        // Panel that stores the "Create new Property" and "Create new RecordType" buttons.
        var new_buttons = $('.caosdb-f-edit-panel-new-button');
        var app = new StateMachine({
            transitions: [{
                name: "init",
                from: 'none',
                to: "initial"
            }, {
                name: "newEntity",
                from: "initial",
                to: "changed"
            }, {
                name: "startEdit",
                from: "initial",
                to: "changed"
            }, {
                name: "insert",
                from: 'changed',
                to: 'wait'
            }, {
                name: "update",
                from: 'changed',
                to: 'wait'
            }, {
                name: "showResults",
                from: 'wait',
                to: 'initial'
            }, {
                name: "cancel",
                from: 'changed',
                to: 'initial'
            }, {
                name: "finish",
                from: '*',
                to: 'final'
            }],
        });
        // Show a button "+" to create a new property when filter results in empty list.
        new_buttons.filter('.caosdb-f-hide-on-empty-input').parent().each(function(index) {
            var button = $(this);
            button.hide();
            var input = button.parent().find("input");
            input.on("input", function(e) {
                if (input.val() == '') {
                    button.fadeOut();
                } else {
                    button.fadeIn();
                }
            });
        });
        // handler for new property button
        // calls newEntity transition of state machine
        new_buttons.filter('.new-property').click(() => {
            edit_mode.create_new_entity("Property").then(entity => {
                app.newEntity(entity);
            }, edit_mode.handle_error);
        });
        // handler for new record type button
        // calls newEntity transition of state machine
        new_buttons.filter('.new-recordtype').click(() => {
            edit_mode.create_new_entity("RecordType").then(entity => {
                app.newEntity(entity);
            }, edit_mode.handle_error);
        });


        // Define the error handler for the state machine.
        app.errorHandler = function(fn) {
            try {
                fn();
            } catch (e) {
                edit_mode.handle_error(e);
            }
        };
        app.onEnterInitial = async function(e) {
            console.log(e);
            app.old = undefined;
            app.errorHandler(() => {
                // make entities dropable and freezable
                new_buttons.attr("disabled", false);
                $('.caosdb-entity-panel').each(function(index) {
                    let entity = this;
                    edit_mode.set_entity_dropable(entity);
                    if (typeof getEntityID(entity) == "undefined" || getEntityID(entity) == '') {
                        // no id -> insert
                        edit_mode.add_start_edit_button(entity, () => {
                            app.newEntity(entity)
                        });
                    } else {
                        // has id -> delete, edit, create RT
                        edit_mode.add_delete_button(entity);
                        edit_mode.add_start_edit_button(entity, () => {
                            app.startEdit(entity)
                        });
                        if (getEntityRole(entity) == "RecordType") {
                            edit_mode.add_new_record_button(entity, () => {
                                edit_mode.create_new_record(getEntityID(entity)).then((entity) => {
                                    app.newEntity(entity);
                                }, edit_mode.handle_error);
                            });
                        }
                    }
                });
            });
        };
        app.onLeaveInitial = function(e) {
            console.log(e);
            app.errorHandler(() => {
                // remove event listeners, add the save button an so on
                $('.caosdb-entity-panel').each(function(index) {
                    edit_mode.unset_entity_dropable(this);
                    edit_mode.remove_start_edit_button(this);
                    edit_mode.remove_new_record_button(this);
                    edit_mode.remove_delete_button(this);
                });
            });
            new_buttons.attr("disabled", true);
        };
        app.onBeforeStartEdit = function(e, entity) {
            console.log(e);
            edit_mode.unhighlight();
            app.old = entity;
            app.entity = $(entity).clone(true)[0];
            edit_mode.smooth_replace(app.old, app.entity);

            edit_mode.add_save_button(app.entity, () => app.update(app.entity));
            edit_mode.add_cancel_button(app.entity, () => app.cancel(app.entity));

            edit_mode.freeze_but(app.entity);
        };
        app.onBeforeCancel = function(e) {
            console.log(e);
            edit_mode.smooth_replace(app.entity, app.old);
            edit_mode.unfreeze();
        };
        app.onUpdate = function(e, entity) {
            console.log(e);
            edit_mode.update_entity(entity).then(response => {
                console.log(response);
                return transformation.transformEntities(response);
            }, edit_mode.handle_error).then(entities => {
                edit_mode.smooth_replace(app.entity, entities[0]);
                app.entity = entities[0];
                app.showResults();
            }, edit_mode.handle_error);
        };
        app.onEnterChanged = function(e) {
            edit_mode.unhighlight();
            hintMessages.removeMessages(app.old);
            edit_mode.make_header_editable(app.entity);
            hintMessages.hintMessages(app.entity);
            $(app.entity).find('.caosdb-annotation-section').remove();
            for (var element of $(app.entity).find('.caosdb-property-row')) {
                edit_mode.make_property_editable(element);
            }
        }
        app.onEnterWait = function(e) {
            console.log(e);
            edit_mode.smooth_replace(app.entity, app.waiting);
        }
        app.onLeaveWait = function(e) {
            console.log(e);
            edit_mode.smooth_replace(app.waiting, app.entity);
        }
        app.onBeforeNewEntity = function(e, entity) {
            console.log(e);
            if (typeof entity == "undefined") {
                throw new TypeError("entity is undefined");
            }

            edit_mode.freeze_but(entity);

            app.entity = entity;
            $(entity).hide();
            if ($('.caosdb-f-main-entities .caosdb-entity-panel').length > 0) {
                $('.caosdb-f-main-entities .caosdb-entity-panel').first().before(entity);
            } else {
                $('.caosdb-f-main-entities').append(entity);
            }
            $(entity).fadeIn();
            edit_mode.init_actions_panels();
            edit_mode.add_save_button(entity, () => app.insert(entity));
            edit_mode.add_cancel_button(entity, () => app.cancel(entity));

            $(window).scrollTop(0);

            app.old = $('<div/>')[0];
        }
        app.onInsert = function(e, entity) {
            console.log(e);
            edit_mode.insert_entity(entity).then(response => {
                console.log(response);
                return transformation.transformEntities(response);
            }, edit_mode.handle_error).then(entities => {
                edit_mode.smooth_replace(app.entity, entities[0]);
                app.entity = entities[0];
                app.showResults();
            }, edit_mode.handle_error);
        }
        app.onFinish = function(e) {
            console.log(e);
            edit_mode.unhighlight();
            if (app.old) {
                edit_mode.smooth_replace(app.entity, app.old);
            }
            edit_mode.unfreeze();
        }
        app.onShowResults = function(e) {
            console.log(e);
            if (!edit_mode.has_errors(app.entity)) {
                app.old = false;
            }
            hintMessages.hintMessages(app.entity);
            edit_mode.unfreeze();
            if (!edit_mode.has_errors(app.entity)) {
                edit_mode.enter_edit_mode(app);
                edit_mode.init_actions_panels();
            }
            resolve_references.init();
            preview.init();
        }
        app.waiting = createWaitingNotification("Please wait.");
        $(app.waiting).hide();
        app.init();
		// TODO: We need some refactoring: there ist the variable editApp which is
		// set with this return value. I think app is a bad name as it contains
		// the state machine. Alos, the state machine should be either passed
		// around or be global (edit_mode.app).
		edit_mode.app = app
        return app;
    }

    this.has_errors = function(entity) {
        return $(entity).find(".alert.alert-danger").length > 0;
    }

    this.freeze_but = function(element) {
        $('.caosdb-f-main-entities').children().each(function(index) {
			if (element != this){
				console.log("Not freezing")
				edit_mode.freeze_entity(this);
			} else {
				console.log("Not freezing")
			}
        });
        //edit_mode.unfreeze_entity(element);
    }

    this.unfreeze = function() {
        $('.caosdb-f-main-entities').children().each(function(index) {
            edit_mode.unfreeze_entity(this);
        });
    }

    // TODO: write generic function format property depending on datatype and the property

    this.retrieve_datatype_list = async function(datatype) {
        var entities = await query("FIND Record " + datatype);
        var files = await query("FIND File " + datatype);

        for (var i = 0; i < entities.length + files.length; i++) {

            if (i < entities.length) {
                var eli = entities[i];
            } else {
                var eli = files[i];
            }
            var prlist = getProperties(eli);
            var prdict = [];
            for (var j = 0; j < prlist.length; j++) {
                prdict.push(prlist[j].name + ": " + prlist[j].value);
            }
            if (prlist.length == 0) {
                prdict.push("ID: " + getEntityID(eli));
            }
            $("select.caosdb-list-" + datatype).not('[data-resolved="true"]').append(
                $("<option value=\"" + getEntityID(eli) + "\">" + prdict.join(", ") + "</option>"));
        }
        $("select.caosdb-list-" + datatype).not('[data-resolved="true"]').attr("data-resolved", "true");
    }

    this.highlight = function(entity) {
        $(entity).addClass("caosdb-v-edit-mode-highlight").css("background-color", "lightgreen");
    }

    this.unhighlight = function() {
        $('.caosdb-v-edit-mode-highlight').removeClass("caosdb-v-edit-mode-highlight").css("background-color", "");
    }

    this.handle_error = function(err) {
        globalError(err);
    }

    this.get_edit_panel = function() {
        return $('.caosdb-f-edit-panel-body')[0];
    }

    this.add_wait_datamodel_info = function() {
        $(this.get_edit_panel()).append(createWaitingNotification("Please wait."));
    }

    this.toggle_edit_panel = function() {
        $(".caosdb-f-main").toggleClass("container-fluid").toggleClass("container");
        $(".caosdb-f-main-entities").toggleClass("col-xs-8");
        $(".caosdb-f-edit").toggleClass("hidden").toggleClass("col-xs-4");
    }

    this.leave_edit_mode_template = function(app) {
        console.log("leave edit mode");
        app.finish();
        $(".caosdb-f-btn-toggle-edit-mode").text("Edit Mode");
        edit_mode.reset_actions_panels();
        window.localStorage.removeItem("edit_mode");
    }

    this.is_edit_mode = function() {
        return window.localStorage.edit_mode !== undefined;
    }

    this.add_cancel_button = function(ent, callback) {
        var cancel_btn = $('<button class="btn btn-link caosdb-update-entity-button caosdb-f-entity-cancel-button">Cancel</button>');

        $(ent).find(".caosdb-f-edit-mode-entity-actions-panel").append(cancel_btn);

        $(cancel_btn).click(callback);
    }

    this.create_new_entity = async function(role) {
        var empty_entity = str2xml('<Response><' + role + '/></Response>');
        return (await transformation.transformEntities(empty_entity))[0];
    }

    this.remove_cancel_button = function(entity) {
        $(entity).find('.caosdb-f-entity-cancel-button').remove()
    }

    /**
     * entity : HTMLElement
     */
    this.freeze_entity = function(entity) {
        $(entity).css("pointer-events", "none").css("filter", "blur(10px)");
    }

    /**
     * entity : HTMLElement
     */
    this.unfreeze_entity = function(entity) {
        $(entity).css("pointer-events", "").css("filter", "");
    }

    this.filter = function(ent_type) {
        var text = $("#caosdb-f-filter-" + ent_type).val();
        if (ent_type == "properties") {
            var short_type = "p";
        } else if (ent_type == "recordtypes") {
            var short_type = "rt";
        } else {
            alert("unkown type");
        }

        var keywords = text.toLowerCase().split(" ");
        var elements = document.getElementsByClassName("caosdb-f-edit-drag");
        for (var el of elements) {
            if (el.id.includes("caosdb-f-edit-" + short_type)) {
                var name = el.innerText.toLowerCase();
                var found_kw = false;
                for (var kw of keywords) {
                    if (name.includes(kw)) {
                        found_kw = true;
                        break;
                    }
                }
                if (found_kw) {
                    $(el).show()
                } else {
                    $(el).hide()
                }
            }
        }
    }


    this.add_start_edit_button = function(entity, callback) {
        edit_mode.remove_start_edit_button(entity);
        var button = $('<button title="Edit this ' + getEntityRole(entity) + '." class="btn btn-link caosdb-update-entity-button caosdb-f-entity-start-edit-button">Edit</button>');

        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        $(button).click(callback);
    }

    this.remove_start_edit_button = function(entity) {
        $(entity).find(".caosdb-f-entity-start-edit-button").remove();
    }


    this.add_new_record_button = function(entity, callback) {
        edit_mode.remove_new_record_button(entity);
        var button = $('<button title="Create a new Record from this RecordType." class="btn btn-link caosdb-update-entity-button caosdb-f-entity-new-record-button">+Record</button>');

        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        $(button).click(callback);
    }

    this.remove_new_record_button = function(entity) {
        $(entity).find(".caosdb-f-entity-new-record-button").remove();
    }

    this.add_delete_button = function(entity, callback) {
        edit_mode.remove_delete_button(entity);
        var button = $('<button title="Delete this ' + getEntityRole(entity) + '." class="btn btn-link caosdb-update-entity-button caosdb-f-entity-delete-button">Delete</button>');

        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        $(button).click(() => {edit_mode.delete_action();});
    }

    this.remove_delete_button = function(entity) {
        $(entity).find(".caosdb-f-entity-delete-button").remove();
    }

}
/**
 * Add the extensions to the webui.
 */
$(document).ready(function() {
    edit_mode.init();
});
