/**
 * annotation module contains all code for retrieving and posting annotations on
 * entities.
 */
this.annotation = new function() {
    /**
     * Create a form for a new CommentAnnotation.
     * 
     * @param entityId,
     *            integer, the entity which is to be annotated.
     * @return HTML form element.
     */
    this.createNewCommentForm = function(entityId) {
        var form = $('<form class="caosdb-new-comment-form">' +
            '<input type="hidden" name="annotationOf" value="' + entityId + '"> ' +
            '<div class="form-group">' +
            '<label for="comment">Your new comment:</label>' +
            '<textarea class="form-control" rows="5" name="newComment" title="Your comment with 5 or more characters." pattern=".{5,}"></textarea>' +
            '</div>' +
            '<button class="btn btn-default" title="Submit this comment." type="submit" name="submit" value="Submit">Submit</button>' +
            '<button class="btn btn-default" title="Cancel this comment." type="reset" name="cancel" value="Cancel">Cancel</button>' +
            '</form>');
        return form[0];
    }

    /**
     * Returns the NewCommentButton for a given AnnotationSection.
     * 
     * @param annotationSection,
     *            HTML element
     * @return HTML button element
     */
    this.getNewCommentButton = function(annotationSection) {
        return $(annotationSection).find("button.caosdb-new-comment-button")[0]
    }

    /**
     * Returns the NewCommentForm for a given AnnotationSection.
     * 
     * @param annotationSection,
     *            HTML element
     * @return HTML form element
     */
    this.getNewCommentForm = function(annotationSection) {
        return $(annotationSection).find("form.caosdb-new-comment-form")[0];
    }

    /**
     * Returns the entityId for a given AnnotationSection. I.e. the ID of the
     * entity which the annotationSection is attached to.
     * 
     * @param annotationSection,
     *            HTML element
     * @return String
     */
    this.getEntityId = function(annotationSection) {
        return $(annotationSection).attr("data-entity-id");
    }

    /**
     * Returns cancel button for a new comment. That is the reset button of the
     * NewCommentForm for a given AnnotationSection.
     * 
     * @param annotationSection,
     *            HTML element
     * @return HTML button element
     */
    this.getCancelNewCommentButton = function(annotationSection) {
        return $(annotationSection).find("form.caosdb-new-comment-form button[type='reset']")[0];
    }

    /**
     * Returns submit button of the NewCommentForm for a given
     * AnnotationSection.
     * 
     * @param annotationSection,
     *            HTML element
     * @return HTML button element
     */
    this.getSubmitNewCommentButton = function(annotationSection) {
        return $(annotationSection).find("form.caosdb-new-comment-form button[type='submit']")[0];
    }

    /**
     * Returns the PleaseWaitNotification for a given AnnotationSection.
     * 
     * @param annotationSection,
     *            HTML element
     * @return HTML element
     */
    this.getPleaseWaitNotification = function(annotationSection) {
        return $(annotationSection).find('.caosdb-please-wait-notification')[0];
    }

    this.createError = function(error) {
        var ret = $('<div class="alert alert-danger caosdb-new-comment-error alert-dismissable">' +
            '<button class="close" data-dismiss="alert" aria-label="close">×</button>' +
            '<strong>' + error.name + '!</strong> ' + error.message + '<p class="small"><pre><code>' +
            (error.stack ? error.stack : "") + '</code></pre></p></div>')[0];
        return ret;
    }

    /**
     * Initialize the process of writing a new comment, posting it to the server
     * and retrieving the answer of the server. This process is tied to a
     * particular 'annotationSection' which in turn belongs to a particular
     * entity which is currently displayed. Every step of this process is
     * reflected in the annotationSection (show a form with a textarea and
     * submit/cancel buttons, show a waiting note, show any results or errors if
     * they occur).
     * 
     * Returns a StateMachine which monitors/controls the process and all events
     * (button clicks, termination of asynchronous functions).
     * 
     * @param annotationSection,
     *            HTML element
     * @return StateMachine object
     */
    this.initNewCommentApp = function(annotationSection) {
        if (annotationSection == null) {
            throw new Error("annotationSection was null");
        }
        var minimalLengthOfComments = 1; // change it here if desired!
        var formWrapper = $('<li class="list-group-item"></li>')[0];
        var newCommentButton = annotation.getNewCommentButton(annotationSection);

        // handles all the logic via the app.on<...> functions!!!
        var app = new StateMachine({
            transitions: [{
                name: "init",
                from: 'none',
                to: "read"
            }, {
                name: "openForm",
                from: 'read',
                to: 'write'
            }, {
                name: "submitForm",
                from: 'write',
                to: 'send'
            }, {
                name: "cancelForm",
                from: 'write',
                to: 'read'
            }, {
                name: "receiveResponse",
                from: 'send',
                to: 'read'
            }, {
                name: "resetApp",
                from: '*',
                to: 'read'
            }, ],
        });
        app.errorHandler = function(fn) {
            try {
                fn();
            } catch (e) {
                annotationSection.insertBefore(annotation.createError(e), annotationSection.children[2]);
                setTimeout(() => app.resetApp(), 1000);
            }
        }
        app.onEnterRead = function(e) {
            app.errorHandler(function(e) {
                formWrapper.innerHTML = "";
                if (formWrapper.parentNode === annotationSection) {
                    annotationSection.removeChild(formWrapper);
                }
                // (re-)enable newCommentButton
                $(newCommentButton).toggleClass("disabled", false);
                newCommentButton.onclick = function(event) {
                    app.openForm();
                };
            });
        };
        app.onLeaveRead = function(e) {
            app.errorHandler(function(e) {
                // disable newCommentButton
                $(newCommentButton).toggleClass("disabled", true);
                newCommentButton.onclick = null;
            });
        };
        app.onEnterWrite = function(e) {
            app.errorHandler(function(e) {
                // add the newCommentForm
                var entityId = annotation.getEntityId(annotationSection);
                var form = annotation.createNewCommentForm(entityId);
                form.onsubmit = function(e) {
                    app.submitForm(form);
                    // return false to interrupt the standard form submission (which
                    // is synchronous)
                    return false;
                };
                form.onreset = function(e) {
                    app.cancelForm(form);
                };
                formWrapper.appendChild(form);
                annotationSection.appendChild(formWrapper);
            });
        }
        app.onBeforeSubmitForm = function(e, form) {
            // validate form
            return annotation.validateNewCommentForm(form, minimalLengthOfComments);
        }
        app.onEnterSend = function(e, form) {
            var pleaseWait = annotation.createPleaseWaitNotification();
            // remove the newCommentForm
            formWrapper.removeChild(formWrapper.children[0]);
            // add waiting notification
            formWrapper.appendChild(pleaseWait);

            // convert and send form
            var xml = annotation.convertNewCommentForm(form);
            var xslPromise = annotation.loadAnnotationXsl(window.sessionStorage.caosdbBasePath);
            var responsePromise = annotation.postCommentXml(xml);
            var commentPromise = annotation.convertNewCommentResponse(responsePromise, xslPromise);
            commentPromise.then(function(resolve) {
                app.receiveResponse(resolve);
            });
        }
        app.onBeforeReceiveResponse = function(e, response) {
            // remove waiting notification and append response
            annotationSection.removeChild(formWrapper);
            annotationSection.appendChild(response[0]);
        }


        // start with read state
        app.init();
        return app;
    }

    /**
     * Create a notification to ask the user to wait while the post request is
     * pending.
     * 
     * @return HTML element;
     */
    this.createPleaseWaitNotification = function() {
        return $('<div class="caosdb-please-wait-notification">Please wait while your comment is being submitted.</div>')[0];
    }

    /**
     * Check if the NewCommentForm has non-empty fields 'annotationOf' and
     * 'newComment'. An optional parameter can specify the minimal length in
     * characters for the newComment field. Default is 1.
     * 
     * @param form,
     *            a HTML form element.
     * @param commentLength,
     *            integer > 0 (default 1)
     * @return boolean
     */
    this.validateNewCommentForm = function(form, min = 1) {
        var fieldsThere = form.annotationOf !== null && form.newComment !== null;
        var fieldsNotEmpty = form.annotationOf.value.length > 0 && form.newComment.value.length >= min;
        return fieldsThere && fieldsNotEmpty;
    }

    /**
     * Convert a NewCommentForm into an XML document for posting.
     * 
     * @param form,
     *            HTML form element
     * @return XML document
     */
    this.convertNewCommentForm = function(form) {
        var xml_str = "<Insert>";
        xml_str += "<Record>";
        xml_str += '<Parent name="CommentAnnotation"/>';
        xml_str += '<Property name="comment">'
        xml_str += form.elements["newComment"].value;
        xml_str += '</Property>';
        xml_str += '<Property name="annotationOf">';
        xml_str += form.elements["annotationOf"].value;
        xml_str += '</Property>';
        xml_str += "</Record>";
        xml_str += "</Insert>";
        return str2xml(xml_str);
    };

    /**
     * Shortcut for postXml. The renaming is also good to be able to replace the
     * function during unit tests with a dummy postXml function
     */
    this.postCommentXml = (xml) => postXml(xml, window.sessionStorage.caosdbBasePath, "?H");

    /**
     * Convert a the response of a POST request for a new CommentAnnotation to
     * HTML via XSLT.
     * 
     * @param response,
     *            an xml document
     * @param xslPromise,
     *            an xsl document or a promise for one.
     * @return an HTML element.
     */
    this.convertNewCommentResponse = async function(response, xslPromise) {
        var ret = (await asyncXslt(response, xslPromise)).firstChild.children;
        $(ret).find("p.small>pre>code").each(function() {
            var text = $(this).html();
            $(this).html('');
            $(this).text(text);
        });
        return markdown.toHtml(ret);
    }

    /**
     * Initialize the annotation sections for each entity. To be called once
     * after the document is ready.
     */
    this.init = function() {
        // call initSingleAnnotationSection for each annotationSection in the
        // document
        $('.caosdb-annotation-section').each(
            function(index, element) {
                annotation.initAnnotationSection(element);
            });
    }

    /**
     * Retrieves all annotations for an entity from the database via the
     * 'requestDatabase' function and transform the response into an array of
     * DOMElement via the xslPromise's result.
     * 
     * @param entityId,
     *            an id of an entity.
     * @param requestDatabase,
     *            a function with one parameter
     * @param xslPromise,
     *            a Promise which resolves to an xsl script.
     * @return an array of DOMElements.
     */
    this.getAnnotationsForEntity = async function(entityId, requestDatabase, xslPromise) {
        return markdown.toHtml((await asyncXslt(requestDatabase(entityId), xslPromise)).firstChild.children);
    }

    this.queryAnnotation = function(referencedId) {
        return $.ajax({
            cache: true,
            dataType: 'xml',
            url: window.sessionStorage.caosdbBasePath + "Entity/?H&query=FIND+Annotation+WHICH+REFERENCES+" + referencedId + "+WITH+ID=" + referencedId,
        });
    }

    this.loadAnnotationXsl = function(basepath) {
        return $.ajax({
            cache: true,
            dataType: 'xml',
            url: basepath + "webinterface/xsl/annotation.xsl",
        });
    }

    this.loadComments = async function(annotationSection) {
        var entityId = annotation.getEntityId(annotationSection);
        var annotations = await annotation.getAnnotationsForEntity(entityId, annotation.queryAnnotation, annotation.loadAnnotationXsl(window.sessionStorage.caosdbBasePath));
        $(annotationSection).append(annotations);
    }

    /**
     * Initialize the annotation section of the given element.
     * 
     * @param element,
     *            the element which represents a single entity's annotation
     *            section
     * @return undefined
     */
    this.initAnnotationSection = function(element) {
        if (element == null) {
            throw new Error("element was null");
        } else if (typeof element != "object") {
            throw new Error("element was not an object.");
        }

        annotation.loadComments(element);

        // TODO: Move this to a separate component.
        // This can fail, if caosdb.js is not loaded yet:
        // if(!userIsAnonymous()) {
        //     annotation.initNewCommentApp(element);
        // }
        // QnD Solution:
        if (!Array.from(
            document.getElementsByClassName("caosdb-user-role")).map(
                el => el.innerText
            ).filter(el => el == "anonymous").length > 0) {
            annotation.initNewCommentApp(element);
        }
    }
};

$(document).ready(annotation.init);
