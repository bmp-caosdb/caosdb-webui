// ** header v3.0
// This file is a part of the CaosDB Project.

// Copyright (C) 2019 IndiScale GmbH
// Max-Planck-Institute for Dynamics and Self-Organization Göttingen

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

// ** end header

// Templates Extension for CaosDB WebUI
// A. Schlemmer, 11/2018
// (c) 2019 D. Hornung <d.hornung@indiscale.com>

/**
 * Loads, generates and expands templates.
 *
 * Templates are stored in conf/ext/templates.json, the format is as follows:
 *
 * [
 *   {
 *     "help": "Find all geological data sets with temperature above {temp} K.",
 *     "query": "FIND Record Geodata with temperature > \"$1\""
 *   }, {
 *     ...
 *   }
 * ]
 */
var templates_ext = new function() {

    this.init = function() {
        this.add_templates();
        this.add_user_templates();
    }

    this.generate_template = function(tempstring, tempquery) {
        // Syntax:
        // tempstring: Suche alle Kisten mit Name {text}
        // tempquery: FIND Record Kiste with KistenName = "$1"
        var re = /\{(.*?)\}/g;
        var preparedstr = tempstring.replace(re, ' <input type="text"> </input>');
        var template1 = $('<div class="row"><div class="col-md-10"><div class="btn invisible" style="padding-left: 0px;">.</div>' + preparedstr + '</div><div class="col-md-2 text-right"><button class="btn btn-default caosdb-button-ok"> <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></button><button class="btn btn-default caosdb-button-search"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span> </button></div></div>');
        var clickfun = (_) => {
            var res = template1.find("input");
            var req = /\$([0-9]+)/g;
            $("#caosdb-query-textarea").val(tempquery.replace(req, (match, p1) => res[p1 - 1].value));
        };
        var clickfun2 = (_) => {
            clickfun();
            $("#caosdb-query-form").submit();
        };
        template1.find(".caosdb-button-ok").click(clickfun);
        template1.find(".caosdb-button-search").click(clickfun2);
        return template1;
    }

    this.add_templates = function() {
        var shortcuts_container = $('<div class="container caosdb-shortcuts-container"><span class="h3">Shortcuts</span></div>');
        $("#caosdb-query-panel").append(shortcuts_container);

        load_config("ext/templates.json").then(temparray => {

            for (var i = 0; i < temparray.length; i++) {
                var tempel = temparray[i];
                shortcuts_container.append(
                    this.generate_template(tempel.help, tempel.query));
            }
        }, e => {
			alert("Error in ext/templates.json!");
		});

        
    }

    this.add_user_templates = async function() {
        var temparray = await query("FIND Record UserTemplate");

        for (var i = 0; i < temparray.length; i++) {
            $(".caosdb-shortcuts-container").append(
                this.generate_template(getProperty(temparray[i], "templateDescription", case_sensitive=false),
                                       getProperty(temparray[i], "Query", case_sensitive=false)));
        }
    }

    
}



$(document).ready(function() {
    templates_ext.init();
});
