/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
'use strict';

window.addEventListener('error', (e) => globalError(e.error));

var globalError = function(error) {
    console.log(error);
    var stack = error.stack;
    var message = "Error! Please help to make CaosDB better! Copy this message and send it via email to info@indiscale.com.\n\n";
    message += error.toString();

    if (stack) {
        message += '\n' + stack;
    }

    window.alert(message);
}

var globalClassNames = new function() {
    this.NotificationArea = "caosdb-preview-notification-area";
    this.WaitingNotification = "caosdb-preview-waiting-notification";
    this.ErrorNotification = "caosdb-preview-error-notification";
}

/**
 * connection module contains all ajax calls.
 */
this.connection = new function() {
    this._init = function() {
        /**
         * Send a get request.
         */
        this.get = async function _get(uri) {
            try {
                return await $.ajax({
                    url: window.sessionStorage.caosdbBasePath + uri,
                    dataType: "xml",
                });
            } catch (error) {
                if (error.status == 414) {
                    throw new Error("UriTooLongException for GET " + uri);
                } else if (error.status != null) {
                    throw new Error("GET " + uri + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Send a put (i.e. update) request.
         */
        this.put = async function _put(uri, data) {
            try {
                return await $.ajax({
                    url: window.sessionStorage.caosdbBasePath + uri,
                    method: 'PUT',
                    dataType: "xml",
                    processData: false,
                    data: data,
                    contentType: 'text/xml',
                });
            } catch (error) {
                if (error.status != null) {
                    throw new Error("PUT " + uri + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Run a script in the backend.
         * @scriptname is the name of the scriptfile without leading scriptpath, e.g. test.py.
         * @params is a associative array with keys being the options and values being the values.
         */
        this.runScript = async function _runScript(scriptname, params) {
            var pstring = "call=" + scriptname;
            for (var key in params) {
                pstring += "&-O" + key + "=" + params[key];
            }
            try {
                return await $.ajax({
                    url: window.sessionStorage.caosdbBasePath + "scripting",
                    method: 'POST',
                    dataType: "xml",
                    processData: false,
                    data: pstring,
                    contentType: 'application/x-www-form-urlencoded',
                });
            } catch (error) {
                if (error.status != null) {
                    throw new Error(
                        "POST scripting returned with HTTP status " + error.status
                            + " - " + error.statusText + "\n"
                            + pstring);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Send a post (i.e. insert) request.
         */
        this.post = async function _post(uri, data) {
            try {
                return await $.ajax({
                    url: window.sessionStorage.caosdbBasePath + uri,
                    method: 'POST',
                    dataType: "xml",
                    processData: false,
                    data: data,
                    contentType: 'text/xml',
                });
            } catch (error) {
                if (error.status != null) {
                    throw new Error("POST " + uri + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Send a delete request.
         * idline: 124&256
         */
        this.deleteEntities = async function _deleteEntities(idline) {
            try {
                return await $.ajax({
                    url: window.sessionStorage.caosdbBasePath + "Entity/" + idline,
                    method: 'DELETE',
                    dataType: "xml",
                    processData: false,
                    data: "",
                    contentType: 'text/xml',
                });
            } catch (error) {
                if (error.status != null) {
                    throw new Error("DELETE " + "Entity/" + idline + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Return the base path of the server.
         */
        this.getBasePath = function() {
            let a = document.createElement('a');
            a.href = window.sessionStorage.caosdbBasePath;
            return a.href;
        }
    }
    this._init();
}

/**
 * transformation module contains all code for tranforming xml into html via
 * xslt.
 */
this.transformation = new function() {
    /**
     * remove all permission information from a server's response.
     *
     * @param {XMLDocument} xml
     * @return {XMLDocument} without <Permissions> tags.
     */
    this.removePermissions = function(xml) {
        $(xml).find('Permissions').remove();
        return xml
    }

    /**
     * Retrieve a certain xslt script by name.
     *
     * @param {String} name
     * @return {XMLDocument} xslt document.
     */
    this.retrieveXsltScript = async function _rXS(name) {
        return await connection.get("webinterface/xsl/" + name);
    }

    /**
     * Transform the server's response with multiple entities into their
     * html representation. The parameter `xml` may also be a Promise.
     *
     * @param {XMLDocument} xml
     * @return {HTMLElement[]} an array of HTMLElements.
     */
    this.transformEntities = async function _tME(xml) {
        let xsl = await this.retrieveEntityXsl();
        let html = await asyncXslt(xml, xsl);
        return $(html).find('div.root > div.caosdb-entity-panel').toArray();
    }

    /**
     * @param {XMLDocument} xml
     * @return {HTMLElement[]} an array of HTMLElements.
     */
    this.transformParent = async function _tME(xml) {
        var xsl = await transformation.retrieveXsltScript("parent.xsl");
        insertParam(xsl, "entitypath", window.sessionStorage.caosdbBasePath + "Entity/");
        // TODO the following line should not have any effect. nevertheless: it
        // does not work without
        xsl = str2xml(xml2str(xsl));
        let html = await asyncXslt(xml, xsl);
        return html;
    }

    /**
     * @param {XMLDocument} xml
     * @return {HTMLElement[]} an array of HTMLElements.
     */
    this.transformProperty = async function _tME(xml) {
        var xsl = await transformation.retrieveXsltScript("property.xsl");
        insertParam(xsl, "filesystempath", window.sessionStorage.caosdbBasePath + "FileSystem/");
        insertParam(xsl, "entitypath", window.sessionStorage.caosdbBasePath + "Entity/");
        insertParam(xsl, "close-char", '×');
        var entityXsl = await transformation.retrieveXsltScript('entity.xsl');
        var messageXsl = await transformation.retrieveXsltScript('messages.xsl');
        var commonXsl = await transformation.retrieveXsltScript('common.xsl');
        var xslt = transformation.mergeXsltScripts(xsl, [messageXsl, commonXsl, entityXsl]);
        let html = await asyncXslt(xml, xslt);
        return html;
    }
    /**
     * @param {XMLDocument} xml
     * @return {HTMLElement[]} an array of HTMLElements.
     */
    this.transformEntityPalette = async function _tME(xml) {
        var xsl = await transformation.retrieveXsltScript("entity_palette.xsl");
        let html = await asyncXslt(xml, xsl);
        return html;
    }

    /**
     * Retrieve the entity.xsl script and modify it such that we can use it
     * without the context of the main.xsl.
     *
     * @return {XMLDocument} xslt script
     */
    this.retrieveEntityXsl = async function _rEX() {
        var entityXsl = await transformation.retrieveXsltScript("entity.xsl");
        var commonXsl = await transformation.retrieveXsltScript("common.xsl");
        var errorXsl = await transformation.retrieveXsltScript('messages.xsl');
        var xslt = transformation.mergeXsltScripts(entityXsl, [errorXsl, commonXsl]);
        insertParam(xslt, "filesystempath", window.sessionStorage.caosdbBasePath + "FileSystem/");
        insertParam(xslt, "entitypath", window.sessionStorage.caosdbBasePath + "Entity/");
        insertParam(xslt, "close-char", '×');
        xslt = injectTemplate(xslt, '<xsl:template match="/"><div class="root"><xsl:apply-templates select="Response/*" mode="entities"/></div></xsl:template>');
        return xslt;
    }

    /**
     * Merges several xsl style sheets into one. All template rules are
     * appended to the main style sheet.
     *
     * @param {XMLDocument} xslMain, the main style sheet
     * @param {XMLDocument[]} xslIncludes, array of style sheets which are to
     *          be included.
     * @return {XMLDocument} a new style sheets with all template rules;
     */
    this.mergeXsltScripts = function(xslMain, xslIncludes) {
        let ret = getXSLScriptClone(xslMain);
        for (var i = 0; i < xslIncludes.length; i++) {
            $(xslIncludes[i].firstElementChild).find('xsl\\:template').each(function(index) {
                $(ret.firstElementChild).append(this);
            });
        }
        return ret;
    }
}

/**
 * transaction module contains all code for insertion, update and deletion of 
 * entities. Currently, only updates are implemented.
 */
this.transaction = new function() {
    this.classNameUpdateForm = "caosdb-update-entity-form";

    /**
     * Retrieve a single entity and return its XML representation.
     *
     * @param {String} entityId
     * @return {Element} an xml element.
     */
    this.retrieveEntityById = async function _rEBI(entityId) {
        let entities = await this.retrieveEntitiesById([entityId]);
        return entities[0];
    }

    /**
     * Retrieve multiple entities and return their XML representation.
     *
     * @param {String[]} entityIds, array of IDs.
     * @return {Element[]} array of xml elements.
     */
    this.retrieveEntitiesById = async function _rEBIs(entityIds) {
        return $(await connection.get(this.generateEntitiesUri(entityIds))).find('Response [id]').toArray();
    }

    /** Sends a PUT request with an xml representation of entities and
     * returns the xml document with the server response.
     * 
     * @param {XMLDocument} xml, the entity elements surrounded by an
     *          <Update> tag.
     * @return {XMLDocument} the server response.
     */
    this.updateEntitiesXml = async function _uE(xml) {
        return await connection.put("Entity/", xml);
    }

    /** Sends a POST request with an xml representation of entities and
     * returns the xml document with the server response.
     * 
     * @param {XMLDocument} xml, the entity elements surrounded by an
     *          <Insert> tag.
     * @return {XMLDocument} the server response.
     */
    this.insertEntitiesXml = async function _iE(xml) {
        return await connection.post("Entity/", xml);
    }

    this.deleteEntities = async function _dE(idlist) {
        return await connection.deleteEntities(idlist.join("&"));
    }

    /**
     * Generate the URI for the retrieval of a list of entities.
     * 
     * @param {String[]} entityIds - An array of entity ids..
     * @return {String} The uri.
     */
    this.generateEntitiesUri = function(entityIds) {
        return "Entity/" + entityIds.join("&");
    }

    /**
     * Submodule for update transactions.
     */
    this.update = new function() {
        /**
         * Create a form for updating entities. It has only a textarea and a
         * submit button.
         *
         * The 'entityXmlStr' contains the entity in it's current (soon-to-be
         * old) version which can then be modified by the user.
         *
         * The 'putCallback' is a function which accepts one parameter, a
         * string representation of an xml document.
         *
         * @param {String} entityXmlStr, the old entity
         * @param {function} putCallback, the function which sends a put request.
         */
        this.createUpdateForm = function(entityXmlStr, putCallback) {
            // check the parameters
            if (putCallback == null) {
                throw new Error("putCallback function must not be null.");
            }
            if (typeof putCallback !== "function" || putCallback.length !== 1) {
                throw new Error("putCallback is to be a function with one parameter.");
            }
            if (entityXmlStr == null) {
                throw new Error("entityXmlStr must not be null");
            }

            // create the form element by element
            let textarea = $('<div class="form-group"><textarea rows="8" style="width: 100%;" name="updateXml"/></div>');
            textarea.find('textarea').val(entityXmlStr);
            let submitButton = $('<button class="btn btn-default" type="submit">Update</button>');
            let resetButton = $('<button class="btn btn-default" type="reset">Reset</button>');
            let form = $('<form class="panel-body"></form>');
            form.toggleClass(transaction.classNameUpdateForm, true);
            form.append(textarea);
            form.append(submitButton);
            form.append(resetButton);

            // reset restores the original xmlStr
            form.on('reset', function(e) {
                textarea.find('textarea').val(entityXmlStr);
                return false;
            });

            // submit calls the putCallback
            form.submit(function(e) {
                putCallback(e.target.updateXml.value);
                return false;
            });

            return form[0];
        }

        /**
         * Start the update of a single entity. Returns a state machine which 
         * 1) removes entity from page. show waiting notification instead. 
         * 2) retrieves the old version's xml
         * 3) shows update form with old xml
         * 4) submits new xml and handles success and failure of the update. 
         *
         * @param {HTMLElement} entity, the div which represent the entity.
         * @return {Object} a state machine.
         */
        this.updateSingleEntity = function(entity) {
            let updatePanel = transaction.update.createUpdateEntityPanel(transaction.update.createUpdateEntityHeading($(entity).find('.caosdb-entity-panel-heading')[0]));
            var app = new StateMachine({
                transitions: [{
                    name: "init",
                    from: 'none',
                    to: "waitRetrieveOld"
                }, {
                    name: "openForm",
                    from: ['waitRetrieveOld', 'waitPutEntity'],
                    to: 'modifyEntity'
                }, {
                    name: "submitForm",
                    from: 'modifyEntity',
                    to: 'waitPutEntity'
                }, {
                    name: "showUpdatedEntity",
                    from: 'waitPutEntity',
                    to: 'final'
                }, {
                    name: "resetApp",
                    from: '*',
                    to: 'final'
                }, ],
            });
            app.errorHandler = function(fn) {
                try {
                    fn();
                } catch (e) {
                    setTimeout(() => app.resetApp(e), 1000);
                }
            }
            app.onInit = function(e, entity) {
                // remove entity
                $(entity).hide();
                app.errorHandler(() => {
                    // show updatePanel instead
                    $(updatePanel).insertBefore(entity);
                    // create and add waiting notification
                    updatePanel.appendChild(transaction.update.createWaitRetrieveNotification());
                    let entityId = getEntityId(entity);
                    transaction.update.retrieveOldEntityXmlString(entityId).then(xmlstr => {
                        app.openForm(xmlstr);
                    }, err => {
                        app.resetApp(err);
                    });
                });
                // retrieve old xml, trigger state change when response is ready
            };
            app.onOpenForm = function(e, entityXmlStr) {
                app.errorHandler(() => {
                    // create and show Form
                    let form = transaction.update.createUpdateForm(entityXmlStr, (xmlstr) => {
                        app.submitForm(xmlstr);
                    });
                    updatePanel.append(form);
                });
            };
            app.onResetApp = function(e, error) {
                $(entity).show();
                $(updatePanel).remove();
                if (error != null) {
                    globalError(error);
                }
            };
            app.onShowUpdatedEntity = function(e, newentity) {
                // remove updatePanel
                updatePanel.remove();
                // show new version of entity
                $(newentity).insertBefore(entity);
                // remove old version
                $(entity).remove();
            };
            app.onSubmitForm = function(e, xmlstr) {
                // remove form
                $(updatePanel).find('form').remove();

                $(updatePanel).find('.' + globalClassNames.ErrorNotification).remove();

                // send HTTP PUT to update entity
                app.errorHandler(() => {
                    transaction.updateEntitiesXml(str2xml('<Update>' + xmlstr + '</Update>')).then(
                        xml => {
                            if ($(xml.firstElementChild).find('Error').length) {
                                // if there is an <Error> tag in the response, show
                                // the response in a new form.
                                app.openForm(xml2str(xml));
                                transaction.update.addErrorNotification($(updatePanel).find('.panel-heading'), transaction.update.createErrorInUpdatedEntityNotification());
                            } else {
                                // if there are no errors show the XSL-transformed
                                // updated entity.
                                transformation.transformEntities(xml).then(entities => {
                                    app.showUpdatedEntity(entities[0]);
                                }, err => {
                                    // errors from the XSL transformation
                                    app.resetApp(err);
                                });
                            }
                        }, err => {
                            // errors from the HTTP PUT request
                            app.resetApp(err);
                        }
                    );
                });
            };
            app.onLeaveWaitPutEntity = function() {
                // remove waiting notifications
                removeAllWaitingNotifications(updatePanel);
            };
            app.onLeaveWaitRetrieveOld = app.onLeaveWaitPutEntity;

            app.init(entity);
            app.updatePanel = updatePanel;

            let closeButton = transaction.update.createCloseButton('.panel', () => {
                app.resetApp();
            });
            $(updatePanel).find('.panel-heading').prepend(closeButton);
            return app;
        }

        /**
         * Retrieves an entity and returns it's untransformed xml representation.
         *
         * @param {String} entityId, an entity id.
         * @return {String} the xml of the entity.
         */
        this.retrieveOldEntityXmlString = async function _retrieveOEXS(entityId) {
            let xml = await transaction.retrieveEntityById(entityId);;
            return xml2str(transformation.removePermissions(xml));
        }

        this.createWaitRetrieveNotification = function() {
            return createWaitingNotification("Retrieving xml and loading form. Please wait.");
        }

        this.createWaitUpdateNotification = function() {
            return createWaitingNotification("Sending update to the server. Please wait.");
        }

        this.createErrorInUpdatedEntityNotification = function() {
            return createErrorNotification("The update was not successful.");
        }

        this.addErrorNotification = function(elem, err) {
            $(elem).append(err);
            return elem;
        }

        /**
         * Create a panel where the waiting notifications and the update form
         * is to be shown. This panel will be inserted before the old entity.
         *
         * @param {HTMLElement} heading, the heading of the panel.
         * @return {HTMLElement} A div.
         */
        this.createUpdateEntityPanel = function(heading) {
            let panel = $('<div class="panel panel-default" style="border-color: blue;"/>');
            panel.append(heading);
            return panel[0];
        };

        /**
         * Create a heading for the update panel from the heading of an entity.
         * Basically, this copies the heading, removes everything but the first
         * element, and adds a span that indicates that one is currently
         * updating something.
         *
         * @param {HTMLElement} entityHeading, the heading of the entity.
         * @return {HTMLElement} the heading for the update panel.
         */
        this.createUpdateEntityHeading = function(entityHeading) {
            let heading = entityHeading.cloneNode(true);
            let update = $('<span><h3>Update</h3></span>')[0];
            $(heading).children().slice(1).remove();
            $(heading).prepend(update);
            $(heading).find('.caosdb-backref-link').remove();
            return heading;
        }

        /**
         * Get the heading from an entity panel.
         *
         * @param {HTMLElement} entityPanel, the entity panel.
         * @return {HTMLElement} the heading.
         */
        this.getEntityHeading = function(entityPanel) {
            return $(entityPanel).find('.caosdb-entity-panel-heading')[0];
        }

        this.initUpdate = function(button) {
            transaction.update.updateSingleEntity(
                $(button).closest('.caosdb-entity-panel')[0]
            );
        }

        this.createCloseButton = function(close, callback) {
            let button = $('<button title="Cancel update" class="btn btn-link close" aria-label="Cancel update">&times;</button>');
            button.bind('click', function() {
                $(this).closest(close).hide();
                callback();
            });
            return button[0];
        }
    }
}


var paging = new function() {

    this.defaultPageLen = 10;
    /**
     * It reads the current page from the current uri's query segment (e.g.
     * '?P=0L10') and sets the correct links of the PREV/NEXT buttons or
     * hides them.
     * 
     * This is called in main.xsl
     * 
     * @param totalEntities,
     *            the total number of entities in a response (which is >=
     *            the number of entities which are currently shown on this
     *            page.
     */
    this.initPaging = function(href, totalEntities) {

        if (totalEntities == null) {
            return false;
        }

        // get links for the next/prev page
        var oldPage = paging.getPSegmentFromUri(href);
        if (oldPage == null) {
            // no paging is to be done -> this is a success after all.
            return true;
        }

        var nextHref = paging.getPageHref(href, paging.getNextPage(oldPage, totalEntities));
        var prevHref = paging.getPageHref(href, paging.getPrevPage(oldPage));

        if (nextHref != null) {
            // set href and show next button
            $('.caosdb-next-button').attr("href", nextHref);
            $('.caosdb-next-button').show();
            $('.caosdb-paging-panel').show();
        } else {
            $('.caosdb-next-button').hide();
        }
        if (prevHref != null) {
            // set href and show prev button
            $('.caosdb-prev-button').attr("href", prevHref);
            $('.caosdb-prev-button').show();
            $('.caosdb-paging-panel').show();
        } else {
            if (prevHref == nextHref) {
                $('.caosdb-paging-panel').hide();
            }
            $('.caosdb-prev-button').hide();
        }

        return true;

    }

    /**
     * Replace the old page string in the given uri or concat it if there was no
     * page string. If page is null return null.
     * 
     * @param uri_old,
     *            the old uri
     * @param page,
     *            the page the new uri shall point to
     * @return a string uri which points to the page denotes by the parameter
     */
    this.getPageHref = function(uri_old, page) {
        if (uri_old == null) {
            throw new Error("uri was null.");
        }
        if (page == null) {
            return null;
        }

        var pattern = /(\?(.*&)?)P=([^&]*)/;
        if (pattern.test(uri_old)) {
            // replace existing P=...
            return uri_old.replace(pattern, "$1P=" + page);
        } else if (/\?/.test(uri_old)) {
            // append P=... to existing query segment
            return uri_old + "&P=" + page;
        } else {
            // no query segment so far
            return uri_old + "?P=" + page;
        }
    }

    /**
     * This function takes an URI and returns the first ocurrence of a paging pattern.
     * If the pattern is not found, null will be returned.
     *
     * @param uri An arbitrary URI, usually the current URI of the window.
     * @return The first part of the query segment which begins with 'P='
     */
    this.getPSegmentFromUri = function(uri) {
        if (uri == null) {
            throw new Error("uri was null.");
        }
        var pattern = /\?(.*&)?P=([^&]*)/;
        var ret = pattern.exec(uri);
        return (ret != null ? ret[2] : null);
    }

    /**
     * Construct a page string for the previous page. Returns null, if there is
     * no previous page. Throws exceptions if P is null or has a wrong format.
     * 
     * @param P,
     *            a paging string
     * @return a String
     */
    this.getPrevPage = function(P) {
        if (P == null) {
            throw new Error("P was null");
        }

        var pattern = /^([0-9]+)L([0-9]+)$/
        var match = pattern.exec(P);
        if (match == null) {
            throw new Error("P didn't match " + pattern.toString());
        }

        var index_old = parseInt(match[1]);
        if (index_old == 0) {
            // no need for a prev button
            return null;
        }

        var length = parseInt(match[2]);
        var index = Math.max(index_old - length, 0);

        return index + "L" + length;
    }

    /**
     * Construct a page string for the next page. Returns null if there is no
     * next page. Throws exceptions if n or P is null or if n isn't an integer >=
     * 0 or if P doesn't have the correct format.
     * 
     * @param P,
     *            a paging string
     * @param n,
     *            total numbers of entities.
     * @return a String
     */
    this.getNextPage = function(P, n) {
        // check n and P for null values and correct formatting
        if (n == null) {
            throw new Error("n was null");
        }
        if (P == null) {
            throw new Error("P was null");
        }
        n = parseInt(n);
        if (isNaN(n) || !(typeof n === 'number' && (n % 1) === 0 && n >= 0)) {
            throw new Error("n is to be an integer >= 0!");
        }
        var pattern = /^([0-9]+)L([0-9]+)$/
        var match = pattern.exec(P);
        if (match == null) {
            throw new Error("P didn't match " + pattern.toString());
        }

        var index_old = parseInt(match[1]);
        var length = parseInt(match[2]);
        var index = index_old + length;

        if (index >= n) {
            // no need for a next button
            return null;
        }
        return index + "L" + length;
    }
};

var queryForm = new function() {
    this.init = function(form) {
        this.restoreLastQuery(form, () => window.sessionStorage.lastQuery);
        this.bindOnClick(form, (set) => {
            window.sessionStorage.lastQuery = set;
            return null;
        });
    };

    this.restoreLastQuery = function(form, getter) {
        if (form == null) {
            throw new Error("form was null");
        }
        if (getter()) {
            form.query.value = getter();
        }
    };

    this.redirect = function(value, pagingOn=true) {
        var pagingparam = "";
        if (pagingOn === true) {
            pagingparam = "P=0L" + paging.defaultPageLen + "&";
        }
        location.href = window.sessionStorage.caosdbBasePath + "Entity/?" + pagingparam + "query=" + value;
    }

    this.bindOnClick = function(form, setter) {
        if (setter == null || typeof(setter) !== 'function' || setter.length !== 1) {
            throw new Error("setter must be a function with one param");
        }

        /*
          Here a submit handler is created that is attached to both the form submit handler
          and the click handler of the button.
          See https://developer.mozilla.org/en-US/docs/Web/Events/submit why this is necessary.
          */
	    var submithandler = function() {

	        // store current query
            var queryField = form.query;
            var value = queryField.value.toUpperCase();
            if (typeof value == "undefined" || value.length == 0) {
                return;
            }
            if (!(value.startsWith("FIND") || value.startsWith("COUNT") || value.startsWith("SELECT"))) {
                queryField.value = "FIND ENTITY WHICH HAS A PROPERTY LIKE '*" + queryField.value + "*'";
            }
            setter(queryField.value);

	        queryForm.redirect(queryField.value, !queryForm.isSelectQuery(queryField.value));
	    };


        // handler for the form
	    form.onsubmit = function(e) {
	        e.preventDefault();
	        submithandler();
            return false;
	    };

        // same handler for the button
        form.getElementsByClassName("caosdb-search-btn")[0].onclick = function() {
	        submithandler();
        };
    };

    /**
     * Is the query a SELECT field,... FROM entity query?
     *
     * @param {HTMLElement} query, the query to be tested.
     * @return {Boolean}
     */
    this.isSelectQuery = function(query) {
        return query.toUpperCase().startsWith("SELECT");
    }

    /**
     * Remove the (hidden) paging input from the query form.
     * The form is changed in-place without copying it.
     *
     * @param {HTMLElement} form, the query form.
     * @return {HTMLElement} the form without the paging input.
     */
    this.removePagingField = function(form) {
        $(form.P).remove();
        return form;
    }
};


this.markdown = new function() {
    this.toHtml = function(textElement) {
        if ($(textElement).hasClass('markdowned')) {
            return textElement;
        }
        $(textElement).toggleClass('markdowned', true);

        $(textElement).find(".caosdb-comment-annotation-text").each(function() {
            let converter = new showdown.Converter();
            let text = $(this).html();
            let html = converter.makeHtml(text.trim());
            $(this).html(html);
        });
        $(textElement).find(".media-body:not(:has(.media-heading))").each(function() {
            $('<h4 class="media-heading">You<small><i> just posted</i></small></h4>').prependTo($(this));
        });
        return textElement;

    }
}

var hintMessages = new function() {
    this.init = function() {
        for (var entity of $('.caosdb-entity-panel')) {
            this.hintMessages(entity);
        }
    }

    this.removeMessages = function(entity) {
        $(entity).find(".alert").remove();
    }

    this.unhintMessages = function(entity) {
        $(entity).find(".caosdb-f-message-badge").remove();
        $(entity).find(".alert").show();
    }

    this.hintMessages = function(entity) {
        this.unhintMessages(entity);
        var messageType = {
            "info": "info",
            "warning": "warning",
            "danger": "error"
        };
        for (let alrt in messageType) {
            $(entity).find(".alert.alert-" + alrt).each(function(index) {
                var messageElem = $(this);
                if (messageElem.parent().find(".caosdb-f-message-badge.alert-" + alrt).length == 0) {
                    messageElem.parent('.caosdb-messages, .caosdb-property-row').prepend('<button title="Click here to show the ' + messageType[alrt] + ' messages of the last transaction." class="btn caosdb-f-message-badge badge alert-' + alrt + '">' + messageType[alrt] + '</button>');
                    messageElem.parent().find(".caosdb-f-message-badge.alert-" + alrt).on("click", function(e) {
                        $(this).hide();
                        console.log(this);
                        console.log(alrt);
                        messageElem.parent().find('.alert.alert-' + alrt).show()
                    });
                }
                messageElem.hide();
            });
        }

        if ($(entity).find(".caosdb-messages > .caosdb-f-message-badge").length > 0) {
            var div = $('<div class="text-right" style="padding: 5px 16px;"/>');
            div.prependTo($(entity).find(".caosdb-messages"));
            var messageBadges = $(entity).find(".caosdb-messages > .caosdb-f-message-badge");
            messageBadges.detach();
            div.append(messageBadges);
        }
        $(entity).find(".caosdb-property-row > .caosdb-f-message-badge").addClass("pull-right");
    }
}


function createErrorNotification(msg) {
    return $('<div class="' + globalClassNames.ErrorNotification + '">' + msg + '<div>')[0];
}

/**
 * Create a waiting notification with a informative message for the waiting user. 
 *
 * @param {String} info, a message for the user
 * @return {HTMLElement} A div with class `caosdb-preview-waiting-notification`.
 */
function createWaitingNotification(info) {
    return $('<div class="' + globalClassNames.WaitingNotification + '">' + info + '</div>')[0];
}

/**
 * Remove all waiting notifications from an element.
 *
 * @param {HTMLElement} elem
 * @return {HTMLElement} The parameter `elem`.
 */
function removeAllWaitingNotifications(elem) {
    $(elem.getElementsByClassName(globalClassNames.WaitingNotification)).remove();
    return elem;
}

/**
 * Extract the ID of an entity by parsing the textContent of the first occuring element with
 * class `caosdb-id`.
 *
 * @param {HTMLElement} entity
 * @returns {Number} ID of entity.
 */
function getEntityId(entity) {
    let id = Number.parseInt(entity.getElementsByClassName("caosdb-id")[0].textContent);
    if (isNaN(id)) throw new Error("id was NaN");
    return id;
}

/**
 * Post an xml document to basepath/Entity
 * 
 * @param xml,
 *            XML document
 * @param basepath,
 *            string
 * @param querySegment,
 *            string
 * @param timeout,
 *            integer (in milliseconds)
 * @return Promise object
 */
function postXml(xml, basepath, querySegment, timeout) {
    return $.ajax({
        type: 'POST',
        contentType: 'text/xml',
        url: basepath + "Entity/" + (querySegment == null ? "" : querySegment),
        processData: false,
        data: xml,
        dataType: 'xml',
        timeout: timeout,
        statusCode: {
            401: function() {
                throw new Error("unauthorized");
            },
        },
    });
}

/**
 * Serialize an xml document into plain string.
 * 
 * @param xml
 * @return string representation of xml
 */
function xml2str(xml) {
    return new XMLSerializer().serializeToString(xml);
}

/**
 * Load a json configuration.
 *
 * If the file cannot be found, an empty array is returned.
 * 
 * @param filename The filename of the configuration file residing in conf/.
 * @return object containing the configuration
 */
async function load_config(filename) {
    try {
        var data = await $.ajax({
            url: window.sessionStorage.caosdbBasePath + "webinterface/conf/" + filename,
            dataType: "json",
        });
    } catch (error) {
        if (error.status == 404) {
            return [];
        } else {
            throw error;
        }
    }
    return data;
}

/**
 * Convert a string into an xml document.
 * 
 * @param s,
 *            a string representation of an xml document.
 * @return an xml document.
 */
function str2xml(s) {
    var parser = new DOMParser();
    return parser.parseFromString(s, "text/xml");
}

/**
 * Asynchronously transform an xml into html via xslt.
 * 
 * @param xmlPromise,
 *            resolves to an input xml document.
 * @param xslPromise,
 *            resolves to a xsl script.
 * @param paramsPromise,
 *            resolves to parameters for the xsl script.
 * @return A promise which resolves to a generated HTML document.
 */
async function asyncXslt(xmlPromise, xslPromise, paramsPromise) {
    var xml, xsl, params;
    [xml, xsl, params] = await Promise.all([xmlPromise, xslPromise, paramsPromise]);
    return xslt(xml, xsl, params);
}

/**
 * transform a xml into html via xslt
 * 
 * @param xml,
 *            the input xml document
 * @param xsl,
 *            the transformation script
 * @param params,
 *            xsl parameter to be set (optionally).
 * @return html
 */
function xslt(xml, xsl, params) {
    var xsltProcessor = new XSLTProcessor();
    if (params) {
        for (var k in params) {
            if (params.hasOwnProperty(k)) {
                xsltProcessor.setParameter(null, k, params[k]);
            }
        }
    }
    if (typeof xsltProcessor.transformDocument == 'function') {
        // old FFs
        var retDoc = document.implementation.createDocument("", "", null);
        xsltProcessor.transformDocument(xml, xsl, retDoc, null);
        return retDoc.documentElement;
    } else {
        // modern browsers
        xsltProcessor.importStylesheet(xsl);
        return xsltProcessor.transformToFragment(xml, document);
    }
}

/**
 * TODO
 */
function getXSLScriptClone(source) {
    return str2xml(xml2str(source))
}

/**
 * TODO
 */
function injectTemplate(orig_xsl, template) {
    var xsl = getXSLScriptClone(orig_xsl);
    var entry_t = xsl.createElement("xsl:template");
    xsl.firstElementChild.appendChild(entry_t);
    entry_t.outerHTML = template;
    return xsl;
}

/**
 * TODO
 */
function insertParam(xsl, name, value = null) {
    var param = xsl.createElement("xsl:param");
    param.setAttribute("name", name);
    if (value != null) {
        param.setAttribute("select", "'" + value + "'");
    }
    xsl.firstElementChild.append(param);
}

/**
 * When the page is scrolled down 100 pixels, the scroll-back button appears.
 * 
 * @return
 */

/**
 * Every initial function calling is done here.
 * 
 * @return
 */
function initOnDocumentReady() {
    hintMessages.init();

    // init query form
    var form = document.getElementById("caosdb-query-form");
    if (form != null) {
        queryForm.init(form);
    }

    // show image 100% width
    $(".entity-image-preview").click(function() {
        $(this).css('width', '100%');
        $(this).css('max-width', "");
        $(this).css('max-height', "");
    });

}

$(document).ready(initOnDocumentReady);
