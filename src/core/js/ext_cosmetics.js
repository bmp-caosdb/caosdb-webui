var cosmetics = new function() {
    this.init = function() {
        this.linkify();
    }

    this.linkify = function() {
        $('.caosdb-property-text-value').each(function(index) {
            if (/^https?:\/\//.test(this.innerText)) {
                var uri = this.innerText;
                var text = uri

                $(this).parent().css("overflow", "hidden");
                $(this).parent().css("text-overflow", "ellipsis");
                $(this).html('<a href="' + uri + '"><span class="glyphicon glyphicon-new-window"></span> ' + text + '</a>');
            }
        });
    }
}


$(document).ready(function() {
    cosmetics.init();
});