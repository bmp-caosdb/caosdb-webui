#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

# Unzip all the external libraries and set up the links in the public folder

SQ=\'
ROOT_DIR = $(abspath .)
MISC_DIR = $(abspath misc)
PUBLIC_DIR = $(abspath public)
CONF_CORE_DIR = $(abspath conf/core)
CONF_EXT_DIR = $(abspath conf/ext)
SRC_CORE_DIR = $(abspath src/core)
SRC_EXT_DIR = $(abspath src/ext)
LIBS_DIR = $(abspath libs)
TEST_CORE_DIR = $(abspath test/core/)
TEST_EXT_DIR = $(abspath test/ext)
LIBS = fonts css/bootstrap.css js/bootstrap.js js/state-machine.js js/jquery.js js/showdown.js

TEST_LIBS = $(LIBS) js/qunit.js css/qunit.css $(subst $(TEST_CORE_DIR)/,,$(shell find $(TEST_CORE_DIR)/))

LIBS_ZIP = $(wildcard $(LIBS_DIR)/*.zip)
LIBS_SUBDIRS = $(addprefix $(LIBS_DIR)/, js css fonts)

ALL: test install

install: clean cp-src cp-ext cp-conf $(addprefix $(PUBLIC_DIR)/, $(LIBS))

test: clean cp-src cp-ext cp-ext-test cp-conf $(addprefix $(PUBLIC_DIR)/, $(TEST_LIBS))
	@for f in $(shell find $(TEST_EXT_DIR) -type f -iname *.js) ; do \
		sed -i "/EXTENSIONS/a \<script src=\"$${f#$(TEST_EXT_DIR)/}\" ></script>" $(PUBLIC_DIR)/index.html ; \
		echo include $$f; \
	done
	@for f in $(wildcard $(SRC_EXT_DIR)/js/*.js) ; do \
		sed -i "/EXTENSIONS/a \<script src=\"$${f#$(SRC_EXT_DIR)/}\" ></script>" $(PUBLIC_DIR)/index.html ; \
		echo include $$f; \
	done
	ln -s $(PUBLIC_DIR) $(PUBLIC_DIR)/webinterface

PORT = 8000
TIMEOUT = 200
XVFB-RUN = xvfb-run -e xerr.log
run-test-server: test
	cd $(PUBLIC_DIR); $(MISC_DIR)/unit_test_http_server.py $(PORT) $(TIMEOUT) False; echo $$? > $(ROOT_DIR)/.server_done

keep-test-server: test
	cd $(PUBLIC_DIR); $(MISC_DIR)/unit_test_http_server.py $(PORT) $(TIMEOUT) True; echo $$? > $(ROOT_DIR)/.server_done

run-qunit:
	$(foreach exec, firefox screen xvfb-run,\
	    $(if $(shell which $(exec)),echo "found $(exec)",$(error "No $(exec) in PATH")))

	# start server
	screen -L -S caosdb-webui-test -t server -d -m -A make run-test-server
	
	# start firefox
	screen -S caosdb-webui-test -X screen -t firefox $(XVFB-RUN) firefox \
		"http://localhost:$(PORT)/webinterface/index.html?loggerPort=$(PORT)"

	# wait until server stops
	while [ 1 -eq 1 ]; do \
		[ ! -e .server_done ] || break; \
	done; \
	true

	# stop firefox
	pkill -u $(shell whoami) firefox || true

	# print stuff
	cat screenlog.* || true
	cat $(PUBLIC_DIR)/qunit.log || true
	cat .server_done || true
	[ $$(cat .server_done) -eq 0 ]

cp-ext:
	for f in $(wildcard $(SRC_EXT_DIR)/js/*) ; do \
		cp -i -r $$f $(PUBLIC_DIR)/js/ ; \
		sed -i "/EXTENSIONS/a \<xsl:element name=\"script\"><xsl:attribute name=\"src\"><xsl:value-of select=\"concat\(\$$basepath, 'webinterface$${f#$(SRC_EXT_DIR)}'\)\" /></xsl:attribute></xsl:element>" $(PUBLIC_DIR)/xsl/main.xsl ; \
	done
	mkdir -p $(PUBLIC_DIR)/html
	for f in $(wildcard $(SRC_EXT_DIR)/html/*) ; do \
		cp -i -r $$(realpath $$f) $(PUBLIC_DIR)/html/ ; \
	done
	for f in $(wildcard $(SRC_EXT_DIR)/xsl/*) ; do \
		cp -i -r $$(realpath $$f) $(PUBLIC_DIR)/xsl/ ; \
	done

cp-ext-test:
	for f in $(wildcard $(TEST_EXT_DIR)/js/*) ; do \
		cp -i -r $$f $(PUBLIC_DIR)/js/ ; \
		sed -i "/EXTENSIONS/a \<xsl:element name=\"script\"><xsl:attribute name=\"src\"><xsl:value-of select=\"concat\(\$$basepath, 'webinterface$${f#$(SRC_EXT_DIR)}'\)\" /></xsl:attribute></xsl:element>" $(PUBLIC_DIR)/xsl/main.xsl ; \
	done
	mkdir -p $(PUBLIC_DIR)/html
	for f in $(wildcard $(TEST_EXT_DIR)/html/*) ; do \
		cp -i -r $$(realpath $$f) $(PUBLIC_DIR)/html/ ; \
	done
	for f in $(wildcard $(TEST_EXT_DIR)/xsl/*) ; do \
		cp -i -r $$(realpath $$f) $(PUBLIC_DIR)/xsl/ ; \
	done

cp-conf:
	mkdir -p $(PUBLIC_DIR)/conf
	if test -d $(CONF_CORE_DIR); then cp -r -t $(PUBLIC_DIR)/conf/ $$(find $(CONF_CORE_DIR)); fi
	if test -d $(CONF_EXT_DIR); then cp -r -t $(PUBLIC_DIR)/conf/ $$(find $(CONF_EXT_DIR)); fi

cp-src:
	cp -r $(SRC_CORE_DIR) $(PUBLIC_DIR)

$(PUBLIC_DIR)/%: $(LIBS_DIR)/%
	ln -s $< $@

$(PUBLIC_DIR)/%: $(TEST_CORE_DIR)/%
	ln -s $< $@

$(PUBLIC_DIR)/%: $(TEST_EXT_DIR)/%
	ln -s $< $@

$(LIBS_DIR)/fonts: unzip
	ln -s $(LIBS_DIR)/bootstrap-3.3.7-dist/fonts $@

$(LIBS_DIR)/js/bootstrap.js: unzip $(LIBS_DIR)/js
	ln -s $(LIBS_DIR)/bootstrap-3.3.7-dist/js/bootstrap.min.js $@

$(LIBS_DIR)/css/bootstrap.css: unzip $(LIBS_DIR)/css
	ln -s $(LIBS_DIR)/bootstrap-3.3.7-dist/css/bootstrap.min.css $@

$(LIBS_DIR)/js/jquery.js: unzip $(LIBS_DIR)/js
	ln -s $(LIBS_DIR)/jquery-3.3.1/jquery-3.3.1.min.js $@

$(LIBS_DIR)/js/showdown.js: unzip $(LIBS_DIR)/js
	ln -s $(LIBS_DIR)/showdown-1.8.6/dist/showdown.min.js $@

$(LIBS_DIR)/js/state-machine.js: unzip $(LIBS_DIR)/js
	ln -s $(LIBS_DIR)/javascript-state-machine-master/dist/state-machine.min.js $@

$(LIBS_DIR)/css/qunit.css: unzip $(LIBS_DIR)/css
	ln -s $(LIBS_DIR)/qunit-2.6.2/qunit-2.6.2.css $@

$(LIBS_DIR)/js/qunit.js: unzip $(LIBS_DIR)/js
	ln -s $(LIBS_DIR)/qunit-2.6.2/qunit-2.6.2.js $@

$(addprefix $(LIBS_DIR)/, js css):
	mkdir $@ || true

.PHONY: clean
clean:
	$(RM) -r $(PUBLIC_DIR)
	for f in $(LIBS_SUBDIRS); do unlink $$f || $(RM) -r $$f || true; done
	for f in $(patsubst %.zip,%/,$(LIBS_ZIP)); do $(RM) -r $$f; done
	$(RM) screenlog.*
	$(RM) .server_done

.PHONY: unzip
unzip:
	for f in $(LIBS_ZIP); do unzip -o -d libs $$f; done


PYLINT = pylint3 -d all -e E,F
PYTHON_FILES = $(subst $(ROOT_DIR)/,,$(shell find $(ROOT_DIR)/ -iname "*.py"))
pylint: $(PYTHON_FILES)
	for f in $(PYTHON_FILES); do $(PYLINT) $$f || exit 1; done

PYLINT_LOCAL = /usr/bin/pylint3 -d all -e E,F
pylint-local: $(PYTHON_FILES)
	for f in $(PYTHON_FILES); do $(PYLINT_LOCAL) $$f || exit 1; done
